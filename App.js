import 'react-native-gesture-handler';
import React, { Component, useEffect, useState } from 'react'
import { View, Text, StatusBar, Dimensions, Platform, TouchableOpacity, Image } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import { AuthContext } from "./src/globals/Context";
import DrawerContent from './src/globals/DrawerContent'

import Header from './src/components/Header'
import HeaderLogo from './src/components/HeaderLogo'

import Login from './src/screens/Login'
import Landing from './src/screens/Landing'
import SignUp from './src/screens/SignUp'
import ForgotPassword from './src/screens/ForgotPassword';

import HomeScreen from './src/screens/Home'
import CreatePostScreen from './src/screens/CreatePost'
import FavouritesScreen from './src/screens/Favourites';
import Profile from './src/screens/Profile';
import Dashboard from './src/screens/Dashboard';
import Notifications from './src/screens/Notifications';
import SearchScreen from './src/components/Search';

// drawer screens
import ProfileSetting from './src/screens/ProfileSetting';
import ProfileSetupScreen from './src/screens/ProfileSetup';
import NotificationSetting from './src/screens/NotificationSetting'
import PrivacySetting from './src/screens/PrivacySetting'
import BlockUser from './src/screens/BlockUser'
import Invite from './src/screens/Invite'
import Help from './src/screens/Help'
import AboutUs from './src/screens/AboutUs'
import ContactUs from './src/screens/ContactUs'
import Cookies from './src/screens/Cookies'
import TermsConditions from './src/screens/TermsConditions'
import Policy from './src/screens/Policy'
import CommunityGuidelines from './src/screens/CommunityGuidelines'
import DirectMessage from './src/screens/DirectMessage'
import Chat from './src/screens/Chat'
import QB from "quickblox-react-native-sdk"
import QBConfig from './src/QBConfig'
import Comments from './src/screens/Comments'
import UsersList from './src/screens/UsersList'

import { HomeServices } from './src/Services/Home.services';
import MyPosts from './src/screens/MyPosts';
import SinglePost from './src/screens/SinglePost';
import EditPost from './src/screens/EditPost';
import FollowersAndFollowings from './src/screens/FollowersAndFollowings';
import UserProfile from './src/screens/UserProfile';
import UserToBlock from './src/screens/UserToBlock';
import OneSignal from 'react-native-onesignal';
const homeServices = new HomeServices();

const { width, height } = Dimensions.get('window');

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;


// const AuthStack = createStackNavigator();
// const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const Auth = createStackNavigator();
// const ProfileStack = createStackNavigator();
const Drawer = createDrawerNavigator();
// const HomeDrawer = createDrawerNavigator();


class LogoTitle extends React.Component {
  render() {
    return (<View style={{ width: '100%', backgroundColor: '#6C0AC7', height: Platform.OS === 'ios' ? 95 : 55, paddingTop: Platform.OS === 'ios' ? 40 : 0 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'relative' }}>
        {this.props.previous &&
          <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10, position: 'absolute', zIndex: 9999, top: 7 }} onPress={this.props.navigation.goBack}>
            <Icon name={"arrow-back"} color="#fff" size={RFPercentage(5)} />
          </TouchableOpacity>
        }
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Image
            source={require('./src/assets/splash.png')}
            style={{ height: 50, width: '100%', resizeMode: 'contain' }}
          />
        </View>
      </View>
    </View>
    );
  }
}

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator initialRouteName="Home">
      <HomeStack.Screen
        name="Home" component={Dashboard}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="CreatePost" component={CreatePostScreen}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="EditPost" component={EditPost}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="ProfileSetup" component={ProfileSetupScreen}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Favourites" component={FavouritesScreen}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Notifications" component={Notifications}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Profile" component={Profile}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="UserProfile" component={UserProfile}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Search" component={SearchScreen}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <Header previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="DirectMessage" component={DirectMessage}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Chat" component={Chat}
        options={{
          headerShown: false,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="UsersList" component={UsersList}
        options={{
          headerShown: false,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="ProfileSetting" component={ProfileSetting}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="NotificationSetting" component={NotificationSetting}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="PrivacySetting" component={PrivacySetting}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="BlockUser" component={BlockUser}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Invite" component={Invite}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Help" component={Help}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="AboutUs" component={AboutUs}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="ContactUs" component={ContactUs}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Cookies" component={Cookies}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="TermsConditions" component={TermsConditions}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Policy" component={Policy}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="CommunityGuidelines" component={CommunityGuidelines}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="Comments" component={Comments}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="MyPosts" component={MyPosts}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="FollowersAndFollowings" component={FollowersAndFollowings}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="SinglePost" component={SinglePost}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
      <HomeStack.Screen
        name="UserToBlock" component={UserToBlock}
        options={{
          headerShown: true,
          header: ({ scene, previous, navigation }) => {
            return (
              <HeaderLogo previous={previous} navigation={navigation} />
            );
          },
        }}
      />
    </HomeStack.Navigator>
  )
}

export default ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [userToken, setUserToken] = useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setLoading(false);
        setUserToken('asdf');
        getUser();
      },
      signUp: () => {
        setLoading(false);
        setUserToken('asdf');
      },
      signOut: async () => {
        try {
          await AsyncStorage.removeItem('_moodflikAppData');
          QB.auth
            .logout()
            .then(function () {
              console.log("QB Sign Out success")
              // signed out successfully
            })
            .catch(function (e) {
              console.log("QB Sign Out Failed", e)
              // handle error
            });
          setLoading(false);
          setUserToken(null);
        } catch (err) {
          console.log(err);
        }
      }
    }
  }, []);


  React.useEffect(() => {
    setTimeout(() => {
      setLoading(false);
      getUser();
    }, 1000);
  }, []);


  const setDeviceId = async (deviceId, uid) => {
    const { userId } = await OneSignal.getDeviceState();
    if (deviceId != userId) {
      var payload = {
        device_id: userId
      }
      await homeServices.updateUserDetails(payload, uid).then(
        (data) => {
          if (data.uuid) {
            console.log("Successfully Updated Device id", data)
          } else {
            console.log("Updated Device id failed", data)
          }
        },
        (error) => {
          console.log("error.response.status", error);
        }
      );
    }
  }

  const setQBUID = async (uid, userbiodata) => {
    try {
      var userData1 = await AsyncStorage.getItem('_moodflikAppData');
      userData = JSON.parse(userData1);
      if (userData && userData != "" && userData.userId) {
        var loginData = {
          ...userData,
          _qbUid: uid
        }
        try {
          await AsyncStorage.setItem('_moodflikAppData', JSON.stringify(loginData));
          var payload = {
            occupant_id: uid
          }
          await homeServices.updateUserDetails(payload, userbiodata.uuid).then(
            (data) => {
              if (data.status) {
                console.log("Successfully Updated Occupant id", data)
              } else {
                console.log("Updated Occupant id failed", data)
              }
            },
            (error) => {
              console.log("error.response.status", error);
            }
          );
        } catch (err) {
          console.log("Error Update id", err);
        }
      }
    } catch (err) {
      console.log(err);
    }
  }

  var connectQB = async (uid) => {
    await homeServices.getUserDetails(uid).then(
      (data) => {
        console.log("ppiiiuu", data)
        if (data && data.uuid) {
          setDeviceId(data.device_id, data.uuid)
          var udata = data;
          QB.settings
            .init(QBConfig)
            .then(function () {
              // SDK initialized successfully
              QB.auth
                .login({
                  login: udata.username,
                  password: 'qb_moodflik_test'
                })
                .then(function (info) {
                  setQBUID(info.user.id, udata)
                  QB.chat
                    .connect({
                      userId: info.user.id,
                      password: 'qb_moodflik_test'
                    })
                    .then(function () {
                      // connected successfully
                      console.log("QB connected successfully")
                    })
                    .catch(function (e) {
                      // some error occurred
                      console.log("Error Connect 1:", e)
                    });
                })
                .catch(function (e) {
                  var fullName = udata.first_name + ' ' + udata.last_name;
                  console.log("Error 2:", e)
                  QB.users
                    .create({
                      email: udata.email,
                      fullName: fullName,
                      login: udata.username,
                      password: 'qb_moodflik_test',
                      phone: udata.phone_number,
                      tags: ['moodflik', 'quickblox']
                    })
                    .then(function (user) {
                      console.log("User created:", user)
                      setQBUID(user.id, udata)
                      connectQB(uid);
                    })
                    .catch(function (e) {
                      console.log("Error 3:", e)
                    });
                });
            })
            .catch(function (e) {
              console.log("Error 4:", e)
            });
          QB.settings.enableAutoReconnect({ enable: true })
        }
      },
      (error) => {
        console.log("error.response.status", error);
      }
    );
  }

  const getUser = async () => {
    try {
      var userData = await AsyncStorage.getItem('_moodflikAppData');
      userData = JSON.parse(userData);
      if (userData && userData != "" && userData.userId) {
        setUserToken("active")
        await connectQB(userData.userId)
      }
    } catch (err) {
      console.log(err);
    }
  }

  if (isLoading) {
    return <View>
      {/* <StatusBar translucent backgroundColor='transparent' /> */}
      <View style={{ height: height, width: width, position: 'relative', backgroundColor: '#6C0AC7' }}>
        <Image
          style={{ height: '100%', width: '100%', resizeMode: 'contain' }}
          source={require('./src/assets/splash.png')}
        />
      </View>
    </View>;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {!userToken ? (
          <Auth.Navigator>
            <Auth.Screen name="Main" component={Landing}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            // options={{
            //   headerTitle: props => <LogoTitle {...props} />,
            //   headerStyle: {
            //     backgroundColor: '#6C0AC7',
            //   },
            // }}
            />
            <Auth.Screen name="Login" component={Login}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
            <Auth.Screen name="SignUp" component={SignUp}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
            <Auth.Screen name="ForgetPassword" component={ForgotPassword}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
            <Auth.Screen name="TermsConditions" component={TermsConditions}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
            <Auth.Screen name="Policy" component={Policy}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
            <Auth.Screen name="CommunityGuidelines" component={CommunityGuidelines}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
            <Auth.Screen name="Cookies" component={Cookies}
              options={{
                headerShown: true,
                header: ({ scene, previous, navigation }) => {
                  return (
                    <LogoTitle previous={previous} navigation={navigation} />
                  );
                },
              }}
            />
          </Auth.Navigator>
        ) : (
          <Drawer.Navigator drawerPosition="right"  drawerContent={props => <DrawerContent {...props} />}>
            <Drawer.Screen name="Shop" component={HomeStackScreen}
              options={
                {
                  drawerLabel: "Shop",
                  drawerIcon: ({ tintColor }) => <Icon name={'home'} size={30} color={tintColor} />
                }
              }
            />
          </Drawer.Navigator>
        )}
      </NavigationContainer>
    </AuthContext.Provider>
  )
}

