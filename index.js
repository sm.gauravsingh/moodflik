/**
 * @format
 */

import { AppRegistry,Platform } from 'react-native';
import React, { useEffect } from 'react'
import App from './App';
import { name as appName } from './app.json';
import OneSignal from 'react-native-onesignal';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from 'react-native-push-notification';
import QB from "quickblox-react-native-sdk"


const appSettings = {
  appId: '91867',
  authKey: 'J5H-KYBZTGWarmg',
  authSecret: 'wPXG4CXPrMKFOQd',
  accountKey: 'G3AHXj94R9sNP5S--fqM',
  apiEndpoint: '',
  chatEndpoint: '',
};


QB.settings
.init(appSettings)
.then(function () {
  console.log("SDK initialized successfully")
})
.catch(function (e) {
  console.log("Some error occurred, look at the exception message for more details-",e)
});
//OneSignal Init Code
OneSignal.setLogLevel(6, 0);
OneSignal.setAppId("6e31a5b1-380f-4016-b2de-1324c73deffd");
//END OneSignal Init Code

//Prompt for push on iOS
OneSignal.promptForPushNotificationsWithUserResponse(response => {
  console.log("Prompt response:", response);
});

//Method for handling notifications received while app in foreground
OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
  console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
  let notification = notificationReceivedEvent.getNotification();
  console.log("notification: ", notification);
  const data = notification.additionalData
  console.log("additionalData: ", data);
  // Complete with null means don't show a notification.
  notificationReceivedEvent.complete(notification);
});

//Method for handling notifications opened
OneSignal.setNotificationOpenedHandler(notification => {
  console.log("OneSignal: notification opened:", notification);
});

// Must be outside of any component LifeCycle (such as `componentDidMount`).
PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function(token) {
    // token obtained from APNS or FCM
    const config = Platform.OS === 'ios' ? {
      deviceToken: token,
      // to receive incoming call notification on iOS device(s) APNS_VOIP should be used
      pushChannel: QB.subscriptions.PUSH_CHANNEL.APNS_VOIP
    } : {
      deviceToken: token.token
    }
  
    QB.subscriptions
      .create(config)
      .then((subscriptions) => { 
        console.log("/* subscription(s) created successfully */",subscriptions)
       })
      .catch(e => { /* handle error */ })
  },
  // (required) Called when a remote is received or opened, or local notification is opened
  onNotification: function (notification) {
    console.log("NOTIFICATION:", notification);
    // process the notification
    // (required) Called when a remote is received or opened, or local notification is opened
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },
  // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);
    // process the action
  },
  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: function (err) {
    console.error(err.message, err);
  },
  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,
  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   * - if you are not using remote notification or do not have Firebase installed, use this:
   *     requestPermissions: Platform.OS === 'ios'
   */
  requestPermissions: true,
});




const Main = () => {
    const store = useGlobalState();
    return (
        <Context.Provider value={store}>
            <App />
        </Context.Provider>

    )
}

AppRegistry.registerComponent(appName, () => App);

