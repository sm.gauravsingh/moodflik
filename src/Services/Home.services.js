import axios from 'react-native-axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

// const baseUrl = "http://ec2-52-14-132-192.us-east-2.compute.amazonaws.com/";
const baseUrl = "https://t87goxe24a.execute-api.ap-south-1.amazonaws.com/mood_dev/";

var getData = async () => {
    try {
        const value = await AsyncStorage.getItem('_moodflikAppData')
        if (value && value !== null) {
            return JSON.parse(value)
        }
    } catch (e) {
        // error reading value
    }
}

export class HomeServices {

    constructor() {
        axios.interceptors.request.use(
            function (config) {
                let tokenData = getData()
                config.headers.Authorization = `token ${tokenData._token}`;
                return config;
            },
            (error) => {
                console.log("error.response.status", error);
                return error;
            }
        );
    }

    async getAppData() {
        try {
            const value = await AsyncStorage.getItem('_moodflikAppData')
            if (value !== null) {
                return JSON.parse(value)
            }
        } catch (e) {
            // error reading value
        }
    }

    getDataToken = async () => {
        try {
            const value = await AsyncStorage.getItem('_moodflikAppData')

            if (value && value !== null) {
                return JSON.parse(value)
            }
        } catch (e) {
            // error reading value
        }
    }

    async getDashboardData(type) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/post/?type=${type}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getDashboardDislikedata() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/dislikehome`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getBioDetails() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/bio/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getUserDetails(uid) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}user/${uid}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async setBioDetails(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/bio/`, requestOptions)
            .then(response => response.json())
            .then(result => {return result})
            .catch(error => console.log('error', error));
    }

    async updateUserDetails(payload,uid) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'PATCH',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}user/${uid}/`, requestOptions)
            .then(response => response.json())
            .then(result => {return result})
            .catch(error => console.log('error', error));
    }

    async createDislikePost(payload) {
        console.log("aś",payload)
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/dislikepost/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async createPost(payload) {
        console.log("aś",payload)
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/post/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async updatePost(payload,id) {
        console.log("aś",payload)
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'PATCH',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/post_update/${id}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async likeDisLikePost(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/reactions/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async likePost(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/add_like/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async disLikePost(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/add_dislike/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async addFavourite(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/favourites/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async removeFavourite(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/remove_favourites/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async addSeen(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/seen/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getFavouritesData(type,id="") {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/favourites/?type=${type}&user_id=${id}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getFavouritesDislikesData() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/dislike_favorite/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getComentsData(postId) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/get_post_comments/${postId}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async addComments(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/comment/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getPostSetting() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}privacy_setting`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getBioSetting() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}privacy_setting/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getLikeSetting() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}privacy_setting/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async savePostSetting(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}privacy_setting/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async saveBioSetting(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/bio_setting/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async saveLikeSetting(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/show_figures/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getPostStats(uid) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/post_stats/${uid}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getMyPosts(type,id="") {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/my_posts/?type=${type}&user_id=${id}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async deletPost(id) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/post_update/${id}`, requestOptions)
            .then(response => response.text())
            .then(result => { return 'deleted' })
            .catch(error => console.log('Delete error', error));
    }


    async getNotificationData() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post_notifications/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async saveNotificationSetting(payload) {
        console.log("as",payload)
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post_notifications/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async saveOtherNotificationSettings(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/others_settings/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async saveFollowData(payload) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/follow/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getFollowData(id = "") {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/follow/?user_id=`+id, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async updateOccupantId(payload,uid) {
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}User/bio/${uid}/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getUserList(q) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/friends/?search=${q}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getPostById(id) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}api/post_by_id/${id}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getUserData(q) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}get_user_by_occ_id/${q}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getMyLikePost() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/my_like_posts/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getMyDisLikePost() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}post/my_dislike_posts/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async getLoadMoreData(url) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `token ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${url}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async searchUser(text) {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}/usersfilters/?search=${text}`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }


    async getBlockedUserList() {
        let tokenData = await this.getDataToken()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}/block_user`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async blockUser(id) {
        var payload = {user_id : id};
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}block_user/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }

    async unblockUser(id) {
        var payload = {user_id : id};
        var myHeaders = new Headers();
        var tokenData = await getData();
        myHeaders.append("Authorization", `Bearer ${tokenData._token}`);
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify(payload);
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        return fetch(`${baseUrl}unblock_user/`, requestOptions)
            .then(response => response.json())
            .then(result => { return result })
            .catch(error => console.log('error', error));
    }




}