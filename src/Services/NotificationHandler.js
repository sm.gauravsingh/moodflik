import OneSignal from 'react-native-onesignal';



export class NotificationHandler {

    async selfPushNotification(mesg){
        const { userId } = await OneSignal.getDeviceState();
        console.log('ppp.....>>',userId)
        const notificationObj = {
            contents: { en: mesg },
            include_player_ids: [userId]
        };
        const jsonString = JSON.stringify(notificationObj);
        OneSignal.postNotification(jsonString, (success) => {
            console.log("Success:", success);
        }, (error) => {
            console.log("Error:", error);
        });
    }

    async pushNotification(mesg,reciepentIds){
        const notificationObj = {
            contents: { en: mesg },
            include_player_ids: reciepentIds
        };
        const jsonString = JSON.stringify(notificationObj);
        OneSignal.postNotification(jsonString, (success) => {
            console.log("Success:", success);
            console.log("Successfully sent notififcation:", success);
        }, (error) => {
            console.log("Error:", error);
        });
    }

}