import React from 'react'
import { StyleSheet, Text, View, ToastAndroid, Platform, AlertIOS } from 'react-native'

export default function Notify(props) {
    if (Platform.OS === 'android') {
        ToastAndroid.show(props.msg, ToastAndroid.SHORT)
      } else {
        AlertIOS.alert(props.msg);
      }
}

const styles = StyleSheet.create({})
