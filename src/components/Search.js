import React from 'react'
import { StyleSheet, Text, View, FlatList, Image, ActivityIndicator, Dimensions, KeyboardAvoidingView, TouchableOpacity,ToastAndroid, AlertIOS,Platform } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import BottomMenu from './BottomMenu';
import CustomInput from './CustomInput';
import { HomeServices } from '../Services/Home.services';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
const { width, height } = Dimensions.get('window');

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function Search(props) {
    const [string, setString] = React.useState("");
    const [isLoader, setIsLoader] = React.useState(false);
    const [isrefresh, setIsRefresh] = React.useState(false);
    const [userLists, setUserLists] = React.useState([]);

    const handleSearchText = (text) => {
        setString(text)
    }

    React.useEffect(() => {
        searchUser()
    }, [string])

    const searchUser = async () => {
        setIsRefresh(true)
        setIsLoader(true)
        await homeServices.searchUser(string).then(
            (data) => {
                setIsRefresh(false)
                setIsLoader(false)
                if (data) {
                    setUserLists(data)
                }
            },
            (error) => {
                setIsRefresh(false)
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    var saveFollowData = async (uid) => {
        var payload = {
            following: uid
        }
        await homeServices.saveFollowData(payload).then(
            (data) => {
                if (data) {
                    if(data.response){
                        var msg = data.response;
                    }else{
                        var msg = "Now following"
                    }
                    if (Platform.OS === 'android') {
                        ToastAndroid.show(msg, ToastAndroid.SHORT)
                    } else {
                        AlertIOS.alert(msg);
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    const checkUsername = (val) => {
        if(val.includes("@")){
            return val;
        }else{
            return '@'+val;
        }
    }

    const renderItem = ({ item, index }) => {
        return <View>
            <View style={{ flexDirection: 'row', width: '90%', alignItems: 'center', padding: 10, justifyContent: 'space-between' }}>
                <View style={{ width: vw * 15 }}>
                    <Image
                        style={styles.img}
                        source={{
                            uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                        }}
                    />
                </View>
                <View style={{ marginLeft: 20, width: vw * 65 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: RFPercentage(2) }}>{checkUsername(item.username)}</Text>
                    <Text style={{ fontSize: RFPercentage(2.7) }}>{item.first_name} {item.last_name && item.last_name}</Text>
                </View>
                <View style={{ marginRight: 15, width: vw * 15 }}>
                    <TouchableOpacity onPress={() => saveFollowData(item.uuid)}>
                        <FIcon5 name="user-plus" size={20} color="#6c0ac7" />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    }



    return (<>
        <View>
            <Text style={{ textAlign: 'center', fontSize: RFPercentage(3), marginTop: 10, fontWeight: 'bold' }}>
                Your Search Result
            </Text>
            <View style={{ marginHorizontal: 15, marginTop: 15 }}>
                <CustomInput placeHolder={"Start typing..."} action={(text) => handleSearchText(text)} value={string} />
            </View>
            {userLists && userLists.length ?
                <FlatList
                    style={{ width: width,marginBottom:120 }}
                    keyExtractor={(item, index) => index}
                    data={userLists}
                    renderItem={(item, index) => renderItem(item, index)}
                    // onEndReached={() => loadMoreData()}
                    // onEndReachedThreshold={0.9}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    ListFooterComponent={
                        isrefresh &&
                        <ActivityIndicator color="#6C0AC7" />
                    }
                />
                :
                <Text style={{ textAlign: 'center' }}>No user found</Text>
            }
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    img: {
        width: 60,
        height: 60,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 60 / 2
    }
})
