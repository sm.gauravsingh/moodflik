import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, StatusBar, View, Dimensions, Platform, Image } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Icon from 'react-native-vector-icons/Ionicons';
import DrawerMenu from './DrawerMenu';
const { width, height } = Dimensions.get('window');

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

export default function Header(props) {

    const openDrawer = () => {
        props.navigation.openDrawer();
    }

    return (<>
        <StatusBar
            animated={true}
            backgroundColor="#6C0AC7"
            hidden={false} />
        <View style={{ width: '100%', backgroundColor: '#6C0AC7', height: Platform.OS === 'ios' ? 100 : 80, flexDirection: 'column', justifyContent: 'center', paddingTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                {props.previous &&
                    <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10 }} onPress={props.navigation.goBack}>
                        <Icon name={"arrow-back"} color="#fff" size={RFPercentage(5)} />
                    </TouchableOpacity>
                }
                <View style={{ width: '45%', flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                        source={require('../assets/splash.png')}
                        style={{ height: Platform.OS === 'ios' ? 40 : 40, width: '100%', resizeMode: 'contain' }}
                    />
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'center', width: '35%' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '100%' }}>
                        <TouchableOpacity style={{ marginLeft: 20 }} onPress={() => props.navigation.navigate("Search")}>
                            <Icon name={props.activeScreen == "Search" ? 'search' : 'search-outline'} style={{ width: '100%', textAlign: 'center', fontSize: RFPercentage(3.5), marginRight: 5, color: props.activeScreen == "Order" ? '#f04c4c' : '#fff' }} />
                            <Text style={{color:'#fff',marginLeft:5,fontSize:RFPercentage(2)}}>Search</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginLeft: 20 }} onPress={() => props.navigation.navigate("DirectMessage")}>
                            <Icon name={props.activeScreen == "Chat" ? 'chatbox-ellipses' : 'chatbox-ellipses-outline'} style={{ width: '100%', textAlign: 'center', fontSize: RFPercentage(3.5), marginRight: 5, color: props.activeScreen == "Order" ? '#f04c4c' : '#fff' }} />
                            <Text style={{color:'#fff',marginLeft:5,fontSize:RFPercentage(2)}}>DMs</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginLeft: 20 }} onPress={openDrawer}>
                            <Icon name={props.activeScreen == "Menu" ? 'menu' : 'menu-outline'} style={{ width: '100%', textAlign: 'center', fontSize: RFPercentage(4), marginRight: 5, marginTop: -3, color: props.activeScreen == "Order" ? '#f04c4c' : '#fff' }} />
                            <Text style={{color:'#fff',marginRight:5,fontSize:RFPercentage(2)}}>Menu</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    drawerMenu: {
        width: vw * 80,
        minHeight: 200,
        position: 'absolute',
        top: 30,
        right: 0,
        zIndex: 9999,
        backgroundColor: 'red'
    }
})
