import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Animated, Modal, FlatList, ActivityIndicator, Text, View, Dimensions, Image, PermissionsAndroid, CheckBox, ToastAndroid, AlertIOS, Platform, PanResponder, KeyboardAvoidingView, TouchableOpacity, Keyboard } from 'react-native'
import CustomButton from '../components/CustomButton'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import Icon from 'react-native-vector-icons/AntDesign';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import FIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MatIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { HomeServices } from '../Services/Home.services';
import { ScrollView } from 'react-native-gesture-handler';
import { RFPercentage } from 'react-native-responsive-fontsize';
import AwesomeAlert from 'react-native-awesome-alerts';
import { TextInput } from 'react-native-paper';
import { RadioButton } from 'react-native-paper';
import Lightbox from 'react-native-lightbox';

const defaultImg = require('../assets/default_profile.png')
const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function Post(props) {
    const [item, setItem] = useState(props.post);
    const [alertEnable, setAlertEnable] = useState(false);
    const [shareEnable, setShareEnable] = useState(false);
    const [alertTitle, setAlertTitle] = useState('');
    const [alertMesg, setAlertMesg] = useState('');
    const [deletedId, setDeletedId] = useState('');
    const [userList, setUserList] = useState([]);
    const [likeRefresh, setLikeListRefresh] = useState(false);
    const [sendEnabled, setSendEnabled] = useState(false);
    const [showAll, setShowAll] = useState(false);
    const [text, setText] = useState('');
    const [checkedUserList, setCheckedUserList] = useState([]);
    const [selectedPostId, setSelectedPostId] = useState('');

    const [panY, setPanY] = useState(new Animated.Value(Dimensions.get('screen').height))
    const top = panY.interpolate({
        inputRange: [-1, 0, 1],
        outputRange: [0, 0, 1],
    });

    var _resetPositionAnim = Animated.timing(panY, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false
    })
    var _closeAnim = Animated.timing(panY, {
        toValue: Dimensions.get('screen').height,
        duration: 500,
        useNativeDriver: false
    })

    const panResponder = React.useMemo(() => PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponder: () => false,
        onPanResponderMove: Animated.event([
            null, { dy: panY }
        ]),
        onPanResponderRelease: (e, gs) => {
            if (gs.dy > 0 && gs.vy > 0) {
                setText('')
                return _closeAnim.start(() => setShareEnable(false));
            }
            return _resetPositionAnim.start();
        },
    }), []);

    const _handleDismiss = () => {
        setText('')
        _closeAnim.start(() => setShareEnable(false));
    }

    React.useEffect(() => {
        if (!shareEnable) {
            _closeAnim.start();
        } else {
            _resetPositionAnim.start();
        }
    }, [shareEnable]);

    var getUserList = async (q) => {
        await homeServices.getUserList(q).then(
            (data) => {
                if (data) {
                    setUserList(data.results)
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    var handleOnchange = (q) => {
        setText(q)
        getUserList(q);
    }

    var handleLikeDisLike = async (post, type) => {
        handleSeen(post, type)
        var postData = {
            post: post.uuid,
            reaction: type
        }
        await homeServices.likeDisLikePost(postData).then(
            (data) => {
                if (data) {
                    console.log("ppp", data)
                    if (props.likeSection) {
                        if (type == "like") {
                            item.like_count = !item.is_like ? item.like_count + 1 : item.like_count
                            item.dislike_count = item.is_dislike ? item.dislike_count - 1 : item.dislike_count
                            item.is_like = true
                            item.is_dislike = false
                        } else {
                            item.dislike_count = !item.is_dislike ? item.dislike_count + 1 : item.dislike_count
                            item.like_count = item.is_like ? item.like_count - 1 : item.like_count
                            item.is_dislike = true
                            item.is_like = false
                        }
                        setItem({ ...item })
                    } else {
                        if (type == "like") {
                            item.like_count = !item.is_like ? item.like_count + 1 : item.like_count
                            item.dislike_count = item.is_dislike ? item.dislike_count - 1 : item.dislike_count
                            item.is_like = true
                            item.is_dislike = false
                        } else {
                            item.dislike_count = !item.is_dislike ? item.dislike_count + 1 : item.dislike_count
                            item.like_count = item.is_like ? item.like_count - 1 : item.like_count
                            item.is_dislike = true
                            item.is_like = false
                        }
                        setItem({ ...item })
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }
    var handleFavourite = async (post, type) => {
        handleSeen(post, type)
        var postData = {
            post: post.uuid,
        }
        await homeServices.addFavourite(postData).then(
            (data) => {
                console.log("favvv", data)
                if (data) {
                    if (!post.is_favourite) {
                        item.favourite_count = !item.is_favourite ? item.favourite_count + 1 : item.favourite_count
                        item.is_favourite = true
                        setItem({ ...item })
                    } else {
                        item.favourite_count = item.is_favourite ? item.favourite_count - 1 : item.favourite_count
                        item.is_favourite = false
                        setItem({ ...item })
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }
    var handleComment = async (post, type) => {
        handleSeen(post, type)
        props.navigation.navigate('Comments', {
            refreshPage: {
                refresh: Math.random(),
                post: post,
                type: type
            },
        })
    }
    var handleShare = async (post, type) => {
        setShareEnable(true)
        getUserList('')
        setSelectedPostId(post.uuid)
        handleSeen(post, type)
    }
    var handleSeen = async (post, type) => {
        var postData = {
            post: post.uuid,
        }
        await homeServices.addSeen(postData).then(
            (data) => {
                if (!item.is_seen) {
                    item.seen_count = item.seen_count + 1;
                    item.is_seen = true
                    setItem({ ...item })
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }
    const dateFormat = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        return result;
    }
    const check5Minute = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        if (result.includes('seconds ago')) {
            return false;
        } else if (result.includes('minutes ago')) {
            let firstWord = result.split(" ")[0]
            var check = parseInt(firstWord.trim());
            if (check > 4) {
                return true;
            } else {
                return false;
            }
        } else if (!result.includes('seconds ago')) {
            return true;
        } else {
            return false;
        }
    }
    const timeDifference = (current, previous) => {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        var elapsed = current - previous;
        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        }
        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }
        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        }
        else if (elapsed < msPerMonth) {
            if (Math.round(elapsed / msPerDay) > 1) {
                return Math.round(elapsed / msPerDay) + ' days ago';
            } else {
                return Math.round(elapsed / msPerDay) + ' day ago';
            }
        }
        else if (elapsed < msPerYear) {
            if (Math.round(elapsed / msPerMonth) > 1) {
                return Math.round(elapsed / msPerMonth) + ' months ago';
            } else {
                return Math.round(elapsed / msPerMonth) + ' month ago';
            }
        }
        else {
            if (Math.round(elapsed / msPerYear) > 1) {
                return Math.round(elapsed / msPerYear) + ' years ago';
            } else {
                return Math.round(elapsed / msPerYear) + ' year ago';
            }
        }
    }

    var saveFollowData = async (uid) => {
        var payload = {
            following: uid
        }
        await homeServices.saveFollowData(payload).then(
            (data) => {
                var msg = ""
                if (data.response) {
                    msg = data.response
                } else {
                    msg = "Now following"
                }
                if (Platform.OS === 'android') {
                    ToastAndroid.show(msg, ToastAndroid.SHORT)
                } else {
                    AlertIOS.alert(msg);
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    const openMenu = () => {
        item.isMenuOpen = item.isMenuOpen ? !item.isMenuOpen : true;
        setItem({ ...item })
    }
    const closeMenu = () => {
        item.isMenuOpen = false;
        setItem({ ...item })
    }

    const handleEdit = () => {
        closeMenu();
        props.navigation.navigate('EditPost', {
            refreshPage: {
                refresh: Math.random(),
                item: item
            },
        })
    }

    const handleDelete = () => {
        closeMenu();
        setAlertTitle('Are you sure want to delete this post ?')
        setAlertEnable(true)
    }

    const handleReport = () => {
        closeMenu();
    }

    const deletePost = async () => {
        setAlertEnable(false);
        var id = item.uuid;
        await homeServices.deletPost(item.uuid).then(
            (data) => {
                if (data == 'deleted') {
                    console.log("ddd", id)
                    setDeletedId(id);
                    var msg = "Post Deleted Successfully";
                    if (Platform.OS === 'android') {
                        ToastAndroid.show(msg, ToastAndroid.SHORT)
                    } else {
                        AlertIOS.alert(msg);
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    const handleCheckUserList = (id) => {
        if (id && id != null && id != "") {
            var tempChecked = checkedUserList;
            if (tempChecked.length < 10) {
                if (tempChecked.includes(id)) {
                    const index = tempChecked.indexOf(id);
                    if (index > -1) {
                        tempChecked.splice(index, 1);
                    }
                } else {
                    tempChecked.push(id)
                }
                if (tempChecked.length > 0) {
                    setSendEnabled(true)
                } else {
                    setSendEnabled(false)
                }
                setCheckedUserList([...tempChecked]);
            } else {
                var msg = "Can not select more than 10 users !!!"
                if (Platform.OS === 'android') {
                    ToastAndroid.show(msg, ToastAndroid.SHORT)
                } else {
                    AlertIOS.alert(msg);
                }
            }
        } else {
            var msg = "Can not send to this user !!!"
            if (Platform.OS === 'android') {
                ToastAndroid.show(msg, ToastAndroid.SHORT)
            } else {
                AlertIOS.alert(msg);
            }
        }
    }

    const handleShareToUsers = () => {
        var selectedUsers = checkedUserList;
        if (selectedUsers.length > 0) {
            setSelectedPostId('');
            setCheckedUserList([]);
            _handleDismiss();
            props.navigation.navigate('DirectMessage', {
                refreshPage: {
                    refresh: Math.random(),
                    type: 'send_post',
                    userlist: selectedUsers,
                    selectedpost: selectedPostId
                },
            })
        }
    }

    const redirectToUserProfile = (id) => {
        if (props.userId != id) {
            props.navigation.navigate('UserProfile', {
                refreshPage: {
                    refresh: Math.random(),
                    type: 'show_profile',
                    userId: id,
                },
            })
        } else {
            props.navigation.navigate('Profile', {
                refreshPage: {
                    refresh: Math.random(),
                },
            })
        }
    }

    const numberFormatter = (num, digits) => {
        const lookup = [
            { value: 1, symbol: "" },
            { value: 1e3, symbol: "k" },
            { value: 1e6, symbol: "M" },
            { value: 1e9, symbol: "G" },
            { value: 1e12, symbol: "T" },
            { value: 1e15, symbol: "P" },
            { value: 1e18, symbol: "E" }
        ];
        const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var item = lookup.slice().reverse().find(function (item) {
            return num >= item.value;
        });
        return item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
    }

    const renderUserList = ({ item, index }) => {
        return <>
            <TouchableOpacity onPress={() => handleCheckUserList(item.occupant_id)}>
                <View style={styles.chat_box}>
                    <View style={styles.image_box}>
                        <Image style={styles.profile_img_user} source={item.photo_url ? item.photo_url + `?buster=${Math.random()}` : defaultImg} />
                    </View>
                    <View style={styles.detail_box}>
                        <Text style={styles.name}>{item.first_name} {item.last_name}</Text>
                        <Text style={styles.username}>{item.username}</Text>
                    </View>
                    <View>
                        <RadioButton
                            value="first"
                            color="#6C0AC7"
                            status={checkedUserList.includes(item.occupant_id) ? 'checked' : 'unchecked'}
                            onPress={() => handleCheckUserList(item.occupant_id)}
                        />
                    </View>
                    {/* <CheckBox
                    value={checkedUserList.includes(item.uuid)}
                    onValueChange={() => handleCheckUserList(item.uuid)}
                /> */}
                </View>
            </TouchableOpacity>
        </>
    }

    return (<>
        {deletedId == '' && deletedId !== item.uuid &&
            <View onPress={() => closeMenu()} key={item.uuid} style={{ paddingHorizontal: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, marginBottom: 10, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', width: '90%', alignItems: 'center' }}>
                        <View style={styles.profile_img}>
                            <Lightbox navigator={props.navigator} style={{ justifyContent: "center" }}
                                renderContent={() => (
                                    <Image
                                        source={{
                                            uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                                        }}
                                        style={{ alignSelf: "center", minWidth: 300, minHeight: 300 }}
                                    />
                                )}>
                                <Image
                                    style={styles.img}
                                    source={{
                                        uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                                    }}
                                />
                            </Lightbox>
                        </View>
                        <TouchableOpacity onPress={() => redirectToUserProfile(item.posted_by)}>
                            <View style={{ marginLeft: 15 }} >
                                <Text>{item.user}</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ marginTop: 3 }}>{item.first_name} {item.last_name && item.last_name}</Text>
                                    {props.likeSection ?
                                        <View style={{ marginLeft: 15 }}>
                                            <Text style={styles.label}>{check5Minute(item.created_at) ? 'was' : 'is currently'} loving:</Text>
                                        </View>
                                        :
                                        <View style={{ marginLeft: 20 }}>
                                            <Text style={styles.label}>{check5Minute(item.created_at) ? '' : 'currently'} dislikes:</Text>
                                        </View>
                                    }
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {props.userId != item.posted_by &&
                        <View style={{ marginRight: 15 }}>
                            <TouchableOpacity onPress={() => saveFollowData(item.posted_by)}>
                                <FIcon5 name="user-plus" size={20} color="#6c0ac7" />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
                {props.likeSection ?
                    <View style={{ marginHorizontal: 10 }}>
                        <Like isDownload={true} mediaUrl={item.media_url} mediaType={item.content_type} c_width={vw * 90} text={item.content} why={item.why_content} navigator={props.navigator} />
                    </View>
                    :
                    <View style={{ marginHorizontal: 10 }}>
                        <Dislike isDownload={true} mediaUrl={item.media_url} mediaType={item.content_type} c_width={vw * 90} text={item.content} why={item.why_content} navigator={props.navigator} />
                    </View>
                }
                <View style={{ marginHorizontal: 5, marginBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: 15, marginVertical: 10 }}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', maxWidth: vw * 60, paddingLeft: 5 }}>
                            {item && item.tagged_users && item.tagged_users.length > 0 && item.tagged_users.map((x, i) => {
                                if (showAll) {
                                    return <TouchableOpacity onPress={() => redirectToUserProfile(x.uuid)} style={{ marginRight: 10 }}>
                                        <Text style={{ color: 'blue' }}>{x.username}</Text>
                                    </TouchableOpacity>
                                } else {
                                    if (i < 2) {
                                        return <TouchableOpacity onPress={() => redirectToUserProfile(x.uuid)} style={{ marginRight: 10 }}>
                                            <Text style={{ color: 'blue' }}>{x.username}</Text>
                                        </TouchableOpacity>
                                    }
                                }
                            })}
                            {item && item.tagged_users && item.tagged_users.length > 0 && !showAll &&
                                <Text style={{ color: 'blue' }} onPress={()=>setShowAll(!showAll)}>...</Text>
                            }
                        </View>
                        <Text>{dateFormat(item.created_at)}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                        <View style={{ flexDirection: 'row' }}>
                            <FIcon onPress={() => handleFavourite(item, props.likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_favourite ? 'heart' : 'heart-o'} size={24} color="#6c0ac7" />
                            <Text>({numberFormatter(item.favourite_count, 1)})</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Icon onPress={() => handleLikeDisLike(item, "like")} style={{ marginRight: 5, marginTop: -3 }} name={item.is_like ? 'like1' : 'like2'} size={25} color="#6c0ac7" />
                            <Text>({numberFormatter(item.like_count, 1)})</Text>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Icon onPress={() => handleLikeDisLike(item, 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_dislike ? 'dislike1' : 'dislike2'} size={25} color="#6c0ac7" />
                            <Text>({numberFormatter(item.dislike_count, 1)})</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <FIcon onPress={() => handleComment(item, props.likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_comment ? 'comment' : 'comment-o'} size={25} color="#6c0ac7" />
                            <Text>({numberFormatter(item.comments_count, 1)})</Text>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <FIcon onPress={() => handleShare(item, props.likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: 0 }} name={true ? 'share-square' : 'share-square-o'} size={25} color="#6c0ac7" />
                            <Text>({numberFormatter(item.like_count, 1)})</Text>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <IonIcon onPress={() => handleSeen(item, props.likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_seen ? 'eye' : 'eye-outline'} size={27} color="#6c0ac7" />
                            <Text>({numberFormatter(item.seen_count, 1)})</Text>
                        </View>
                        <View style={{ position: 'relative' }}>
                            <MatIcon onPress={() => openMenu()} style={{ marginRight: 0, marginTop: 0 }} name={'dots-vertical'} size={27} color="#6c0ac7" />
                            {item.isMenuOpen && item.isMenuOpen &&
                                <View style={styles.editMenu}>
                                    {props.userId == item.posted_by ? <>
                                        <TouchableOpacity onPress={() => handleEdit()} style={styles.menuBox}>
                                            <Icon name={'edit'} size={RFPercentage(2.7)} color="#000" />
                                            <Text style={styles.menuText}>Edit</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => handleDelete()} style={styles.menuBox}>
                                            <Icon name={'delete'} size={RFPercentage(2.7)} color="#000" />
                                            <Text style={styles.menuText}>Delete</Text>
                                        </TouchableOpacity>
                                    </>
                                        :
                                        <TouchableOpacity onPress={() => handleReport()} style={styles.menuBox}>
                                            <Icon name={'warning'} size={RFPercentage(2.7)} color="#000" />
                                            <Text style={styles.menuText}>Report</Text>
                                        </TouchableOpacity>
                                    }
                                    <TouchableOpacity onPress={() => closeMenu()} style={styles.menuBox}>
                                        <Icon name={'close'} size={RFPercentage(2.7)} color="#000" />
                                        <Text style={styles.menuText}>Close</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    </View>
                </View>
            </View>
        }
        <AwesomeAlert
            show={alertEnable}
            showProgress={false}
            title={alertTitle}
            message={alertMesg}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="No"
            confirmText="Yes"
            cancelButtonColor='#28a745'
            confirmButtonColor="#6C0AC7"
            onCancelPressed={() => {
                setAlertEnable(false)
            }}
            onConfirmPressed={() => {
                deletePost();
                setAlertEnable(false);
            }}
            onDismiss={() => {
                setAlertEnable(false)
            }}
        />
        <Modal
            animated
            animationType="fade"
            visible={shareEnable}
            transparent
            onRequestClose={() => _handleDismiss()}>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={styles.container2}
            >
                <View style={styles.overlay}>
                    <Animated.View {...panResponder.panHandlers} style={[styles.container, { top }]}>
                        <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ width: vw * 10, height: 4, backgroundColor: '#e7e2e2', borderRadius: 7, marginTop: 7 }}>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ width: vw * 100, marginTop: 7 }}>
                                <TextInput placeholder="Search ..." value={text} onChangeText={(text) => handleOnchange(text)} style={{ height: 40, backgroundColor: '#e7e2e2' }} />
                            </View>
                        </View>
                        <FlatList
                            style={{ width: width }}
                            keyExtractor={(item, index) => index}
                            data={userList}
                            key={'usersection'}
                            // initialScrollIndex={likepostIndex}
                            renderItem={(item, index) => renderUserList(item, index)}
                            // onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            // ListFooterComponent={this.renderFooter.bind(this)}
                            ListFooterComponent={
                                likeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                        <View>
                            <TouchableOpacity >
                                <Text onPress={() => sendEnabled ? handleShareToUsers() : null} style={[styles.sendBtn, { backgroundColor: sendEnabled ? '#6C0AC7' : 'grey' }]}>Send</Text>
                            </TouchableOpacity>
                        </View>
                    </Animated.View>
                </View>
            </KeyboardAvoidingView>
        </Modal>
    </>
    )
}

const styles = StyleSheet.create({
    sendBtn: {
        textAlign: 'center',
        width: width,
        padding: 7,
        color: '#fff',
        fontSize: RFPercentage(2.7),
    },
    img: {
        width: 60,
        height: 60,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 60 / 2
    },
    label: {
        fontWeight: 'bold',
        color: '#000',
        fontStyle: 'italic',
        fontSize: RFPercentage(2.7),
        textAlign: 'center',
        marginBottom: 10
    },
    editMenu: {
        width: 150,
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: 999,
        bottom: 27,
        right: 0,
        borderRadius: 3,
        borderWidth: 0.5,
        borderColor: 'grey'
    },
    menuBox: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5
    },
    menuText: {
        fontSize: RFPercentage(2.7),
        padding: 5
    },
    overlay: {
        backgroundColor: 'rgba(0,0,0,0.2)',
        flex: 1,
        justifyContent: 'flex-end',
    },
    container2: {
        flex: 1,
    },
    container: {
        backgroundColor: 'white',
        borderTopRightRadius: 12,
        borderTopLeftRadius: 12,
        maxHeight: vh * 90
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    chat_box: {
        flexDirection: 'row',
        padding: 5,
        paddingLeft: 10,
        borderBottomColor: '#000',
        // borderBottomWidth: 0.5
    },
    name: {
        fontSize: RFPercentage(2),
        color: '#000'
    },
    username: {
        fontSize: RFPercentage(1.5),
        color: '#000'
    },
    msg: {
        fontSize: RFPercentage(2),
        color: '#444'
    },
    image_box: {
        width: 40,
    },
    profile_img_user: {
        width: 30,
        height: 30,
        borderWidth: 0.5,
        borderRadius: 70 / 2,
        borderColor: '#000',
        resizeMode: 'cover'
    },
    detail_box: {
        width: vw * 75,
    },
})
