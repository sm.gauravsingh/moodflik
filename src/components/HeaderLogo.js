import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View,Platform, Dimensions, Image } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Icon from 'react-native-vector-icons/Ionicons';
import DrawerMenu from './DrawerMenu';
const { width, height } = Dimensions.get('window');

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

export default function HeaderLogo(props) {

    return (
        <View style={{ width: '100%', backgroundColor: '#6C0AC7',  height: Platform.OS === 'ios' ? 100 : 80, flexDirection: 'column',paddingTop: Platform.OS === 'ios' ? 30 : 0, justifyContent: 'center' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                {props.previous &&
                    <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10,marginTop:10}} onPress={props.navigation.goBack}>
                        <Icon name={"arrow-back"} color="#fff" size={RFPercentage(5)} />
                    </TouchableOpacity>
                }
                <View style={{flexDirection:'row',justifyContent:'center'}}>
                    <Image
                        source={require('../assets/splash.png')}
                        style={{ height: 50,marginTop:10,marginLeft:-20, width: '100%', resizeMode: 'contain' }}
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    drawerMenu: {
        width: vw * 80,
        minHeight: 200,
        position: 'absolute',
        top: 30,
        right: 0,
        zIndex: 9999,
        backgroundColor: 'red'
    }
})
