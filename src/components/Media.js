import React from 'react'
import { StyleSheet, Text, View, Image, Dimensions, PermissionsAndroid, AlertIOS, Platform, ToastAndroid, Alert, TouchableOpacity } from 'react-native'
import VideoPlayer from 'react-native-video-player';
import { RFPercentage } from 'react-native-responsive-fontsize';
import Lightbox from 'react-native-lightbox';
import { Thumbnail } from 'react-native-thumbnail-video';
import RNFetchBlob from 'rn-fetch-blob';
import Icon from 'react-native-vector-icons/Ionicons';
import FIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AwesomeAlert from 'react-native-awesome-alerts';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
export default function Media(props) {
    const { type, url, mediaStyle, mediaWidth, isDownload, mediaHeight } = props
    const [isLoading, setIsLoading] = React.useState(true);
    const [showDownload, setShowDownload] = React.useState(false);
    const [duration, setDuration] = React.useState(0);

    const onLoadStart = (data) => setIsLoading(true);
    const onLoad = (data) => {
        setDuration(data.duration);
        setIsLoading(false);
    };

    const historyDownload = (fileUrl) => {
        //Function to check the platform
        //If iOS the start downloading
        //If Android then ask for runtime permission
        if (Platform.OS === 'ios') {
            downloadHistory(fileUrl);
        } else {
            try {
                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'storage title',
                        message: 'storage_permission',
                    },
                ).then(granted => {
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //Once user grant the permission start downloading
                        console.log('Storage Permission Granted.');
                        downloadHistory(fileUrl);
                    } else {
                        //If permission denied then show alert 'Storage Permission 
                        Alert.alert('storage_permission');
                    }
                });
            } catch (err) {
                //To handle permission related issue
                console.log('error', err);
            }
        }
    }

    const get_url_extension = (url) => {
        return url.split(/[#?]/)[0].split('.').pop().trim();
    }

    const getMimeType = async (fileUrl) => {
        return fetch(fileUrl)
            .then(response => {
                return response.blob().then(blob => {
                    return {
                        contentType: response.headers.get("Content-Type"),
                        raw: blob
                    }
                })
            })
            .then(data => {
                return data;
            });
    }

    const downloadHistory = async (fileUrl) => {
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir;
        let date = new Date();
        var fileExt = '';
        if (fileUrl.includes('quickblox.com')) {
            var type = await getMimeType(fileUrl);
            if (type != "") {
                var extension = type.contentType.split("/")
                fileExt = extension && extension.length && extension[1] ? extension[1] : '';
            }
        } else {
            if (get_url_extension(fileUrl) != '') {
                fileExt = get_url_extension(fileUrl);
            }
        }
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                //Related to the Android only
                useDownloadManager: true,
                notification: true,
                path:
                    PictureDir +
                    '/Moodflik-' +
                    Math.floor(date.getTime() + date.getSeconds() / 2) + "." + fileExt,
                description: 'Post File Download',
            },
        };
        if (fileExt != '') {
            config(options)
                .fetch('GET', fileUrl)
                .then((res) => {
                    //Showing alert after successful downloading
                    console.log('res -> ', JSON.stringify(res));
                    var msg = 'File Downloaded Successfully.';
                    if (Platform.OS === 'android') {
                        ToastAndroid.show(msg, ToastAndroid.SHORT)
                    } else {
                        AlertIOS.alert(msg);
                    }
                });
        } else {
            var msg = 'File is corrupted or invalid path.';
            if (Platform.OS === 'android') {
                ToastAndroid.show(msg, ToastAndroid.SHORT)
            } else {
                AlertIOS.alert(msg);
            }
        }
    }

    const getFileIcon = (fileUrl) => {
        var ext = get_url_extension(fileUrl)
        if (ext == 'pdf') {
            return 'file-pdf'
        } else if (ext == 'doc' || ext == 'docx') {
            return 'file-word'
        } else if (ext == 'xml') {
            return 'xml'
        } else {
            return 'file'
        }
    }

    const downloadImage = (type) => {
        setShowDownload(true)
    }

    const getMedia = (type, value) => {
        if (type == "image" || type == "gif" || type == "photo") {
            return <Lightbox navigator={props.navigator} onLongPress={() => isDownload ? downloadImage('Image') : null} style={{ justifyContent: "center" }}
                renderContent={() => (
                    <Image
                        source={{ uri: value }}
                        style={{ alignSelf: "center", minWidth: 300, minHeight: 300 }}
                    />
                )}>
                <Image style={[mediaStyle, { width: mediaWidth ? mediaWidth : '100%', minHeight: mediaHeight ? mediaHeight : 200 }]} source={{ uri: value }} />
            </Lightbox>
        } else if (type == "video") {
            return <View style={mediaStyle} >
                <VideoPlayer
                    video={{ uri: value }}
                    autoplay={true}
                    defaultMuted={true}
                    videoWidth={mediaWidth ? mediaWidth : vw * 90}
                    videoHeight={mediaHeight ? mediaHeight : 208}
                    loop={true}
                />
            </View>
        } else if (type == "thumbnail") {
            return <Thumbnail imageWidth={'100%'} url={value} />
        } else if (type == 'file') {
            return <TouchableOpacity style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#fff', padding: 5, borderRadius: 3, justifyContent: 'center', marginTop: 10 }} onPress={() => isDownload ? historyDownload(value) : null}>
                <FIcon name={getFileIcon(value)} size={RFPercentage(3)} color="#fff" />
                {isDownload && <>
                    <Text style={{ paddingLeft: 10, paddingRight: 10, color: '#fff', fontSize: RFPercentage(2.2) }}>Download</Text>
                    <Icon name="cloud-download-outline" size={RFPercentage(3)} color="#fff" />
                </>}
            </TouchableOpacity>
        } else {
            return <TouchableOpacity style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#fff', padding: 5, borderRadius: 3, justifyContent: 'center', marginTop: 10 }} onPress={() => isDownload ? historyDownload(value) : null}>
                <FIcon name={getFileIcon(value)} size={RFPercentage(3)} color="#fff" />
                {isDownload && <>
                    <Text style={{ paddingLeft: 10, paddingRight: 10, color: '#fff', fontSize: RFPercentage(2.2) }}>Download</Text>
                    <Icon name="cloud-download-outline" size={RFPercentage(3)} color="#fff" />
                </>}
            </TouchableOpacity>
        }
    }

    return (<>
        <View>
            {getMedia(type, url)}
        </View>
        <AwesomeAlert
            show={showDownload}
            showProgress={false}
            title={`Save`}
            message={``}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="Cancel"
            confirmText="Yes"
            cancelButtonColor='#28a745'
            confirmButtonColor="#6C0AC7"
            onCancelPressed={() => {
                setShowDownload(false)
            }}
            onConfirmPressed={() => {
                historyDownload(url)
                setShowDownload(false)
            }}
            onDismiss={() => {
                setShowDownload(false)
            }}
        />
    </>
    )
}

const styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
        justifyContent: 'center',
    },
})
