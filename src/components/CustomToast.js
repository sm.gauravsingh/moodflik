import React from 'react'
import { StyleSheet, Text, View, TouchableHighlight, Dimensions } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

export default function CustomToast(props) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{props.title}</Text>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <TouchableHighlight
                    style={{ padding: 10 }}
                    onPress={() => {
                        props.action();
                    }}>
                    <Text>{props.text}</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    style={{ padding: 10 }}
                    onPress={() => {
                        props.onCancel();
                    }}>
                    <Text>Close</Text>
                </TouchableHighlight>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 5,
        zIndex: 99,
        width: width - 40,
        marginHorizontal: 20,
        // backgroundColor: '#d1e7dd',
        backgroundColor: '#fff',
        borderRadius: 3,
        // borderColor: '#1ace79',
        borderColor: '#000',
        borderWidth: 0.3
    },
    title: {
        paddingLeft: 10,
        fontSize: RFPercentage(2.5),
        fontWeight: 'bold',
        color: '#000'
    }
})
