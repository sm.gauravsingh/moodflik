import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import AwesomeAlert from 'react-native-awesome-alerts';

export default function CommonAlert(props) {
    return (
        <AwesomeAlert
            show={props.showAlert}
            showProgress={false}
            title={props.title}
            message={props.message}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={props.cancelText && props.cancelText != '' ? true : false}
            showConfirmButton={true}
            cancelText={props.cancelText}
            confirmText={props.confirmText && props.confirmText != '' ? props.confirmText : 'OK'}
            cancelButtonColor='#28a745'
            confirmButtonColor={props.cancelText && props.cancelText != '' ? "#6C0AC7" : "#28a745"}
            onCancelPressed={() => {
                props.hideAlert(false)
            }}
            onConfirmPressed={() => {
                props.hideAlert(false)
            }}
            onDismiss={() => {
                props.hideAlert(false)
            }}
        />
    )
}

const styles = StyleSheet.create({})
