import React from 'react'
import { StyleSheet, Text, View, Dimensions,Image, Share } from 'react-native'
import CustomButton from '../components/CustomButton'
import { ScrollView } from 'react-native-gesture-handler'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { TouchableOpacity } from 'react-native';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
export default function Invite() {

    const onShare = async () => {
        console.log("sdsdsd")
        try {
            const result = await Share.share({
                message:
                    'https://www.moodflik.com/download',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    
    const onShareApp = async () => {
        console.log("sdsdsd")
        try {
            const result = await Share.share({
                message:
                    'https://www.moodflik.com/download',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };


    return (
        <ScrollView>
            <Text style={styles.heading}>Invite a friend</Text>
            <View style={{ marginHorizontal: 10 }}>
                <Text style={styles.para}>
                    Hey, check out Moodflik, a unique micro-blogging platform that allows you to see and share reactions to things and events you love and dislike in real-time, simultaneously.
                </Text>
                <Text style={styles.para}>
                    Join and follow me and many others today by getting the free app at :
                </Text>

                {/* <Text style={styles.link}>https://www.moodflik.com/download</Text> */}
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <View>
                        <TouchableOpacity onPress={onShare}>
                            <Image
                                style={{ height:100,width: vw * 50, resizeMode:'contain' }}
                                source={require('../assets/google.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={onShareApp}>
                            <Image
                                style={{  marginTop:23,height:55,width: vw * 50, resizeMode:'contain' }}
                                source={require('../assets/apple.png')}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
                {/* <View style={{ marginHorizontal: 80, marginBottom: 20 }}>
                    <CustomButton enabled={true} btnAction={() => onShare()} text={'Copy'} />
                </View> */}
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    heading: {
        textAlign: 'center',
        fontSize: RFPercentage(3),
        fontWeight: 'bold',
        marginVertical: 10
    },
    subHeading: {
        fontSize: RFPercentage(2.3),
        fontWeight: 'bold',
        marginVertical: 10
    },
    para: {
        lineHeight: 20,
        marginBottom: 15
    },
    link: {
        lineHeight: 20,
        marginBottom: 15,
        textDecorationLine: 'underline',
        color: '#03B4C6'
    }
})
