import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList, Text, View, Dimensions, Image, ActivityIndicator } from 'react-native'
import { RFPercentage } from 'react-native-responsive-fontsize';
import { HomeServices } from '../Services/Home.services';
import Loader from '../components/Loader'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Post from '../components/Post';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function MyPosts(props) {
    const [likeSection, setLikeSection] = useState(true);
    const [isLoader, setIsLoader] = useState(false);
    const [postData, setPostData] = useState([]);
    const [userId, setUserId] = useState('');
    const [isrefresh, setIsRefresh] = useState(false);
    const [postPagination, setPostPagination] = useState({});

    useEffect(() => {
        getUser();
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.check && props.route.params.refreshPage.check == 'userProfile') {
                if (props.route.params.refreshPage.type == 'like') {
                    getPostStats(true,props.route.params.refreshPage.user_id);
                    setLikeSection(true);
                } else {
                    getPostStats(false,props.route.params.refreshPage.user_id);
                    setLikeSection(false);
                }
            } else {
                if (props.route.params.refreshPage.type == 'like') {
                    getPostStats(true,"");
                    setLikeSection(true);
                } else {
                    getPostStats(false,"");
                    setLikeSection(false);
                }
            }
        }
    }, [props]);

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    var getPostStats = async (type,id) => {
        setIsLoader(true)
        if (type) {
            await homeServices.getMyPosts('like',id).then(
                (data) => {
                    setIsLoader(false)
                    console.log("pp", data)
                    if (data.results) {
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setPostPagination(pagination)
                        setPostData(data.results)
                    }
                },
                (error) => {
                    setIsLoader(false)
                    console.log("error.response.status", error);
                }
            );
        } else {
            await homeServices.getMyPosts('dislike',id).then(
                (data) => {
                    setIsLoader(false)
                    if (data.results) {
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setPostPagination(pagination)
                        setPostData(data.results)
                    }
                },
                (error) => {
                    setIsLoader(false)
                    console.log("error.response.status", error);
                }
            );
        }
    }

    var getLoadMoreData = async (url) => {
        await homeServices.getLoadMoreData(url).then(
            (data) => {
                if (data.results) {
                    setIsLoader(false)
                    setPostData([...postData, ...data.results]);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setPostPagination(pagination)
                    setIsRefresh(false)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const loadMoreData = () => {
        if (postPagination && postPagination.next && postPagination.next != "") {
            getLoadMoreData(postPagination.next);
            setIsRefresh(true)
        }
    }

    const renderPost = ({ item, index }) => {
        return <Post navigator={props.navigator} userId={userId} navigation={props.navigation} likeSection={likeSection} post={item} />
    }

    return (<>
        {isLoader &&
            <Loader />
        }
        <View style={{ width: width }}>
            {postData && postData.length > 0 ?
                <FlatList
                    style={{ width: width, }}
                    keyExtractor={(item, index) => index}
                    data={postData}
                    renderItem={(item, index) => renderPost(item, index)}
                    onEndReached={() => loadMoreData()}
                    onEndReachedThreshold={0.9}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    ListFooterComponent={
                        isrefresh &&
                        <ActivityIndicator color="#6C0AC7" />
                    }
                />
                :
                <Text style={{ textAlign: 'center', fontSize: RFPercentage(3), fontStyle: 'italic', color: 'grey', marginTop: 50 }}>No Post Found</Text>
            }
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        paddingHorizontal: 5,
        height: height
    },
    img: {
        width: 60,
        height: 60,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 60 / 2
    },
    label: {
        fontWeight: 'bold',
        color: '#000',
        fontStyle: 'italic',
        fontSize: RFPercentage(2.7),
        textAlign: 'center',
        marginBottom: 10
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
})
