import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, Image, FlatList, ActivityIndicator } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { HomeServices } from '../Services/Home.services';
import Lightbox from 'react-native-lightbox';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import UserToBlock from './UserToBlock';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();
export default function BlockUser(props) {
    const [blockedUserList, setBlockedUserList] = useState([]);

    useEffect(() => {
        getBlockedUserList();
    }, []);

    var getBlockedUserList = async () => {
        await homeServices.getBlockedUserList().then(
            (data) => {
                if (data) {
                    setBlockedUserList(data);
                    console.log("ppp-", data)
                }
            },
            (error) => {
                console.log("error.response.status", error);
            });
    }

    const checkUsername = (val) => {
        if (val.includes("@")) {
            return val;
        } else {
            return '@' + val;
        }
    }

    const handleUnblock = async(uid) => {
        await homeServices.unblockUser(uid).then(
            (data) => {
                if (data) {
                    getBlockedUserList();
                }
            },
            (error) => {
                console.log("error.response.status", error);
            });
    }

    const renderItem = ({ item, index }) => {
        return <View>
            <View style={{ flexDirection: 'row', width: '90%', alignItems: 'center', padding: 10, justifyContent: 'space-between' }}>
                <View style={{ width: vw * 15 }}>
                    <Lightbox navigator={props.navigator} style={{ justifyContent: "center" }}
                        renderContent={() => (
                            <Image
                                source={{
                                    uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                                }}
                                style={{ alignSelf: "center", minWidth: 300, minHeight: 300 }}
                            />
                        )}>
                        <Image
                            style={styles.img}
                            source={{
                                uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                            }}
                        />
                    </Lightbox>
                </View>
                {/* <TouchableOpacity onPress={() => redirectToUserProfile(item.uuid)}> */}
                <View style={{ marginLeft: 20, width: vw * 55 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: RFPercentage(2) }}>{checkUsername(item.username)}</Text>
                    <Text style={{ fontSize: RFPercentage(2.7) }}>{item.first_name} {item.last_name && item.last_name}</Text>
                </View>
                {/* </TouchableOpacity> */}
                {/* {item.is_following ?
                    <View style={{ marginRight: 15, width: vw * 15 }}>
                        <TouchableOpacity onPress={() => handleUnfollow(item.uuid)}>
                            <FIcon5 name="user-minus" size={20} color="#6c0ac7" />
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{ marginRight: 15, width: vw * 15 }}>
                        <TouchableOpacity onPress={() => saveFollowData(item.uuid)}>
                            <FIcon5 name="user-plus" size={20} color="#6c0ac7" />
                        </TouchableOpacity>
                    </View>
                } */}
                <View style={{ marginRight: 15, width: vw * 15 }}>
                    <TouchableOpacity onPress={() => handleUnblock(item.uuid)}>
                        <Text>Unblock</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    }

    

    const userToBlock = () => {
        props.navigation.navigate('Search',{
            refreshPage:{
                refresh : Math.random(),
                type : 'block'
            },
        })
    }

    useEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.type) {
                setFollowType(props.route.params.refreshPage.type);
                setUserLists(props.route.params.refreshPage.list);
            }
        }
    }, [props]);


    return (
        <ScrollView>
            <View style={{ marginHorizontal: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                        <MaterialIcons name={'block'} style={styles.icon} />
                    </View>
                    <Text style={styles.heading}>Block User</Text>
                </View>
                <View style={{ marginHorizontal: 10 }}>
                    <TouchableOpacity onPress={()=>userToBlock()}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                                <Icon name={'person-add'} style={styles.icon2} />
                            </View>
                            <Text style={styles.subHeading}>Add blocked user</Text>
                        </View>
                    </TouchableOpacity>
                    <Text>Blocked Users:</Text>
                    <View>
                        {blockedUserList && blockedUserList.length > 0 ?
                            <FlatList
                                style={{ width: width, marginBottom: 120 }}
                                keyExtractor={(item, index) => index}
                                data={blockedUserList}
                                renderItem={(item, index) => renderItem(item, index)}
                                // onEndReached={() => loadMoreData()}
                                // onEndReachedThreshold={0.9}
                                ItemSeparatorComponent={() => <View style={styles.separator} />}
                            // ListFooterComponent={
                            //     isrefresh &&
                            //     <ActivityIndicator color="#6C0AC7" />
                            // }
                            />
                            :
                            <Text style={{ textAlign: 'center' }}>No user found</Text>
                        }
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    heading: {
        fontSize: RFPercentage(3),
        fontWeight: 'bold',
        marginVertical: 10,
    },
    icon: {
        textAlign: 'center',
        fontSize: RFPercentage(4),
        color: '#000',
        marginRight: 10,
    },
    icon2: {
        textAlign: 'center',
        fontSize: RFPercentage(3),
        color: '#000',
        marginRight: 10,
    },
    subHeading: {
        fontSize: RFPercentage(2.3),
        fontWeight: 'bold',
        marginVertical: 10
    },
    para: {
        lineHeight: 20,
        marginBottom: 15
    },
    img: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2
    },
    separator:{
        borderBottomColor:'#000',
        borderBottomWidth:0.5
    }
})
