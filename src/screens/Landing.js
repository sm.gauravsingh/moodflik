import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, TouchableOpacity } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Loader from '../components/Loader'
import CustomButton from '../components/CustomButton'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import Icon from 'react-native-vector-icons/AntDesign';
import FIcon from 'react-native-vector-icons/FontAwesome';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import FEIcon from 'react-native-vector-icons/Feather';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MatIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Platform } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

export default function Landing({ navigation }) {
    const [isLoader, setIasLoader] = useState(false);
    const [trendingReactionsOpen, setTrendingReactionsOpen] = useState(false);
    const [likeSection, setLikeSection] = useState(true);
    const [showJoinAlert, setShowJoinAlert] = useState(false);
    const [likesCount, setLikesCount] = useState(0);
    const [dislikesCount, setDislikesCount] = useState(0);
    const [trendingLikePost, setTrendingLikePost] = useState([]);
    const [trendingDislikePost, setTrendingDisLikePost] = useState([]);

    useEffect(() => {
        getTrendingLikePost();
        getTrendingDisLikePost();
    }, []);

    const getTrendingLikePost = () => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "token 43e153506412cc9b16722b7885a041b74b9d7972");
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://t87goxe24a.execute-api.ap-south-1.amazonaws.com/mood_dev/api/most_seen/?type=like", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.results) {
                    setTrendingLikePost(result.results);
                    setLikesCount(result.count);
                }
            })
            .catch(error => console.log('error', error));
    }

    const getTrendingDisLikePost = () => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "token 43e153506412cc9b16722b7885a041b74b9d7972");
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://t87goxe24a.execute-api.ap-south-1.amazonaws.com/mood_dev/api/most_seen/?type=dislike", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.results) {
                    setTrendingDisLikePost(result.results);
                    setDislikesCount(result.count);
                }
            })
            .catch(error => console.log('error', error));
    }

    const numberFormatter = (num, digits) => {
        const lookup = [
            { value: 1, symbol: "" },
            { value: 1e3, symbol: "k" },
            { value: 1e6, symbol: "M" },
            { value: 1e9, symbol: "G" },
            { value: 1e12, symbol: "T" },
            { value: 1e15, symbol: "P" },
            { value: 1e18, symbol: "E" }
        ];
        const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var item = lookup.slice().reverse().find(function (item) {
            return num >= item.value;
        });
        return item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
    }

    const dateFormat = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        return result;
    }
    const check5Minute = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        if (result.includes('seconds ago')) {
            return false;
        } else if (result.includes('minutes ago')) {
            let firstWord = result.split(" ")[0]
            var check = parseInt(firstWord.trim());
            if (check > 4) {
                return true;
            } else {
                return false;
            }
        } else if (!result.includes('seconds ago')) {
            return true;
        } else {
            return false;
        }
    }
    const timeDifference = (current, previous) => {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        var elapsed = current - previous;
        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        }
        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }
        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        }
        else if (elapsed < msPerMonth) {
            if (Math.round(elapsed / msPerDay) > 1) {
                return Math.round(elapsed / msPerDay) + ' days ago';
            } else {
                return Math.round(elapsed / msPerDay) + ' day ago';
            }
        }
        else if (elapsed < msPerYear) {
            if (Math.round(elapsed / msPerMonth) > 1) {
                return Math.round(elapsed / msPerMonth) + ' months ago';
            } else {
                return Math.round(elapsed / msPerMonth) + ' month ago';
            }
        }
        else {
            if (Math.round(elapsed / msPerYear) > 1) {
                return Math.round(elapsed / msPerYear) + ' years ago';
            } else {
                return Math.round(elapsed / msPerYear) + ' year ago';
            }
        }
    }

    const handleOnPress = () => {
        setShowJoinAlert(true);
    }

    return (
        <ScrollView style={styles.main}>
            {isLoader &&
                <Loader />
            }
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ width: vw * 90 }}>
                    <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: RFPercentage(2.7), marginTop: 10 }}>Welcome to Moodflik, Where you can:</Text>
                    <View>
                        <View style={styles.row}>
                            <View style={{ width: '20%', flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                <Icon name="heart" size={RFPercentage(3.5)} color="#6C0AC7" />
                                <View style={styles.verticleBar}></View>
                                <Icon name="dislike1" style={{ marginRight: 10 }} size={RFPercentage(3.5)} color="#6C0AC7" />
                            </View>
                            <View style={{ width: '80%' }}>
                                <Text>
                                    See, share and speak about daily events and experiences of things you <Text style={{ color: 'green', fontStyle: 'italic' }}>love</Text> and <Text style={{ color: 'red', fontStyle: 'italic' }}>dislike</Text> simultaneously.
                                </Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{ width: '20%', marginTop: 5 }}>
                                <FIcon name="user-plus" size={RFPercentage(4)} color="#6C0AC7" />
                            </View>
                            <View style={{ width: '80%' }}>
                                <Text>
                                    Find and follow your favourite people, places and organisations with similar interests and dislikes.
                                </Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{ width: '20%', marginTop: 5, position: 'relative' }}>
                                <MIcon name="chat-outline" size={RFPercentage(5)} color="#6C0AC7" />
                                <FEIcon name="hash" style={{ position: 'absolute', top: RFPercentage(1.2), left: RFPercentage(1.5) }} size={RFPercentage(2)} color="#6C0AC7" />
                            </View>
                            <View style={{ width: '80%' }}>
                                <Text>
                                    Join live hot trending topics of events happening locally and globally with diverse reactions.
                                </Text>
                            </View>
                        </View>
                    </View>
                    <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: RFPercentage(2.5), marginTop: 10 }}>No account? No problem. Jump on board in seconds!</Text>

                    <View>
                        <CustomButton text="Sign Up" enabled={true} btnAction={() => navigation.navigate('SignUp')} btnWidth={80} />
                        <CustomButton text="Sign In" enabled={true} btnAction={() => navigation.navigate('Login')} btnWidth={80} invertColour={'#fff'} textColor="#6C0AC7" isBorder={true} />
                        <CustomButton text="See Trending Reactions" btnAction={() => setTrendingReactionsOpen(!trendingReactionsOpen)} enabled={true} btnWidth={80} isIcon={true} icon={<View style={{ position: 'relative' }}>
                            <MIcon name="chat-outline" size={RFPercentage(5)} color="#fff" />
                            <FEIcon name="hash" style={{ position: 'absolute', top: RFPercentage(1.2), left: RFPercentage(1.5) }} size={RFPercentage(2)} color="#fff" />
                        </View>} />
                    </View>
                    {trendingReactionsOpen &&
                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <CustomButton text={`Public Likes(${likesCount})`} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={44} invertColour={!likeSection ? '#6C0AC7' : '#108A07'} textColor="#fff" isIcon={true} icon={<Icon name="heart" size={Platform.OS == 'ios' ? RFPercentage(2.2) : RFPercentage(2.3)} color="#fff" />} textAlign={"left"} />
                                <CustomButton text={`Public Dislikes(${dislikesCount})`} invertColour={likeSection ? '#6C0AC7' : '#BF1414'} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={44} isIcon={true} icon={<Icon name="dislike1" size={Platform.OS == 'ios' ? RFPercentage(2.2) : RFPercentage(2.3)} color="#fff" />} textAlign={"left"} />
                            </View>
                            <View style={{ marginTop: 15, marginBottom: 20 }}>
                                {likeSection ?
                                    <View>
                                        {trendingLikePost && trendingLikePost.map((x, i) => {
                                            return <View style={{ marginBottom: 10, borderBottomColor: '#ddd', borderBottomWidth: 0.7 }}>
                                                <Like isDownload={false} mediaUrl={x.media_url} mediaType={x.content_type} c_width={vw * 90} text={x.content} why={x.why_content} />
                                                <View style={{ marginHorizontal: 5, marginBottom: 10 }}>
                                                    <View style={{ alignSelf: 'flex-end', marginRight: 15, marginVertical: 10 }}>
                                                        <Text>{dateFormat(x.created_at)}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <FIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_favourite ? 'heart' : 'heart-o'} size={24} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.favourite_count, 1)})</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -3 }} name={x.is_like ? 'like1' : 'like2'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.like_count, 1)})</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_dislike ? 'dislike1' : 'dislike2'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.dislike_count, 1)})</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <FIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_comment ? 'comment' : 'comment-o'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.comments_count, 1)})</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row' }}>
                                                            <FIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: 0 }} name={false ? 'share-square' : 'share-square-o'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.like_count, 1)})</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row' }}>
                                                            <IonIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_seen ? 'eye' : 'eye-outline'} size={27} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.seen_count, 1)})</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        })}
                                    </View>
                                    :
                                    <View>
                                        {trendingDislikePost && trendingDislikePost.map((x, i) => {
                                            return <View style={{ marginBottom: 10, borderBottomColor: '#ddd', borderBottomWidth: 0.7 }}>
                                                <Dislike isDownload={false} mediaUrl={x.media_url} mediaType={x.content_type} c_width={vw * 90} text={x.content} why={x.why_content} />
                                                <View style={{ marginHorizontal: 5, marginBottom: 10 }}>
                                                    <View style={{ alignSelf: 'flex-end', marginRight: 15, marginVertical: 10 }}>
                                                        <Text>{dateFormat(x.created_at)}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <FIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_favourite ? 'heart' : 'heart-o'} size={24} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.favourite_count, 1)})</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -3 }} name={x.is_like ? 'like1' : 'like2'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.like_count, 1)})</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_dislike ? 'dislike1' : 'dislike2'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.dislike_count, 1)})</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <FIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_comment ? 'comment' : 'comment-o'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.comments_count, 1)})</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row' }}>
                                                            <FIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: 0 }} name={false ? 'share-square' : 'share-square-o'} size={25} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.like_count, 1)})</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row' }}>
                                                            <IonIcon onPress={() => handleOnPress()} style={{ marginRight: 5, marginTop: -2 }} name={x.is_seen ? 'eye' : 'eye-outline'} size={27} color="#6c0ac7" />
                                                            <Text>({numberFormatter(x.seen_count, 1)})</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        })}
                                    </View>
                                }
                            </View>
                        </View>
                    }
                </View>
            </View>
            <AwesomeAlert
                show={showJoinAlert}
                showProgress={false}
                title={'Register First'}
                message={'Please Sign In to use this feature.'}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={true}
                showCancelButton={false}
                showConfirmButton={true}
                cancelText="OK"
                confirmText="OK"
                cancelButtonColor='#28a745'
                confirmButtonColor="#6C0AC7"
                onCancelPressed={() => {
                    setShowJoinAlert(false)
                }}
                onConfirmPressed={() => {
                    setShowJoinAlert(false);
                }}
                onDismiss={() => {
                    setShowJoinAlert(false)
                }}
            />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        marginTop: 15
    },
    verticleBar: {
        height: RFPercentage(3.5),
        backgroundColor: '#6C0AC7',
        width: 3
    }
})
