import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Alert, ScrollView, Modal, ToastAndroid, Platform, AlertIOS, Dimensions, CheckBox, TouchableOpacity } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Loader from '../components/Loader'
import CustomButton from '../components/CustomButton'
import CustomInput from '../components/CustomInput'
import DatePicker from 'react-native-date-picker'
import { Picker } from '@react-native-picker/picker'
import { DatePickerModal } from 'react-native-paper-dates';
import AwesomeAlert from 'react-native-awesome-alerts';
import 'intl';
import 'intl/locale-data/jsonp/en';
import Notify from '../components/Notify'

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

const errors = {
    fname: "",
    lname: "",
    username: "",
    email: "",
    password: "",
    dob: "",
    gender: ""
}

export default function SignUp({ navigation }) {
    const [isLoader, setIsLoader] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [fname, setFname] = useState("");
    const [lname, setLname] = useState("");
    const [username, setUsername] = useState("");
    const [gender, setGender] = useState("");
    const [isChecked, setIsChecked] = useState(false);
    const [dob, setDob] = useState("");
    const [date, setDate] = useState("Date of Birth");
    const [showCalendar, setShowCalendar] = useState(false);
    const [checkBoxError, setCheckBoxError] = useState("");
    const [error, setError] = useState(errors);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMsg, setAlertMsg] = useState('');

    const activateCalendar = (val) => {
        setShowCalendar(true)
    }

    useEffect(() => {
        if (isChecked) {
            setCheckBoxError("");
        }
    }, [isChecked]);

    const alertCalendar = () =>
        Alert.alert(
            "Alert Title",
            <DatePicker
                date={dob}
                onDateChange={setDob}
                mode="date"
            />,
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
        );

    const onDismissSingle = React.useCallback(() => {
        setShowCalendar(false);
    }, [showCalendar]);

    const onConfirmSingle = React.useCallback(
        (params) => {
            setShowCalendar(false);
            setDob(params.date);
            setDate(convertDate(params.date));
        },
        [showCalendar, dob]
    );

    const convertDate = (date) => {
        var d = new Date(date);
        var t = d.getDate();
        var n = d.getMonth();
        var y = d.getFullYear();
        var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        return t + " " + mlist[n] + " " + y
    }

    const handleSignUp = () => {
        if (isChecked) {
            setCheckBoxError('');
            var data = {
                first_name: fname,
                last_name: lname,
                username: username,
                email: email,
                password: password,
                date_of_birth: dob && dob != "" && getDob(dob),
                gender: gender,
                terms_confirmed: isChecked ? 1 : 0
            }
            var check = validateData();
            if (check.fname == "" && check.lname == "" && check.username == "" && check.email == "" && check.password == "" && check.dob == "" && check.gender == "") {
                var baseUrl = "https://t87goxe24a.execute-api.ap-south-1.amazonaws.com/mood_dev/"
                const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                };
                setIsLoader(true)
                console.log(requestOptions);
                fetch(baseUrl + 'user/', requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        setIsLoader(false)
                        if (data.status) {
                            NotifyMesg("User registered successfully.");
                            navigation.navigate("Login")
                        } else {
                            if (data.date_of_birth) {
                                setAlertMsg('You must be aged 13 or over to sign up.')
                                setShowAlert(true)
                            }
                            if (data.username) {
                                setAlertMsg('A user with this username already exists.')
                                setShowAlert(true)
                            }
                            if (data.message == "User already present") {
                                setAlertMsg('This email already exists.')
                                setShowAlert(true)
                            }else{
                                setAlertMsg('This email already exists.')
                                setShowAlert(true)
                            }
                        }
                    })
                    .catch(function (error) {
                        // NotifyMesg(error);
                        setIsLoader(false)
                        console.log("errror", error)
                    });
            }
        } else {
            setCheckBoxError("Please check the box to proceed.");
        }
    }

    const getDob = (date) => {
        var d = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
        var m = date.getMonth() > 9 ? date.getMonth() : "0" + date.getMonth();
        var y = date.getFullYear();
        var newDate = y + "-" + m + "-" + d;
        return newDate;
    }

    const validateData = () => {
        var errors = {
            fname: "",
            lname: "",
            username: "",
            email: "",
            password: "",
            dob: "",
            gender: ""
        }
        if (fname == "") {
            errors.fname = "Please enter first name";
        } else if (!/^[a-zA-Z ]+$/.test(fname)) {
            errors.fname = "Please enter a valid first name";
        } else {
            errors.fname = "";
        }
        if (lname == "") {
            errors.lname = "Please enter last name";
        } else if (!/^[a-zA-Z ]+$/.test(lname)) {
            errors.lname = "Please enter a valid last name";
        } else {
            errors.lname = "";
        }
        if (username == "") {
            errors.username = "Please enter a username";
        } else if (!/^[a-zA-Z0-9]+$/.test(username)) {
            errors.username = "Please enter a valid username";
        } else {
            errors.username = "";
        }
        if (email == "") {
            errors.email = "Please enter a email";
        } else if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            errors.email = "Please enter a valid email";
        } else {
            errors.email = "";
        }
        if (password == "") {
            errors.password = "Please enter a password";
        } else if (!/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(password)) {
            errors.password = "Password should be between 6-16 characters including small and capital letters, number(s) and special characters.";
        } else {
            errors.password = "";
        }
        if (dob == "") {
            errors.dob = "Please select Date of Birth";
        } else {
            errors.dob = "";
        }
        if (gender == "") {
            errors.gender = "Please select Gender";
        } else {
            errors.gender = "";
        }
        setError(errors);
        return errors;
    }

    const NotifyMesg = (msg) => {
        if (Platform.OS === 'android') {
            ToastAndroid.show(msg, ToastAndroid.SHORT)
        } else {
            AlertIOS.alert(msg);
        }
    }

    const switchScreen = (screen) => {
        navigation.navigate(screen,{
            refreshPage:{
                refresh : Math.random()
            },
        })
    }

    return (
        <ScrollView style={styles.main}>
            {isLoader &&
                <Loader />
            }
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ width: vw * 90, minHeight: vh * 85 }}>
                    <View style={{ flexDirection: 'column', height: "100%", justifyContent: 'center' }}>
                        <View style={{ marginBottom: 20 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: RFPercentage(3), textAlign: 'center', marginBottom: 20 }}>Sign Up</Text>
                            {error.fname != "" && <Text style={{ color: 'red' }}>{error.fname}</Text>}
                            <CustomInput value={fname} action={(text) => setFname(text)} placeHolder={"First Name"} />
                            {error.lname != "" && <Text style={{ color: 'red' }}>{error.lname}</Text>}
                            <CustomInput value={lname} action={(text) => setLname(text)} placeHolder={"Last Name"} />
                            {error.username != "" && <Text style={{ color: 'red' }}>{error.username}</Text>}
                            <CustomInput value={username} action={(text) => setUsername(text)} placeHolder={"Username"} />
                            {error.email != "" && <Text style={{ color: 'red' }}>{error.email}</Text>}
                            <CustomInput value={email} action={(text) => setEmail(text)} placeHolder={"Email address"} />
                            {error.password != "" && <Text style={{ color: 'red' }}>{error.password}</Text>}
                            <CustomInput value={password} action={(text) => setPassword(text)} placeHolder={"Password"} secureTextEntry={true} />
                            {error.dob != "" && <Text style={{ color: 'red' }}>{error.dob}</Text>}
                            <TouchableOpacity style={styles.textBox} onPress={() => setShowCalendar(!showCalendar)}>
                                <Text>{date}</Text>
                            </TouchableOpacity>
                            <DatePickerModal
                                // locale={'en'} optional, default: automatic
                                mode="single"
                                visible={showCalendar}
                                onDismiss={() => onDismissSingle()}
                                date={dob}
                                onConfirm={(params) => onConfirmSingle(params)}
                                // validRange={{
                                //   startDate: new Date(2021, 1, 2),  // optional
                                //   endDate: new Date(), // optional
                                // }}
                                // onChange={} // same props as onConfirm but triggered without confirmed by user
                                // saveLabel="Save" // optional
                                label="Select date of birth" // optional
                            // animationType="slide" // optional, default is 'slide' on ios/android and 'none' on web
                            />
                            {error.gender != "" && <Text style={{ color: 'red' }}>{error.gender}</Text>}
                            <View style={styles.picker}>
                                <Picker
                                    selectedValue={gender}
                                    style={styles.picker}
                                    onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
                                    prompt={"Gender"}
                                // mode={'dropdown'}
                                >
                                    <Picker.Item label="Select Gender" value="" />
                                    <Picker.Item label="Male" value="M" />
                                    <Picker.Item label="Female" value="F" />
                                    <Picker.Item label="Prefer not to say" value="NA" />
                                </Picker>
                            </View>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 15 }}>
                                <Text style={{ fontSize: RFPercentage(1.7), marginRight: 10, marginTop: 1 }}>
                                    By ticking the box, you are agreeing with our
                                </Text>
                                <TouchableOpacity onPress={()=>switchScreen('TermsConditions')}>
                                    <Text style={{ fontSize: RFPercentage(2), color: '#03B4C6', marginRight: 10 }}>
                                        Terms and conditions
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>switchScreen('Policy')}>
                                    <Text style={{ fontSize: RFPercentage(2), color: '#03B4C6', marginRight: 10 }}>
                                        Policy and Data Use
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>switchScreen('CommunityGuidelines')}>
                                    <Text style={{ fontSize: RFPercentage(2), color: '#03B4C6', marginRight: 10 }}>
                                        Community guidelines
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>switchScreen('Cookies')}>
                                    <Text style={{ fontSize: RFPercentage(2), color: '#03B4C6', marginRight: 10 }}>
                                        Cookie use
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.checkboxContainer}>
                                <CheckBox
                                    value={isChecked}
                                    onValueChange={setIsChecked}
                                    style={[styles.checkbox, { color: 'red' }]}
                                />
                                <Text style={styles.label}>Agree & Create Account</Text>
                            </View>
                            {checkBoxError != "" &&
                                <Text style={{ color: 'red', textAlign: 'center' }}>{checkBoxError}</Text>
                            }
                            <CustomButton text="Confirm Sign Up" enabled={true} btnAction={() => handleSignUp()} btnWidth={60} />

                        </View>
                    </View>
                </View>
            </View>
            <AwesomeAlert
                show={showAlert}
                showProgress={false}
                // title={`Choose Image`}
                message={alertMsg}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={true}
                showCancelButton={false}
                showConfirmButton={true}
                cancelText="Cancel"
                confirmText="OK"
                cancelButtonColor='#28a745'
                confirmButtonColor="#6C0AC7"
                onCancelPressed={() => {
                    setShowAlert(false)
                }}
                onConfirmPressed={() => {
                    setShowAlert(false)
                }}
                onDismiss={() => {
                    setShowAlert(false)
                }}
            />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        marginTop: 15
    },
    picker: {
        borderWidth: 1,
        borderColor: '#aaa',
        borderRadius: 10
    },
    checkboxContainer: {
        flexDirection: "row",
        justifyContent: 'center'
    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin: 8,
    },
    textBox: {
        borderWidth: 1,
        borderColor: '#aaa',
        borderRadius: 10,
        padding: 16,
        marginBottom: vh * 2,
    }
})
