import React from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
export default function Help() {
    return (
        <ScrollView>
            <Text style={styles.heading}>Help</Text>
            <View style={{ marginHorizontal: 10 }}>
                <Text style={styles.subHeading}>FAQs</Text>
                <Text style={styles.subHeading}>Q – How do I control my daily notifications for posts and follower requests?</Text>
                <Text>A – Go to the menu and click on the notification settings to adjust the notifications you receive from the web and mobile app.</Text>
                <Text style={styles.subHeading}>Q – I do not want my posts and Bio to be made public how do I control these?</Text>
                <Text>A – Navigate to the menu and click on the privacy settings and then select the type of users that you are comfortable with viewing your posts and bio</Text>
                <Text style={styles.subHeading}>Q – If I block public members from seeing my posts, does that mean I cannot also see their posts?</Text>
                <Text>A – You will still be able to view other people’s public posts if they have not restricted a non-following user from viewing them.</Text>
                <Text style={styles.subHeading}>Q – Why was my mobile number requested when I signed up?</Text>
                <Text>A – We requested for your mobile to help you and help us too. Providing your mobile gives you a secondary option of logging in, in case you forgot your username or email address. It always helps you to easily connect with your contacts who may also be on moodflik. Having your mobile number also helps us to protect your account from potential hacking as can send you specifically generated codes via SMS message to verify your identity and we can also send you activation codes when logging in for the first time.</Text>
                <Text style={styles.subHeading}>Q – What part of my profile information will not be visible to other users (including my followers and those I am following?)</Text>
                <Text>A – Your mobile number will be kept private and only visible to you.</Text>
                <Text style={styles.subHeading}>Q – How do I delete my account?</Text>
                <Text>A – You can delete your account by going to your account and profile settings and right at the bottom there is a button titled “Delete Account”.</Text>
                <Text style={styles.subHeading}>Q – How do I block a user?</Text>
                <Text>A – Go to the menu and find the option to “Block user” towards the bottom of the list. Then you can simply search for the user in question, when found click on block and that user will no longer be able to view or make or send any posts to you going forward.</Text>

                <Text>Please note that the user will NOT be alerted to this and you may also be unblock them later should you wish by following the same process and this time finding the user on the list of blocked users and click unblock.</Text>
                <Text style={styles.subHeading}>Flag and Report</Text>
                <Text>The following are unwelcome activities on the moodflik platform:</Text>
                <Text>
                    Bullying
                    Grooming
                    Spamming
                    False advertisement
                    Fake or under-aged accounts (under the age of 13)
                    False and/ disparaging posts or comments about other members or moodflik
                    Inciting of hate and violence or discrimination

                    Any of these activities are to be brought to our attention with immediate effect in order for us to investigate and take due action on such persons.
            </Text>
                <Text style={styles.subHeading}>Experiencing technical issues?</Text>
                <Text>If you come across or are experiencing any technical glitch or bug on the web application or mobile app, please send us a detailed report to info@moodflik.com. Please provide as much information as you can and be clear on your description. You may also attach pictures or videos to support your findings.</Text>
                <Text style={styles.subHeading}>Feedback</Text>
                <Text>We here at Moodflik are constantly growing and seeking to further improve on our service so if you have found something currently on our platform which you think could be done better or maybe done away with, or you just have some great ideas for to consider then please do not hesitate to also contact us on  info@moodflik.com</Text>

                <Text>If for whatever reason, we have not covered or answered your questions on our FAQs section then please do not hesitate to use our “Contact Us” page to reach out to us. We would love to hear from you.</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    heading: {
        textAlign: 'center',
        fontSize: RFPercentage(3),
        fontWeight: 'bold',
        marginVertical: 10
    },
    subHeading: {
        fontSize: RFPercentage(2.3),
        fontWeight: 'bold',
        marginVertical: 10
    },
    para: {
        lineHeight: 20,
        marginBottom: 15
    }
})
