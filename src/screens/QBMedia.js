import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native'
import QB from "quickblox-react-native-sdk"
import Media from '../components/Media';

export default function QBMedia(props) {
    const [url, setUrl] = useState('');

    useEffect(() => {
        getQBContent(props.id)
    }, [props]);

    const getQBContent = async (mediaId) => {
        const params = { id: parseInt(mediaId) };
        QB.content
            .getInfo(params)
            .then((file) => {
                const getUrlParam = { uid: file.uid };
                QB.content
                    .getPrivateURL(getUrlParam)
                    .then(function (url) {
                        setUrl(url);
                    })
                    .catch(function (e) {
                        console.log("get url error:",e)
                    })
            })
            .catch(e => {
                console.log("get file error:",e)
            })
    }
    
    return (
        <View>
            {url != "" ?
                <Media url={url} navigator={props.navigator} type={props.type} />
                :
                <></>
            }
        </View>
    )
}

const styles = StyleSheet.create({})
