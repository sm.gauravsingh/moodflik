import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Animated, Modal, FlatList, ActivityIndicator, Text, View, Dimensions, Image, PermissionsAndroid, CheckBox, ToastAndroid, AlertIOS, Platform, PanResponder, KeyboardAvoidingView, Keyboard } from 'react-native'
import CustomButton from '../components/CustomButton'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import Icon from 'react-native-vector-icons/AntDesign';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import FIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MatIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { HomeServices } from '../Services/Home.services';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { RFPercentage } from 'react-native-responsive-fontsize';
import AwesomeAlert from 'react-native-awesome-alerts';
import { TextInput } from 'react-native-paper';
import { RadioButton } from 'react-native-paper';

const defaultImg = require('../assets/default_profile.png')
const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function ChatPost(props) {
    const [item, setItem] = useState([]);

    useEffect(() => {
        getSinglePost(props.body)
    }, [props]);

    const getSinglePost = async (id) => {
        await homeServices.getPostById(id).then(
            (data) => {
                if (data) {
                    setItem(data);
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }
    return (<>
        {item && item ?
            <View key={item.uuid} style={{ marginBottom: 10 }}>
                {item.post_type == 'like' ?
                    item.post_type &&
                    <View style={{ marginHorizontal: 10 }}>
                        <Like mediaUrl={item.media_url} mediaType={item.content_type} c_width={'100%'} text={item.content} why={item.why_content} navigator={props.navigator} />
                    </View>
                    :
                    item.post_type &&
                    <View style={{ marginHorizontal: 10 }}>
                        <Dislike mediaUrl={item.media_url} mediaType={item.content_type} c_width={'100%'} text={item.content} why={item.why_content} navigator={props.navigator} />
                    </View>
                }
            </View>
            :
            <></>
        }
    </>
    )
}

const styles = StyleSheet.create({
    sendBtn: {
        textAlign: 'center',
        width: width,
        padding: 7,
        color: '#fff',
        fontSize: RFPercentage(2.7),
    },
    img: {
        width: 60,
        height: 60,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 60 / 2
    },
    label: {
        fontWeight: 'bold',
        color: '#000',
        fontStyle: 'italic',
        fontSize: RFPercentage(2.7),
        textAlign: 'center',
        marginBottom: 10
    },
    editMenu: {
        width: 150,
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: 999,
        bottom: 27,
        right: 0,
        borderRadius: 3,
        borderWidth: 0.5,
        borderColor: 'grey'
    },
    menuBox: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5
    },
    menuText: {
        fontSize: RFPercentage(2.7),
        padding: 5
    },
    overlay: {
        backgroundColor: 'rgba(0,0,0,0.2)',
        flex: 1,
        justifyContent: 'flex-end',
    },
    container2: {
        flex: 1,
    },
    container: {
        backgroundColor: 'white',
        borderTopRightRadius: 12,
        borderTopLeftRadius: 12,
        maxHeight: vh * 90
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    chat_box: {
        flexDirection: 'row',
        padding: 5,
        paddingLeft: 10,
        borderBottomColor: '#000',
        // borderBottomWidth: 0.5
    },
    name: {
        fontSize: RFPercentage(2),
        color: '#000'
    },
    username: {
        fontSize: RFPercentage(1.5),
        color: '#000'
    },
    msg: {
        fontSize: RFPercentage(2),
        color: '#444'
    },
    image_box: {
        width: 40,
    },
    profile_img: {
        width: 30,
        height: 30,
        borderWidth: 0.5,
        borderRadius: 70 / 2,
        borderColor: '#000',
        resizeMode: 'cover'
    },
    detail_box: {
        width: vw * 75,
    },
})
