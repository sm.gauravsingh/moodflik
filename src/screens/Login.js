import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, TouchableOpacity } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Loader from '../components/Loader'
import CustomButton from '../components/CustomButton'
import CustomInput from '../components/CustomInput'
import AsyncStorage from '@react-native-async-storage/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import axios from 'react-native-axios';

import { AuthContext } from '../globals/Context'

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const baseUrl = "https://t87goxe24a.execute-api.ap-south-1.amazonaws.com/mood_dev/";

export default function Login({ navigation }) {
    const { signIn } = React.useContext(AuthContext)
    const [isLoader, setIasLoader] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [emailError, setEmailError] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [showAlert, setShowAlert] = useState(false);

    const handleSignIn = () => {
        var check = "";
        var check2 = "";
        if (email == "") {
            setEmailError("Please enter a email");
            check = "NA";
        } else if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            setEmailError("Please enter a valid email");
            check = "NA";
        } else {
            setEmailError("");
            check = "";
        }
        if (password == "") {
            setPasswordError("Please enter a password");
            check2 = "NA";
        }
        // else if (!/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(password)) {
        //     setPasswordError("Password should be between 6-16 characters including small and capital letters, number(s) and special characters");
        //     check2 = "NA";
        // } 
        else {
            setPasswordError("");
            check2 = "";
        }

        if (check == "" && check2 == "") {
            setIasLoader(true)
            var data = JSON.stringify({
                "email": email,
                "password": password
            });
            var config = {
                method: 'post',
                url: baseUrl+'user/authorize/',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            };
            axios(config)
                .then(function (response) {
                    Login(response.data)
                })
                .catch(function (error) {
                    setShowAlert(true)
                    console.log(error);
                });
        }
    }

    const Login = async (data) => {
        setIasLoader(false)
        if (data.status) {
            var loginData = {
                userId: data.data.uuid,
                _token: data.data.accessToken,
                _qbUid: ''
            }
            try {
                console.log("Adsds", loginData);
                await AsyncStorage.setItem('_moodflikAppData', JSON.stringify(loginData));
                signIn()
            } catch (err) {
                console.log(err);
            }
        } else {
            setShowAlert(true)
        }
    }

    return (<>
        <ScrollView style={styles.main}>
            {isLoader &&
                <Loader />
            }
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ width: vw * 90, minHeight: vh * 85 }}>
                    <View style={{ flexDirection: 'column', height: "100%", justifyContent: 'center' }}>
                        <View>
                            <Text style={{ fontWeight: 'bold', fontSize: RFPercentage(3), textAlign: 'center', marginBottom: 20 }}>Sign In</Text>
                            {emailError != "" && <Text style={{ color: 'red' }}>{emailError}</Text>}
                            <CustomInput returnKeyType="next" value={email} action={(text) => setEmail(text)} placeHolder={"Email address"} />
                            {passwordError != "" && <Text style={{ color: 'red' }}>{passwordError}</Text>}
                            <CustomInput value={password} action={(text) => setPassword(text)} placeHolder={"Password"} secureTextEntry={true} />
                            <TouchableOpacity onPress={() => navigation.navigate('ForgetPassword')}>
                                <Text style={{ fontSize: RFPercentage(2.5), textDecorationStyle: 'solid', textDecorationLine: 'underline', textAlign: 'center' }}>Forgot Password?</Text>
                            </TouchableOpacity>
                            <CustomButton returnKeyType="go" text="Login" enabled={true} btnAction={() => handleSignIn()} btnWidth={60} />
                            <CustomButton returnKeyType="go" text="Sign Up" enabled={true} btnAction={() => navigation.navigate('SignUp')} btnWidth={60} />
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
        <AwesomeAlert
            show={showAlert}
            showProgress={false}
            // title={`Choose Image`}
            message={"Email or Password is incorrect"}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={false}
            showConfirmButton={true}
            cancelText="Cancel"
            confirmText="OK"
            cancelButtonColor='#28a745'
            confirmButtonColor="#6C0AC7"
            onCancelPressed={() => {
                setShowAlert(false)
                setIasLoader(false)
            }}
            onConfirmPressed={() => {
                setShowAlert(false)
                setIasLoader(false)
            }}
            onDismiss={() => {
                setShowAlert(false)
                setIasLoader(false)
            }}
        />
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        marginTop: 15
    }
})
