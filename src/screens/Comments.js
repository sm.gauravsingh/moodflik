import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, Image, ActivityIndicator, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import { RFPercentage } from 'react-native-responsive-fontsize';
import { HomeServices } from '../Services/Home.services';
import Loader from '../components/Loader'
import { ScrollView } from 'react-native-gesture-handler';
import CustomInput from '../components/CustomInput'
import Icon from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function Comments(props) {
    const [isLoader, setIsLoader] = useState(false);
    const [comments, setComments] = useState([]);
    const [commentText, setCommentText] = useState("");
    const [type, setType] = useState("");
    const [post, setPost] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoader(true)
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage.post) {
            getComments(props.route.params.refreshPage.post);
            setPost(props.route.params.refreshPage.post);
            setType(props.route.params.refreshPage.type);
        }
    }, [props]);

    var getComments = async (post) => {
        await homeServices.getComentsData(post.uuid).then(
            (data) => {
                if (data) {
                    setIsLoader(false)
                    setComments(data);
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const addComment = async () => {
        setIsLoading(true);
        if (commentText != "") {
            var payload = {
                post: post.uuid,
                comment: commentText
            }
            await homeServices.addComments(payload).then(
                (data) => {
                    setIsLoader(false)
                    if (data && data.status) {
                        getComments(post);
                        setCommentText('');
                        setIsLoading(false);
                    }
                },
                (error) => {
                    setIsLoader(false)
                    console.log("error.response.status", error);
                }
            );
        }
    }

    const dateFormat = (date) => {
        var date = new Date(date);
        var month = date.toLocaleString('en-us', { month: 'short' });
        var day = getShortDay(date);
        var year = date.getFullYear();
        var d = date.getDate();
        return day + " " + d + " " + month + " " + year;
    }

    const getShortDay = (dateString) => {
        // var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var d = new Date(dateString);
        var dayName = days[d.getDay()];
        return dayName;
    }


    return (<>
        {isLoader &&
            <Loader />
        }
        <ScrollView style={{ marginBottom: 90 }}>
            <Text style={{ fontSize: RFPercentage(3), fontWeight: 'bold', textAlign: 'center' }}>Comments</Text>
            {comments.length > 0 && comments.map((c, i) => {
                return <View style={{ marginHorizontal: 10 }}>
                    <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', marginTop: 10 }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start', height: '100%' }}>
                            <Image
                                style={styles.img}
                                source={{
                                    uri: c.photo_url ? c.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                                }}
                            />
                        </View>
                        <View style={{ maxWidth: vw * 85 }}>
                            <View style={styles.commentBox}>
                                <Text style={styles.username}>{c.username}</Text>
                                <Text style={styles.name}>{c.first_name} {c.last_name}</Text>
                                <Text style={styles.comment}>{c.comment}</Text>
                            </View>
                            <Text style={styles.time}>{dateFormat(c.created_at)}</Text>
                        </View>
                    </View>
                </View>
            })}
        </ScrollView>
        <View style={styles.commentInputBox}>
            <View style={{ width: vw * 85 }}>
                <CustomInput returnKeyType="next" value={commentText} action={(text) => setCommentText(text)} placeHolder={"Write a comment..."} />
            </View>
            <View style={{ flexDirection: 'column', justifyContent: 'center', width: vw * 15, paddingBottom: 15 }}>
                {isLoading ?
                    <View style={{ position: 'relative' }}>
                        <ActivityIndicator size="small" color="#fff" />
                    </View>
                    : <TouchableOpacity onPress={addComment}>
                        <Icon name={"md-send-sharp"} color="#fff" style={{ marginLeft: 15 }} size={RFPercentage(4)} />
                    </TouchableOpacity>
                }
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    img: {
        width: 40,
        height: 40,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 40 / 2
    },
    username: {
        fontSize: RFPercentage(2.3),
        fontWeight: 'bold'
    },
    name: {
        fontSize: RFPercentage(2.3),
        fontWeight: 'bold'
    },
    comment: {
        fontSize: RFPercentage(2),
        fontWeight: '100',
    },
    time: {
        fontSize: RFPercentage(1.5),
        fontWeight: '100',
        color: '#525252',
        marginLeft: 10
    },
    commentBox: {
        marginLeft: 10,
        backgroundColor: '#d3d3d3',
        borderRadius: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingTop: 5
    },
    commentInputBox: {
        backgroundColor: '#6C0AC7',
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingTop: 15,
        position: 'absolute',
        bottom: 0
    }
})
