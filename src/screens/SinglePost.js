import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native'
import Post from '../components/Post'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { HomeServices } from '../Services/Home.services';

const homeServices = new HomeServices();

export default function SinglePost(props) {
    const [item, setItem] = React.useState({});
    const [isLoader, setIsLoader] = React.useState(false);
    const [userId, setUserId] = React.useState('');

    useEffect(() => {
        getUser();
        setIsLoader(true);
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage.type == 'single_post') {
            getSinglePost(props.route.params.refreshPage.postId)
        }
    }, [props]);

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    const getSinglePost = async (id) => {
        await homeServices.getPostById(id).then(
            (data) => {
                if (data) {
                    console.log(";;;;;;;", data)
                    setItem(data);
                    setIsLoader(false);
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    return (
        <View>
            {!isLoader ?
                <Post navigator={props.navigator} userId={userId} navigation={props.navigation} likeSection={item && item.post_type && item.post_type == "like" ? true : false} post={item} />
                :
                <></>
            }
        </View>
    )
}

const styles = StyleSheet.create({})
