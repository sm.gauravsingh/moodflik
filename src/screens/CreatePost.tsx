import React, { useRef, useCallback, Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView, DrawerLayoutAndroid, KeyboardAvoidingView, Image, Dimensions, TouchableOpacity, Alert, Platform, Modal } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
// import ImagePicker  from "react-native-image-picker";
import Loader from '../components/Loader'
import CustomButton from '../components/CustomButton'
import CustomTextArea from '../components/CustomTextArea'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import { Thumbnail } from 'react-native-thumbnail-video';
import BottomMenu from '../components/BottomMenu'
import NavigationView from "../components/NavigationView";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import FIcon from 'react-native-vector-icons/FontAwesome';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Header from '../components/Header';
import { RNS3 } from 'react-native-s3-upload';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { HomeServices } from '../Services/Home.services';
import DocumentPicker from 'react-native-document-picker';
import Video from 'react-native-video';
import AwesomeAlert from 'react-native-awesome-alerts';
import { NotificationHandler } from '../Services/NotificationHandler';
import Media from '../components/Media';
import {
    GiphyClipsRendition,
    GiphyContent,
    GiphyDialog,
    GiphyDialogConfig,
    GiphyDialogEvent,
    GiphyDialogMediaSelectEventHandler,
    GiphyDirection,
    GiphyGridView,
    GiphyMedia,
    GiphyMediaView,
    GiphyRendition,
    GiphyVideoView,
} from '@giphy/react-native-sdk'
import '../giphy.setup'
import { DEFAULT_DIALOG_SETTINGS, GiphyDialogSettings } from '../Settings'
import { Dialog } from './Dialog'
import CustomInput from '../components/CustomTextArea';
import { TextInput } from 'react-native-paper';
const defaultImg = require('../assets/default_profile.png')

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();
const notificationHandler = new NotificationHandler();

const bio_data = {
    phone_number: "",
    country: "",
    website: "",
    city: "",
    me: "",
    like: "",
    dislike: "",
    user: "",
    photo_url: ""
}

export default function CreatePost(props) {
    const drawer = useRef(null);
    const [isLoader, setIsLoader] = useState(false);
    const [tagPeople, setTagPeople] = useState(false);
    const [likeSection, setLikeSection] = useState(true);
    const [lovingText, setLovingText] = useState("");
    const [lovingTextWhy, setLovingTextWhy] = useState("");
    const [dislikeText, setDislikeText] = useState("");
    const [dislikeTextWhy, setDislikeTextWhy] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [gifUri, setGifUri] = useState("");
    const [videoUri, setVideoUri] = useState("");
    const [fileUri, setFileuri] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [chooseImageOption, setChooseImageOption] = useState(false);
    const [bioId, setBioId] = useState('');
    const [bioData, setBioData] = useState(bio_data);
    const [userId, setUserId] = useState('');
    const [mediaUrl, setMediaUrl] = useState("");
    const [mediaType, setMediaType] = useState("");
    const [userSearch, setUserSearch] = useState("");
    const [userList, setUserList] = useState([]);
    const [taggedUserId, setTaggedUserId] = useState([]);
    const [dislikeTaggedUserId, setDislikeTaggedUserId] = useState([]);
    const [medias, setMedias] = useState<GiphyMedia[]>([]);
    const [giphyDialogSettings, setGiphyDialogSettings] = useState<GiphyDialogConfig>(DEFAULT_DIALOG_SETTINGS)

    const mediasRef = useRef(medias)
    mediasRef.current = medias

    const addMedia = useCallback((media: GiphyMedia) => {
        setMedias([media, ...mediasRef.current])
        setGifUri(media.url)
        setMediaUrl(media.url)
        setMediaType('gif')
        console.log('giiiffff', media.url)
    }, [])


    useEffect(() => {
        getUser();
        setIsLoader(true)
    }, []);

    useEffect(() => {
        setMediaUrl('');
        setMediaType('')
    }, [likeSection]);

    const checkUsername = (val: string) => {
        if (val.includes("@")) {
            return val;
        } else {
            return '@' + val;
        }
    }


    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                getBioData(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        const handler: GiphyDialogMediaSelectEventHandler = (e) => {
            addMedia(e.media)
            GiphyDialog.hide()
        }
        const listener = GiphyDialog.addListener(
            GiphyDialogEvent.MediaSelected,
            handler
        )
        return () => {
            listener.remove()
        }
    }, [addMedia])

    var getBioData = async (uid) => {
        await homeServices.getUserDetails(uid).then(
            (data) => {
                console.log("nionionio", data)
                setIsLoader(false)
                if (data && data.uuid) {
                    setBioId(data.uuid);
                    setBioData(data)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }


    const handleMediaClick = async (fileType) => {
        if (fileType == "image") {
            setChooseImageOption(true);
            return true;
        }
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            uploadImageToS3(res, fileType);
            console.log("uploadedUrl------", res)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const handleCamera = () => {
        setChooseImageOption(false)
        var options = {
            mediaType: 'photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        }
        launchCamera(options, function (response) {
            var file = {
                uri: response.uri,
                name: response.fileName,
                type: "image/png"
            }
            uploadImageToS3(file, 'image')
        });
    }
    const handleLibrary = () => {
        setChooseImageOption(false)
        var options = {
            mediaType: 'photo'
        }
        launchImageLibrary(options, function (response) {
            var file = {
                uri: response.uri,
                name: response.fileName,
                type: "image/png"
            }
            uploadImageToS3(file, 'image')
        })
    }

    const uploadImageToS3 = (file, fileType) => {
        setChooseImageOption(false)
        setIsLoader(true)
        const options = {
            keyPrefix: "img/",
            bucket: "moodflik-portal",
            region: "us-east-2",
            accessKey: "AKIAWFNJSIDVAQA2KNEU",
            secretKey: "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",
            successActionStatus: 201
        }
        RNS3.put(file, options).then(response => {
            if (response.status !== 201) {
                setIsLoader(false)
                throw new Error("Failed to upload image to S3");
            }
            setIsLoader(false)
            console.log("upload response", response.body)
            setMediaUrl(response.body.postResponse.location)
            if (fileType == "image") {
                setImageUrl(response.body.postResponse.location)
                setMediaType('image')
            } else if (fileType == "gif") {
                setGifUri(response.body.postResponse.location)
                setMediaType('gif')
            } else if (fileType == "video") {
                setVideoUri(response.body.postResponse.location)
                setMediaType('video')
            } else if (fileType == "file") {
                setFileuri(response.body.postResponse.location)
                setMediaType('file')
            }
            // getUserData();
        }).catch(function (error) {
            console.log("aeee", error)
        });
    }

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem('_moodflikAppData')
            if (value && value !== null) {
                return JSON.parse(value)
            }
        } catch (e) {
            console.log(e)
        }
    }

    const addPost = async (type) => {
        setIsLoader(true)
        var userData = await getData();
        if (type == "Like") {
            var payload = {
                "content": lovingText,
                "content_type": mediaType && mediaType != '' ? mediaType : 'text',
                "post_type": "like",
                "why_content": lovingTextWhy,
                "media_url": mediaUrl,
                "tagged": taggedUserId ? taggedUserId : []
            }
            await homeServices.createPost(payload).then(
                (data) => {
                    setIsLoader(false)
                    console.log("Post added-", data);
                    notificationHandler.selfPushNotification("Your post has been successfully added.")
                    if (data && data.uuid) {
                        setImageUrl("")
                        setFileuri("")
                        setLovingText("")
                        setDislikeText("")
                        setLovingTextWhy("")
                        setDislikeTextWhy("")
                        setShowAlert(!showAlert)
                        setTaggedUserId([])
                        sendNotification(taggedUserId)
                    }
                },
                (error) => {
                    console.log("error.response.status", error);
                }
            );
        } else {
            var payload = {
                "content": dislikeText,
                "content_type": mediaType && mediaType != '' ? mediaType : 'text',
                "post_type": "dislike",
                "why_content": dislikeTextWhy,
                "media_url": mediaUrl,
                "tagged": dislikeTaggedUserId ? dislikeTaggedUserId : []
            }
            await homeServices.createPost(payload).then(
                (data) => {
                    setIsLoader(false)
                    notificationHandler.selfPushNotification("Your post has been successfully added.")
                    console.log("Post added-", data);
                    if (data && data.uuid) {
                        setImageUrl("")
                        setFileuri("")
                        setLovingText("")
                        setDislikeText("")
                        setLovingTextWhy("")
                        setDislikeTextWhy("")
                        setShowAlert(!showAlert)
                        setDislikeTaggedUserId([])
                        sendNotification(dislikeTaggedUserId)
                    }
                },
                (error) => {
                    console.log("error.response.status", error);
                }
            );

        }
    }

    const sendNotification = async(users:any) => {
        var tempDeviceId = [];
        await homeServices.getUserList('').then(
            (data) => {
                if (data.length > 0) {
                    data.map((k)=>{
                        if(users.includes(k.uuid)){
                            if(k.device_id && k.device_id != "" && k.device_id != null){
                                tempDeviceId.push(k.device_id)
                            }
                        }
                    })
                    notificationHandler.pushNotification("Your have been tagged to the post.",tempDeviceId)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );

    }

    const youtube_parser = (url) => {
        var regExp = /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
        var match = url.match(regExp);
        return (match && match[1].length == 11) ? true : false;
    }

    const handleLovingText = (text: string) => {
        setLovingText(text);
        if (youtube_parser(text)) {
            setMediaUrl(text)
            setMediaType('thumbnail')
        }
    }

    const handleDislikeText = (text: string) => {
        setDislikeText(text);
        if (youtube_parser(text)) {
            setMediaUrl(text)
            setMediaType('thumbnail')
        }
    }

    const selectTagUser = (data: any) => {
        if (likeSection) {
            var tempArr = taggedUserId;
            if (!tempArr.includes(data.uuid)) {
                tempArr.push(data.uuid)
            } else {
                const index = tempArr.indexOf(data.uuid);
                if (index > -1) {
                    tempArr.splice(index, 1);
                }
            }
            setTaggedUserId(tempArr)
        } else {
            var tempArr2 = dislikeTaggedUserId;
            if (!tempArr2.includes(data.uuid)) {
                tempArr2.push(data.uuid)
            } else {
                const index = tempArr2.indexOf(data.uuid);
                if (index > -1) {
                    tempArr2.splice(index, 1);
                }
            }
            setDislikeTaggedUserId(tempArr2)
        }
        var tempList = userList
        setUserList([...tempList])
    }

    const openTagPeople = async (text: any) => {
        setUserSearch(text)
        await homeServices.getUserList(text).then(
            (data) => {
                setTagPeople(true)
                if (data) {
                    setUserList(data.results)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    return (<>
        <ScrollView style={styles.main}>
            {isLoader ?
                <Loader />
                : <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <View style={{ width: vw * 90, marginBottom: 100 }}>
                        <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <View>
                                        <Image style={styles.img} source={bioData.photo_url ? { uri: bioData.photo_url } : require('../assets/example.png')} />
                                    </View>
                                    <View style={{ marginLeft: 20 }}>
                                        <Text>{bioData.first_name} {bioData.last_name && bioData.last_name}</Text>
                                        <Text>{bioData.username}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <CustomButton textStyle={{ width: 35 * vw }} text={`What’s got you smiling today?`} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={43} invertColour={!likeSection ? '#6C0AC7' : '#108A07'} textColor="#fff" isIcon={true} icon={<Image
                                style={{ height: 25, width: 25, marginLeft: 20 }}
                                source={require('../assets/like.png')}
                            />} textAlign={"left"} />
                            <CustomButton textStyle={{ width: 35 * vw }} text={`What’s been getting on your nerves today?`} invertColour={likeSection ? '#6C0AC7' : '#BF1414'} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={43} isIcon={true} icon={<Image
                                style={{ height: 25, width: 25, marginLeft: 20 }}
                                source={require('../assets/dislike.png')}
                            />} textAlign={"left"} />
                        </View>
                        {likeSection ?
                            <View>
                                <Text style={{ marginTop: 15, fontWeight: 'bold', textAlign: 'center' }}>I'm Currently Loving:</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={lovingText} action={(text) => handleLovingText(text)} charCount={130} max={130} />
                                    {mediaUrl != "" &&
                                        <View style={{ padding: 5 }}>
                                            <Media url={mediaUrl} type={mediaType} />
                                        </View>
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                                    <View>
                                        <TouchableOpacity style={[styles.mediaIcon, { position: 'relative' }]} onPress={() => openTagPeople('')}>
                                            <Text style={styles.tagged}>{taggedUserId.length}</Text>
                                            <Icon name="tag-plus" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 10 }}>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => GiphyDialog.show()}>
                                            <Icon name="gif" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("image")}>
                                            <Icon name="camera" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("video")}>
                                            <Icon name="video" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("file")}>
                                            <Icon name="attachment" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <Text style={styles.italic}>Why? (Optional)</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={lovingTextWhy} action={(text) => setLovingTextWhy(text)} charCount={150} max={150} />
                                </View>
                                <CustomButton text="Post" enabled={true} btnAction={() => addPost("Like")} btnWidth={40} />
                            </View>
                            :
                            <View>
                                <Text style={{ marginTop: 15, fontWeight: 'bold', textAlign: 'center' }}>I Currently Dislike:</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={dislikeText} action={(text) => handleDislikeText(text)} charCount={130} max={130} />
                                    {mediaUrl != "" &&
                                        <View style={{ padding: 5 }}>
                                            <Media url={mediaUrl} type={mediaType} />
                                        </View>
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                                    <View>
                                        <TouchableOpacity style={[styles.mediaIcon, { position: 'relative' }]} onPress={() => openTagPeople('')}>
                                            <Text style={styles.tagged}>{dislikeTaggedUserId.length}</Text>
                                            <Icon name="tag-plus" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 10 }}>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => GiphyDialog.show()}>
                                            <Icon name="gif" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("image")}>
                                            <Icon name="camera" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("video")}>
                                            <Icon name="video" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                        <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("file")}>
                                            <Icon name="attachment" size={RFPercentage(4)} color="#000" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <Text style={styles.italic}>Why? (Optional)</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={dislikeTextWhy} action={(text) => setDislikeTextWhy(text)} charCount={150} max={150} />
                                </View>
                                <CustomButton text="Post" enabled={true} btnAction={() => addPost("Dislike")} btnWidth={40} />
                            </View>
                        }
                    </View>
                </View>
            }
        </ScrollView>
        <AwesomeAlert
            show={showAlert}
            showProgress={false}
            title={`${likeSection ? 'Like' : 'Dislike'} post is created.`}
            // message={`Do you want to create post for ${likeSection ? 'Dislike' : 'Like'} also?`}
            message={`Would you like to create a ${likeSection ? 'Dislike' : 'Like'} post?`}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="No"
            confirmText="Yes"
            cancelButtonColor='#dc3545'
            confirmButtonColor="#28a745"
            onCancelPressed={() => {
                props.navigation.navigate('Home', {
                    refreshPage: {
                        refresh: Math.random(),
                        type: 'post_added'
                    },
                })
            }}
            onConfirmPressed={() => {
                setLikeSection(!likeSection)
                setShowAlert(false)
            }}
            onDismiss={() => {
                setShowAlert(false)
            }}
        />
        <AwesomeAlert
            show={chooseImageOption}
            showProgress={false}
            title={`Choose Image`}
            message={`Select Option:`}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="Camera"
            confirmText="Gallery"
            cancelButtonColor='#28a745'
            confirmButtonColor="#6C0AC7"
            onCancelPressed={() => {
                handleCamera()
                setChooseImageOption(false)
            }}
            onConfirmPressed={() => {
                handleLibrary()
                setChooseImageOption(false)
            }}
            onDismiss={() => {
                setChooseImageOption(false)
            }}
        />
        <Modal
            animationType="slide"
            transparent={false}
            visible={tagPeople}
            onRequestClose={() => {
                setTagPeople(!tagPeople);
            }}
        >
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ width: 90 * vw, marginTop: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: vw * 80 }}>
                            <TextInput value={userSearch} onChangeText={(text) => openTagPeople(text)} placeholder={"Type here ..."} secureTextEntry={false} />
                        </View>
                        <View style={{ width: vw * 10, marginTop: 17, justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => setTagPeople(!tagPeople)}>
                                <Icon name="close" size={RFPercentage(4)} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        {userList && userList.length > 0 && userList.map((x) => {
                            return <TouchableOpacity onPress={() => selectTagUser(x)}>
                                <View style={styles.chat_box}>
                                    <View style={styles.image_box}>
                                        <Image style={styles.profile_img} source={x.photo_url ? x.photo_url + `?buster=${Math.random()}` : defaultImg} />
                                    </View>
                                    <View style={styles.detail_box}>
                                        <Text style={styles.name}>{x.first_name} {x.last_name}</Text>
                                        <Text style={styles.username}>{x.username}</Text>
                                    </View>
                                    <View>
                                        {likeSection ?
                                            taggedUserId.includes(x.uuid) &&
                                            <Icon name="check" style={{ marginTop: 10 }} size={RFPercentage(4)} color="#79FE0C" />
                                            :
                                            dislikeTaggedUserId.includes(x.uuid) &&
                                            <Icon name="check" style={{ marginTop: 10 }} size={RFPercentage(4)} color="#79FE0C" />
                                        }
                                    </View>
                                </View>
                            </TouchableOpacity>
                        })}
                    </View>
                </View>
            </View>
        </Modal>
        <View style={{ position: 'absolute', top: Platform.OS === 'ios' ? height - 180 : height - 160, width: width }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={styles.footer}>
                    <BottomMenu
                        navigation={props.navigation}
                    />
                </View>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    italic: {
        fontWeight: 'bold',
        fontSize: RFPercentage(2.6),
        fontStyle: 'italic',
        textAlign: 'center'
    },
    mediaIcon: {
        width: 60,
        alignItems: 'center'
    },
    img: {
        width: 70,
        height: 70,
        borderRadius: 70 / 2,
        borderColor: '#000',
        borderWidth: 0.5
    },
    contentBox: {
        borderColor: '#aaa',
        borderWidth: 0.5,
        borderRadius: 7,
        marginBottom: 5
    },
    chat_box: {
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: '#000',
        borderBottomWidth: 0.5
    },
    name: {
        fontSize: RFPercentage(3)
    },
    username: {
        fontSize: RFPercentage(2)
    },
    detail_box: {
        width: vw * 60,
    },
    image_box: {
        width: 65,
    },
    profile_img: {
        width: 50,
        height: 50,
        borderWidth: 0.5,
        borderRadius: 70 / 2,
        borderColor: '#000',
        resizeMode: 'cover'
    },
    tagged: {
        position: 'absolute',
        top: -3,
        right: 15,
        zIndex: 10,
        backgroundColor: '#79FE0C',
        height: 15,
        fontSize: RFPercentage(1.5),
        width: 15,
        textAlign: 'center',
        borderRadius: 50
    }
})
