import React, { Component, useRef, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, PermissionsAndroid, TouchableOpacity, DrawerLayoutAndroid, ScrollView, Alert } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Title from '../components/Title'
import CustomButton from '../components/CustomButton'
import { launchCamera, launchImageLibrary, ImagePicker } from 'react-native-image-picker';
import { globalColors, globalSpaces, globalStyles, globalHeadingFont, globalFontFamily } from '../globals/theme'
import CustomInput from '../components/CustomInput'
import InputText from '../components/inputText/inputText'
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationView from "../components/NavigationView";
import Header from '../components/Header';
import BottomMenu from '../components/BottomMenu';
import { HomeServices } from '../Services/Home.services';
import { RNS3 } from 'react-native-s3-upload';
import AwesomeAlert from 'react-native-awesome-alerts';
import Loader from '../components/Loader';
import QB from "quickblox-react-native-sdk"
import AsyncStorage from '@react-native-async-storage/async-storage';
import CommonAlert from '../components/CommonAlert';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

const homeServices = new HomeServices();

const bio_data = {
    phone_number: "",
    country: "",
    website: "",
    city: "",
    me: "",
    like: "",
    dislike: "",
    user: "",
    photo_url: ""
}

export default function ProfileSetting(props) {
    const drawer = useRef(null);
    const [bioData, setBioData] = useState(bio_data);
    const [updatedBioData, setUpdatedBioData] = useState(bio_data);
    const [chooseImageOption, setChooseImageOption] = useState(false);
    const [isLoader, setIsLoader] = useState(false);
    const [meCharLimit, setMeCharLimit] = useState(30);
    const [likeCharLimit, setLikeCharLimit] = useState(50);
    const [dislikeCharLimit, setDislikeCharLimit] = useState(50);
    const [userId, setUserId] = useState('');
    const [alert, setAlert] = useState(false);

    useEffect(() => {
        getUser();
        setIsLoader(true);
        checkPermission();
    }, []);

    const checkPermission = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,)
            }
        })
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
            }
        })
    }

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                getBioData(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    const handleCamera = () => {
        checkPermission();
        var options = {
            mediaType: 'photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        }
        launchCamera(options, function (response) {
            var file = {
                uri: response.uri,
                name: response.fileName,
                type: "image/png"
            }
            uploadImageToS3(file)
            updateQBAvatar(response.uri)
        });
    }
    const handleLibrary = () => {
        var options = {
            mediaType: 'photo'
        }
        launchImageLibrary(options, function (response) {
            var file = {
                uri: response.uri,
                name: response.fileName,
                type: "image/png"
            }
            uploadImageToS3(file)
            updateQBAvatar(response.uri)
        })
    }

    const uploadImageToS3 = (file) => {
        setChooseImageOption(false)
        // setIsLoader(true)
        const options = {
            keyPrefix: "img/",
            bucket: "moodflik-portal",
            region: "us-east-2",
            accessKey: "AKIAWFNJSIDVAQA2KNEU",
            secretKey: "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",
            successActionStatus: 201
        }
        RNS3.put(file, options).then(response => {
            if (response.status !== 201) {
                setIsLoader(false)
                throw new Error("Failed to upload image to S3");
            }
            setIsLoader(false)
            console.log("upload response", response.status)
            setBioData({
                ...bioData,
                profile_photo: response.body.postResponse.location,
                photo_url: response.body.postResponse.location
            })
            // getUserData();
        }).catch(function (error) {
            console.log("aeee", error)
        });

    }

    var getBioData = async (uid) => {
        await homeServices.getUserDetails(uid).then(
            (data) => {
                console.log("ppppp",data)
                setIsLoader(false)
                if (data && data.uuid) {
                    setBioData(data)
                    setMeCharLimit(data.me.length)
                    setLikeCharLimit(data.like.length)
                    setDislikeCharLimit(data.dislike.length)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const handleInputChange = (value, name) => {
        setBioData({
            ...bioData,
            [name]: value
        })

        setUpdatedBioData({
            ...updatedBioData,
            [name]: value
        })
    }

    const updateData = async () => {
        var payload = {
            ...updatedBioData,
            // user: bioData.uuid,
            photo_url: bioData.profile_photo,
        };
        var cleanPayload = await clean(payload);
        if(cleanPayload && Object.keys(cleanPayload).length > 0){
            setIsLoader(true)
            await homeServices.updateUserDetails(cleanPayload, bioData.uuid).then(
                (data) => {
                    setIsLoader(false)
                    if (data) {
                        setAlert(true)
                    }
                },
                (error) => {
                    setIsLoader(false)
                    console.log("error.response.status", error);
                }
            );
        }
    }

    const clean = async (obj) => {
        for (var propName in obj) {
          if (obj[propName] === null || obj[propName] === undefined || obj[propName] === "") {
            delete obj[propName];
          }
        }
        return obj
    }

    const updateQBAvatar = async (fileUrl) => {
        await homeServices.getBioDetails().then(
            (data) => {
                if (data.status) {
                    const fileData = {
                        url: fileUrl,
                        public: true
                    };
                    QB.content
                        .upload(fileData)
                        .then(function (file) {
                            // file uploaded successfully
                            console.log("qb avatar updated succes", file)
                            const userUpdate = { blobId: file.id, login: data.bio_details[0].username, };
                            return QB.users.update(userUpdate);
                        })
                        .then(function (user) {
                            console.log("qb user updated succes", user)
                            // user updated successfully
                        })
                        .catch(function (error) {
                            console.log("qb avatar updated failed", error)
                            // inspect error message to check what is wrong
                        });
                }
            },
            (error) => {
                console.log("error.response.status", error);
            });
    }

    const charCount = (myString) => {
        var remText = myString.replace(/ /g, "");
        return remText.length;
    }

    return (
        <>
            {isLoader && <Loader />}
            <ScrollView style={styles.main}>
                <View style={styles.container}>
                    <View style={styles.ProfileSetup}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Icon name="person-add" size={RFPercentage(3.5)} color="#000" />
                            <Title text={"ACCOUNT AND PROFILE SETTING:"} titleStyle={styles.titleStyle} titleContainer={styles.titleContainer} />
                        </View>
                        <View style={styles.profile_img}>
                            <TouchableOpacity style={styles.img} onPress={() => setChooseImageOption(true)}>
                                <Image style={{ height: 100, width: 100, borderRadius: 50, }} source={bioData.photo_url ? { uri: bioData.photo_url } : require('../assets/example.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.input_container}>
                        <InputText
                            placeholder='Choose Username'
                            containerStyle={styles.inputViewContainer}
                            placeholderTextColor='#333333'
                            inputContainerStyle={styles.inputContainer}
                            inputStyle={styles.inputStyle}
                            onChangeText={text => handleInputChange(text, "username")}
                            value={bioData.username}
                        />
                        <InputText
                            placeholder='Mobile Number'
                            containerStyle={styles.inputViewContainer}
                            placeholderTextColor='#333333'
                            inputContainerStyle={styles.inputContainer}
                            inputStyle={styles.inputStyle}
                            onChangeText={text => handleInputChange(text, "phone_number")}
                            value={bioData.phone_number}
                        />
                        <InputText
                            placeholder='City'
                            // style={{color:'red'}}
                            containerStyle={styles.inputViewContainer}
                            placeholderTextColor='#333333'
                            inputContainerStyle={styles.inputContainer}
                            inputStyle={[styles.inputStyle]}
                            onChangeText={text => handleInputChange(text, "city")}
                            value={bioData.city}
                        />
                        <InputText
                            placeholder='Website (Optional)'
                            containerStyle={styles.inputViewContainer}
                            placeholderTextColor='#333333'
                            inputContainerStyle={styles.inputContainer}
                            inputStyle={styles.inputStyle}
                            onChangeText={text => handleInputChange(text, "website")}
                            value={bioData.website}
                        />
                    </View>
                    <View>
                        <Text style={[styles.bio_txt, { textAlign: 'center' }]}>Edit Bio:</Text>
                    </View>
                    <View style={styles.input_with_title}>
                        <View>
                            <Text style={styles.bio_txt}>Me in 3 Words:</Text>
                            <InputText
                                placeholder=''
                                containerStyle={[styles.inputViewContainer,]}
                                placeholderTextColor='#333333'
                                inputContainerStyle={[styles.inputContainer, { marginHorizontal: 40 }]}
                                inputStyle={styles.inputStyle}
                                onChangeText={text => { handleInputChange(text, "me"); setMeCharLimit(text.length) }}
                                maxLength={30}
                                value={bioData.me}
                            />
                            <Text style={[styles.remaining_txt, { textAlign: 'right', bottom: 20 }]}>{meCharLimit == 30 ? '(30 Max)' : `(${30 - meCharLimit} Left)`}</Text>
                        </View>
                        <View>
                            <Text style={styles.bio_txt}>Things I Love:</Text>
                            <InputText
                                placeholder=''
                                containerStyle={[styles.inputViewContainer,]}
                                placeholderTextColor='#333333'
                                inputContainerStyle={[styles.inputContainer, { marginHorizontal: 40 }]}
                                inputStyle={styles.inputStyle}
                                onChangeText={text => { handleInputChange(text, "like"); setLikeCharLimit(text.length) }}
                                maxLength={50}
                                value={bioData.like}
                            />
                            <Text style={[styles.remaining_txt, { textAlign: 'right', bottom: 20 }]}>{likeCharLimit == 50 ? '(50 Max)' : `(${50 - likeCharLimit} Left)`}</Text>
                        </View>
                        <View>
                            <Text style={styles.bio_txt}>Things I Dislike:</Text>
                            <InputText
                                placeholder=''
                                containerStyle={[styles.inputViewContainer,]}
                                placeholderTextColor='#333333'
                                inputContainerStyle={[styles.inputContainer, { marginHorizontal: 40 }]}
                                inputStyle={styles.inputStyle}
                                onChangeText={text => { handleInputChange(text, "dislike"); setDislikeCharLimit(text.length) }}
                                maxLength={50}
                                value={bioData.dislike}
                            />
                            <Text style={[styles.remaining_txt, { textAlign: 'right', bottom: 20 }]}>{dislikeCharLimit == 50 ? '(50 Max)' : `(${50 - dislikeCharLimit} Left)`}</Text>
                        </View>
                    </View>
                    <View style={{ marginBottom: 20, alignSelf: 'flex-end' }}>
                        <CustomButton text="Update" enabled={true} btnAction={() => updateData()} btnWidth={30} />
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <CustomButton text="Delete Account" enabled={true} btnAction={() => navigation.navigate('ProfileSetting')} btnWidth={50} />
                    </View>
                </View>
            </ScrollView>
            <AwesomeAlert
                show={chooseImageOption}
                showProgress={false}
                title={`Choose Image`}
                message={`Select Option:`}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={true}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText="Camera"
                confirmText="Gallery"
                cancelButtonColor='#28a745'
                confirmButtonColor="#6C0AC7"
                onCancelPressed={() => {
                    handleCamera()
                    setChooseImageOption(false)
                }}
                onConfirmPressed={() => {
                    handleLibrary()
                    setChooseImageOption(false)
                }}
                onDismiss={() => {
                    setChooseImageOption(false)
                }}
            />
            <CommonAlert showAlert={alert} message={'Successfully Updated the details'} hideAlert={(val)=>setAlert(val)} />
        </>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    container: {
        marginHorizontal: globalSpaces.marginHorizontal20,
    },
    ProfileSetup: {
        alignItems: 'center',
        marginTop: globalSpaces.marginTop10

    },
    img: {
        width: 120,
        height: 120
    },
    titleStyle: {
        fontFamily: globalFontFamily.fontNunitoBold,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: globalHeadingFont.h3,
        fontWeight: '700',

    },
    titleContainer: {
        // backgroundColor: 'red',
        marginLeft: 20
    },
    profile_img: {
        marginTop: globalSpaces.marginTop20
    },

    inputContainer: {
        borderBottomWidth: 0.5,
        borderColor: 'gray',
        color: 'red'

    },
    inputStyle: {
        fontFamily: globalFontFamily.fontNunitoRegular,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 20,
        fontWeight: '600',
    },
    input_container: {
        marginHorizontal: 20,
        marginTop: globalSpaces.marginTop20

    },
    bio_txt: {
        fontFamily: globalFontFamily.fontNunitoRegular,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 21,
        fontWeight: '600',
        // textAlign: 'center'

    },
    picker: {
        borderBottomWidth: 0.5,
        borderColor: 'gray',
        color: 'red',
        marginHorizontal: globalSpaces.marginHorizontal10,

    },
    input_with_title: {
        marginTop: 25

    },
    remaining_txt: {
        fontFamily: globalFontFamily.fontNunitoRegular,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 17,
        fontWeight: '600',
    }
})
