import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Dimensions, ScrollView, Switch } from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { globalColors, globalSpaces, globalStyles, globalHeadingFont, globalFontFamily } from '../globals/theme'
import { HomeServices } from '../Services/Home.services';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/Loader'

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

const defaultNotificationSetting = {
    comments: true,
    like_dislike: true,
    direct_message: false,
    chat_alert: false,
    allow_mentions: false,
    every_hour: true,
    every_three_hours: true,
    every_five_hours: true,
}

export default function NotificationSetting() {
    const [userId, setUserId] = useState('');
    const [isLoader, setIsLoader] = useState(false);
    const [notificationSettings, setNotificationSettings] = useState(defaultNotificationSetting);

    useEffect(() => {
        getUser();
    }, []);

    const toggleSwitch = (value, key) => {
        setNotificationSettings({
            ...notificationSettings,
            [key]: value
        })
        handleNotificationSetting({
            ...notificationSettings,
            [key]: value
        })
    }

    var getNotificationData = async () => {
        setIsLoader(false)
        await homeServices.getNotificationData().then(
            (data) => {
                console.log("error.response.status", data);
                if (data.status) {
                setIsLoader(false)
                    setNotificationSettings(data.data);
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                getNotificationData(userData.userId);
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    const handleNotificationSetting = async (data) => {
        await homeServices.saveNotificationSetting(data).then(
            (data) => {
                if (data) {
                    console.log(data)
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    return (<>
        {isLoader &&
            <Loader />
        }
        <ScrollView style={styles.main}>
            <View style={styles.container}>
                <View style={styles.heading}>
                    <MaterialIcons name="notifications" size={RFPercentage(5.5)} color="#000" />
                    <Text style={styles.notification_txt}>Notification Settings</Text>
                </View>
                <View style={styles.middle}>
                    <Text style={styles.subheading} >Posts:</Text>
                    <View style={styles.switch_container}>
                        <Text style={styles.txt}>Comments on your posts</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.comments ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "comments")}
                            value={notificationSettings.comments}
                        />
                    </View>
                    <View style={styles.switch_container}>
                        <Text style={styles.txt}>Likes and dislikes on your posts</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.like_dislike ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "like_dislike")}
                            value={notificationSettings.like_dislike}
                        />
                    </View>
                </View>
                <View style={styles.middle}>
                    <Text style={styles.subtxt}>Alert on Posts made by people you are following every:</Text>
                    <View style={styles.switch_container}>
                        <Text style={styles.hour}>1 hour</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.every_hour ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "every_hour")}
                            value={notificationSettings.every_hour}
                        />
                    </View>
                    <View style={styles.switch_container}>
                        <Text style={styles.hour}>3 hours</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.every_three_hours ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "every_three_hours")}
                            value={notificationSettings.every_three_hours}
                        />
                    </View>
                    <View style={styles.switch_container}>
                        <Text style={styles.hour}>5 hours</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.every_five_hours ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "every_five_hours")}
                            value={notificationSettings.every_five_hours}
                        />
                    </View>
                </View>
                <View style={styles.middle}>
                    <Text style={styles.subheading}>People and Messages:</Text>
                    <View style={styles.switch_container}>
                        <Text style={styles.txt}>New Direct messages</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.direct_message ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "direct_message")}
                            value={notificationSettings.direct_message}
                        />
                    </View>
                    <View style={styles.switch_container}>
                        <Text style={styles.txt}>Chat alert</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.chat_alert ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "chat_alert")}
                            value={notificationSettings.chat_alert}
                        />
                    </View>
                    <View style={styles.switch_container}>
                        <Text style={styles.txt}>Mentions by other users</Text>
                        <Switch
                            trackColor={{ false: "#FF7F7F", true: "#90ee90" }}
                            thumbColor={notificationSettings.allow_mentions ? "#00ff00" : "#ff0000"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(value) => toggleSwitch(value, "allow_mentions")}
                            value={notificationSettings.allow_mentions}
                        />
                    </View>
                </View>
            </View>
        </ScrollView>
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    container: {
        marginHorizontal: globalSpaces.marginHorizontal10,
    },
    heading: {
        flexDirection: 'row',
        marginTop: globalSpaces.marginTop10
    },
    notification_txt: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 22,
        fontWeight: '800',
        fontWeight: 'bold',
        paddingLeft: globalSpaces.paddingLeft10
    },
    switch_container: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    middle: {
        marginVertical: globalSpaces.marginVertical10,

    },
    subheading: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 17,
        fontWeight: '800',
        fontWeight: 'bold',
        paddingBottom: globalSpaces.paddingBottom10
    },
    txt: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.textLightGrey,
        fontSize: 16,
        fontWeight: '600',
        // paddingBottom:globalSpaces.paddingBottom15,
        paddingVertical: globalSpaces.paddingVertical15
    },
    hour: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.textLightGrey,
        fontSize: 16,
        fontWeight: '600',
        paddingVertical: globalSpaces.paddingVertical10
    },
    subtxt: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.textLightGrey,
        fontSize: 16,
        fontWeight: '600',
        paddingBottom: globalSpaces.paddingBottom5,
    }
})
