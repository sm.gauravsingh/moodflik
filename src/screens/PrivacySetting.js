import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, ScrollView, Dimensions } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { globalColors, globalSpaces, globalStyles, globalHeadingFont, globalFontFamily } from '../globals/theme'
import { RadioButton } from 'react-native-paper';
import { HomeServices } from '../Services/Home.services';
import { TouchableOpacity } from "react-native-gesture-handler";

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

const defaultSetting = {
    privacy_settings:2,
    bio_settings:1,
    like_dislike_settings:1
}

export default function PrivacySetting() {
    const [isLoader, setIsLoader] = useState(false);
    const [privacySetting, setPrivacySetting] = useState(defaultSetting);

    useEffect(() => {
        setIsLoader(true)
        getPostSetting();
    }, []);

    var getPostSetting = async () => {
        await homeServices.getPostSetting().then(
            (data) => {
                setIsLoader(false)
                console.log("000",data)
                var tempSetting = privacySetting
                if (data.bio_settings) {
                    if(data.bio_settings == 'Show_Posts_to_anyone'){
                        tempSetting = {
                            ...tempSetting,
                            bio_settings:0
                        }
                    }else if(data.bio_settings == 'Only_Followers'){
                        tempSetting = {
                            ...tempSetting,
                            bio_settings:1
                        }
                    }else if(data.bio_settings == 'Only_Following'){
                        tempSetting = {
                            ...tempSetting,
                            bio_settings:2
                        }
                    }
                }
                if (data.privacy_settings) {
                    if(data.privacy_settings == 'Anyone'){
                        tempSetting = {
                            ...tempSetting,
                            privacy_settings:1
                        }
                    }else if(data.privacy_settings == 'OnlyFollowers'){
                        tempSetting = {
                            ...tempSetting,
                            privacy_settings:2
                        }
                    }else if(data.privacy_settings == 'OnlyFollowing'){
                        tempSetting = {
                            ...tempSetting,
                            privacy_settings:3
                        }
                    }
                }
                if (data.like_dislike_settings) {
                    if(data.like_dislike_settings == 'Show_Posts_to_anyone'){
                        tempSetting = {
                            ...tempSetting,
                            like_dislike_settings:0
                        }
                    }else if(data.like_dislike_settings == 'Only_Followers'){
                        tempSetting = {
                            ...tempSetting,
                            like_dislike_settings:1
                        }
                    }else if(data.like_dislike_settings == 'Only_Following'){
                        tempSetting = {
                            ...tempSetting,
                            like_dislike_settings:2
                        }
                    }
                }
                setPrivacySetting(tempSetting)
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const savePostSetting = async(name,value) => {
        var payload = {
            ...privacySetting,
            [name]:value
        }
        console.log("dddd",payload)
        await homeServices.savePostSetting(payload).then(
            (data) => {
                if (data.status) {
                    setIsLoader(false)
                    getPostSetting()
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    return (
        <ScrollView style={styles.main}>
            <View style={styles.container}>
                <View style={styles.heading}>
                    <MaterialCommunityIcons name="eye-off" size={RFPercentage(5.5)} color="#000" />
                    <Text style={styles.notification_txt}>PRIVACY SETTINGS:</Text>
                </View>
                <View>
                    <Text style={styles.subheading} >Who can see my posts?</Text>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="first"
                            color="#6C0AC7"
                            status={privacySetting.privacy_settings === 1 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("privacy_settings",1)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("privacy_settings",1)}>
                            <Text style={styles.txt}>Anyone</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="second"
                            color="#6C0AC7"
                            status={privacySetting.privacy_settings === 2 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("privacy_settings",2)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("privacy_settings",2)}>
                            <Text style={styles.txt}>Only my followers</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="third"
                            color="#6C0AC7"
                            status={privacySetting.privacy_settings === 3 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("privacy_settings",3)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("privacy_settings",3)}>
                            <Text style={styles.txt}>Only those I’m following</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <View>
                    <Text style={styles.subheading} >Share my Bio with:</Text>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="first"
                            color="#6C0AC7"
                            status={privacySetting.bio_settings === 0 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("bio_settings",0)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("bio_settings",0)}>
                            <Text style={styles.txt}>Anyone</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="second"
                            color="#6C0AC7"
                            status={privacySetting.bio_settings === 1 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("bio_settings",1)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("bio_settings",1)}>
                            <Text style={styles.txt}>Only my followers</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="third"
                            color="#6C0AC7"
                            status={privacySetting.bio_settings === 2 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("bio_settings",2)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("bio_settings",2)}>
                            <Text style={styles.txt}>Only those I’m following</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <View>
                    <Text style={styles.subheading} >Show my total Likes and Dislikes figures?</Text>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="first"
                            color="#6C0AC7"
                            status={privacySetting.like_dislike_settings === 0 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("like_dislike_settings",0)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("like_dislike_settings",0)}>
                            <Text style={styles.txt}>Anyone</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="second"
                            color="#6C0AC7"
                            status={privacySetting.like_dislike_settings === 1 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("like_dislike_settings",1)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("like_dislike_settings",1)}>
                            <Text style={styles.txt}>Only my followers</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.radio_button}>
                        <RadioButton
                            value="third"
                            color="#6C0AC7"
                            status={privacySetting.like_dislike_settings === 2 ? 'checked' : 'unchecked'}
                            onPress={() => savePostSetting("like_dislike_settings",2)}
                        />
                        <TouchableOpacity onPress={() => savePostSetting("like_dislike_settings",2)}>
                            <Text style={styles.txt}>Only those I’m following</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    container: {
        marginHorizontal: globalSpaces.marginHorizontal10,
    },
    heading: {
        flexDirection: 'row',
        marginTop: globalSpaces.marginTop10
    },
    notification_txt: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 22,
        fontWeight: '800',
        fontWeight: 'bold',
        paddingLeft: globalSpaces.paddingLeft10
    },
    radio_button: {
        flexDirection: 'row'
    },
    subheading: {
        fontFamily: globalFontFamily.fontLatoBold,
        letterSpacing: 1,
        color: globalColors.black,
        fontSize: 17,
        fontWeight: '800',
        fontWeight: 'bold',
        paddingLeft: globalSpaces.paddingLeft5,
        paddingVertical: globalSpaces.paddingVertical20
    },
    txt: {
        fontFamily: globalFontFamily.fontNunitoRegular,
        letterSpacing: 1,
        color: globalColors.textLightGrey,
        fontSize: 16,
        fontWeight: '600',
        paddingTop: globalSpaces.paddingTop5
    },
})
