import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, ScrollView, TouchableOpacity, Image, PermissionsAndroid } from 'react-native'
import CustomButton from '../components/CustomButton'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import Icon from 'react-native-vector-icons/AntDesign';
import FIcon from 'react-native-vector-icons/FontAwesome';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import BottomMenu from '../components/BottomMenu'
import AwesomeAlert from 'react-native-awesome-alerts';
import { HomeServices } from '../Services/Home.services';
import Loader from '../components/Loader';
import { globalColors, globalSpaces, globalStyles, globalHeadingFont, globalFontFamily } from '../globals/theme'
import { launchCamera, launchImageLibrary, ImagePicker } from 'react-native-image-picker';
import { RNS3 } from 'react-native-s3-upload';
import { RFPercentage } from 'react-native-responsive-fontsize';
import QB from "quickblox-react-native-sdk"
import AsyncStorage from '@react-native-async-storage/async-storage';
import CommonAlert from '../components/CommonAlert';
import Lightbox from 'react-native-lightbox';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

const bio_data = {
    phone_number: "",
    country: "",
    website: "",
    city: "",
    me: "",
    like: "",
    dislike: "",
    user: "",
    photo_url: ""
}

export default function UserProfile(props) {
    const [bioData, setBioData] = useState(bio_data);
    const [chooseImageOption, setChooseImageOption] = useState(false);
    const [isLoader, setIsLoader] = useState(false);
    const [isCover, setIsCover] = useState(false);
    const [userId, setUserId] = useState('');
    const [myLikePost, setMyLikePost] = useState(0);
    const [myDisLikePost, setMyDisLikePost] = useState(0);
    const [followData, setFollowData] = useState({});
    const [alert, setAlert] = useState(false);
    const [favouriteLikesCount, setFavouriteLikesCount] = useState(0);
    const [favouriteDislikesCount, setFavouriteDislikesCount] = useState(0);

    useEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.type == 'show_profile') {
                getUserDetails(props.route.params.refreshPage.userId)
                getFollowData(props.route.params.refreshPage.userId);
                setIsLoader(true);
                checkPermission();
            }
        }
    }, [props]);

    const checkPermission = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,)
            }
        })
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
            }
        })
    }

    const getUserDetails = async (id) => {
        if (id != "") {
            setUserId(id)
            getPostStats(id);
            getBioData(id);
            getFavouritesData(id);
        }
    }

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                getPostStats(userData.userId);
                getBioData(userData.userId);
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    var getPostStats = async (uid) => {
        await homeServices.getMyPosts('like',uid).then(
            (data) => {
                setIsLoader(false)
                if (data) {
                    setMyLikePost(data.count)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
        await homeServices.getMyPosts('dislike',uid).then(
            (data) => {
                setIsLoader(false)
                if (data) {
                    setMyDisLikePost(data.count)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    var getBioData = async (uid) => {
        await homeServices.getUserDetails(uid).then(
            (data) => {
                setIsLoader(false)
                console.log('user details', data)
                if (data) {
                    setBioData(data)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    var getFollowData = async (id) => {
        await homeServices.getFollowData(id).then(
            (data) => {
                setIsLoader(false)
                if (data.status) {
                    setFollowData(data.data)
                    console.log("uuuu", data.data)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const handleMyPosts = (type) => {
        props.navigation.navigate('MyPosts', {
            refreshPage: {
                refresh: Math.random(),
                type: type,
                check:'userProfile',
                user_id:userId
            },
        })
    }

    const handleFollowersAndFollowings = (data, type) => {
        props.navigation.navigate('FollowersAndFollowings', {
            refreshPage: {
                refresh: Math.random(),
                type: type,
                list: data
            },
        })
    }

    var getFavouritesData = async (id) => {
        await homeServices.getFavouritesData('like',id).then(
            (data) => {
                if (data.results) {
                    setFavouriteLikesCount(data.count)
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
        await homeServices.getFavouritesData('dislike',id).then(
            (data) => {
                if (data.results) {
                    setFavouriteDislikesCount(data.count);
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    const handleFavPosts = (type) => {
        props.navigation.navigate('Favourites', {
            refreshPage: {
                refresh: Math.random(),
                check: 'fav',
                type: type,
                user_id:userId
            },
        })
    }

    return (<>
        {isLoader && <Loader />}
        <ScrollView style={{ height: height }}>
            <View style={styles.main}>
                <View style={{ position: 'relative', width: width }}>
                    {bioData.cover_photo_url ?
                        <View style={{ position: 'relative', width: width }}>
                            <Lightbox navigator={props.navigator} style={{ justifyContent: "center" }}
                                renderContent={() => (
                                    <Image
                                        source={bioData.cover_photo_url ? { uri: bioData.cover_photo_url } : require('../assets/example.png')}
                                        style={{ alignSelf: "center", minWidth: 300, minHeight: 300 }}
                                    />
                                )}>
                                <Image style={styles.cover_img} source={bioData.cover_photo_url ? { uri: bioData.cover_photo_url } : require('../assets/example.png')} />
                            </Lightbox>
                        </View>
                        :
                        <View style={styles.cover}>
                        </View>
                    }
                    {bioData.photo_url ?
                        <View style={{ position: 'absolute', width: width, top: 110, zIndex: 99 }}>
                            <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                <View style={{ position: 'relative', width: 150 }}>
                                    <Lightbox navigator={props.navigator} style={{ justifyContent: "center" }}
                                        renderContent={() => (
                                            <Image
                                                source={bioData.photo_url ? { uri: bioData.photo_url } : require('../assets/example.png')}
                                                style={{ alignSelf: "center", minWidth: 300, minHeight: 300 }}
                                            />
                                        )}>
                                        <Image style={styles.circle_img} source={bioData.photo_url ? { uri: bioData.photo_url } : require('../assets/example.png')} />
                                    </Lightbox>
                                </View>
                            </View>
                        </View>
                        :
                        <View style={{ position: 'absolute', width: width, top: 110, zIndex: 99 }}>
                            <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                <View style={[styles.circle, { backgroundColor: 'grey' }]}>
                                </View>
                            </View>
                        </View>
                    }
                </View>
                <View style={styles.description}>
                    <Text style={styles.name}>{bioData.first_name} {bioData.last_name && bioData.last_name}</Text>
                    <Text style={styles.name}>@{bioData.username}</Text>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                        <Text style={styles.from}>From: </Text>
                        <Text style={styles.text}>{bioData.city}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={styles.me}>Me, in 3 words: </Text>
                        <Text style={styles.text}>{bioData.me}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={styles.love}>Things I Love: </Text>
                        <Text style={styles.text}>{bioData.like}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={styles.dislike}>Things I Dislike: </Text>
                        <Text style={styles.text}>{bioData.dislike}</Text>
                    </View>
                </View>
                <Text style={{ textAlign: 'center', fontSize: RFPercentage(2.7), fontWeight: 'bold' }}>Favourites:</Text>
                <View style={{ marginHorizontal: 5 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <CustomButton text={`Likes (${favouriteLikesCount ? favouriteLikesCount : 0})`} btnAction={() => handleFavPosts('like')} enabled={true} btnWidth={48} invertColour={'#108A07'} textColor="#fff" textAlign={'left'} />
                        <CustomButton text={`Dislikes (${favouriteDislikesCount ? favouriteDislikesCount : 0})`} invertColour={'#bf1414'} btnAction={() => handleFavPosts('dislike')} enabled={true} btnWidth={48} isIcon={true} textAlign={"left"} />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <CustomButton text={`Followers (${followData.followers && followData.followers.length})`} btnAction={() => handleFollowersAndFollowings(followData.followers, 'Followers')} enabled={true} btnWidth={48} invertColour={'#6C0AC7'} textColor="#fff" isIcon={true} textAlign={'left'} />
                        <CustomButton text={`Following (${followData.following && followData.following.length})`} btnAction={() => handleFollowersAndFollowings(followData.following, 'Following')} enabled={true} btnWidth={48} invertColour={'#6C0AC7'} textColor="#fff" isIcon={true} textAlign={'left'} />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <CustomButton text={`Things I Love(${myLikePost ? myLikePost : 0})`} btnAction={() => handleMyPosts('like')} enabled={true} btnWidth={48} invertColour={'#108A07'} textColor="#fff" isIcon={true} icon={<Icon name="heart" size={20} color="#fff" />} textAlign={'left'} />
                        <CustomButton text={`Things I Dislikes(${myDisLikePost ? myDisLikePost : 0})`} invertColour={'#bf1414'} btnAction={() => handleMyPosts('dislike')} enabled={true} btnWidth={48} isIcon={true} icon={<Icon name="dislike1" size={20} color="#fff" style={{ left: 10, top: 2 }} />} textAlign={"left"} />
                    </View>
                </View>
            </View>
        </ScrollView>
        <BottomMenu
            navigation={props.navigation}
        />
        <CommonAlert showAlert={alert} message={'Picture uploaded'} hideAlert={(val) => setAlert(val)} />
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        marginBottom: 10,
        width: width
    },
    cover_img: {
        width: '100%',
        resizeMode: 'cover',
        height: 200
    },
    edit_cover: {
        position: 'absolute',
        top: 5,
        right: 10,
        backgroundColor: '#fff',
        borderRadius: 50 / 2,
        padding: 5,
        paddingRight: 0,
        zIndex: 99999
    },
    edit_profile: {
        position: 'absolute',
        top: 10,
        right: 10,
        backgroundColor: '#fff',
        borderRadius: 50 / 2,
        padding: 5,
        paddingRight: 0,
        zIndex: 99999
    },
    cover: {
        alignSelf: 'center',
        height: 200
    },
    circle_img: {
        height: 150,
        width: 150,
        borderRadius: 150 / 2,
        borderWidth: 1,
        borderColor: '#fff'
    },
    circle: {
        height: 150,
        width: 150,
        borderRadius: 150 / 2,
        borderColor: '#fff',
        borderWidth: 0.5,
        alignSelf: 'center',
        backgroundColor: '#fff',
        marginTop: 10
    },
    profile_txt: {
        textAlign: 'center',
        width: '90%',
        alignSelf: 'center',
        marginTop: 60,
        color: '#6C0AC7',
        fontFamily: globalFontFamily.fontNunitoRegular,
        letterSpacing: 1,
        fontSize: 12,
        fontWeight: '800',
    },
    description: {
        marginTop: 60,
        marginBottom: 10,
        paddingHorizontal: 10
    },
    name: {
        fontFamily: globalFontFamily.fontNunitoBold,
        letterSpacing: 1,
        color: globalColors.gray,
        fontSize: 22,
        fontWeight: '600',
        textAlign: 'center',
        marginTop: 10
    },
    from: {
        fontFamily: globalFontFamily.fontNunitoBold,
        letterSpacing: 1,
        color: globalColors.gray,
        fontSize: 22,
        fontWeight: '800',
    },
    me: {
        fontFamily: globalFontFamily.fontNunitoBold,
        letterSpacing: 1,
        color: '#6C0AC7',
        fontSize: 23,
        fontWeight: '800',
    },
    love: {
        fontFamily: globalFontFamily.fontNunitoBold,
        letterSpacing: 1,
        color: '#108A07',
        fontSize: 23,
        fontWeight: '800',
    },
    dislike: {
        color: '#BF1414',
        fontFamily: globalFontFamily.fontNunitoBold,
        letterSpacing: 1,
        fontSize: 23,
        fontWeight: '800',
    },
    text: {
        fontSize: 23,
        color: '#000'
    },
    cover_txt: {
        color: '#6C0AC7',
        fontFamily: globalFontFamily.fontNunitoRegular,
        letterSpacing: 1,
        fontSize: 15,
        fontWeight: '600',
        marginTop: 50
    }
})
