import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, FlatList, Image, ActivityIndicator, Dimensions, KeyboardAvoidingView, TouchableOpacity, ToastAndroid, AlertIOS, Platform } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { HomeServices } from '../Services/Home.services';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import Lightbox from 'react-native-lightbox';
const { width, height } = Dimensions.get('window');

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function FollowersAndFollowings(props) {
    const [userLists, setUserLists] = useState([]);
    const [isrefresh, setIsRefresh] = React.useState(false);
    const [followType, setFollowType] = useState('');

    useEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.type) {
                setFollowType(props.route.params.refreshPage.type);
                setUserLists(props.route.params.refreshPage.list);
            }
        }
    }, [props]);

    var saveFollowData = async (uid) => {
        var payload = {
            following: uid
        }
        await homeServices.saveFollowData(payload).then(
            (data) => {
                if (data) {
                    if (data.response) {
                        var msg = data.response;
                    } else {
                        var msg = "Now following"
                    }
                    if (Platform.OS === 'android') {
                        ToastAndroid.show(msg, ToastAndroid.SHORT)
                    } else {
                        AlertIOS.alert(msg);
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    var handleUnfollow = async (uid) => {
        var payload = {
            following: uid
        }
        await homeServices.saveFollowData(payload).then(
            (data) => {
                if (data) {
                    if (data.response) {
                        var msg = data.response;
                    } else {
                        var msg = "Now following"
                    }
                    if (Platform.OS === 'android') {
                        ToastAndroid.show(msg, ToastAndroid.SHORT)
                    } else {
                        AlertIOS.alert(msg);
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    const checkUsername = (val) => {
        if (val.includes("@")) {
            return val;
        } else {
            return '@' + val;
        }
    }

    const redirectToUserProfile = (id) => {
        if (props.userId != id) {
            props.navigation.navigate('UserProfile', {
                refreshPage: {
                    refresh: Math.random(),
                    type: 'show_profile',
                    userId: id,
                },
            })
        } else {
            props.navigation.navigate('Profile', {
                refreshPage: {
                    refresh: Math.random(),
                },
            })
        }
    }

    const renderItem = ({ item, index }) => {
        return <View>
            <View style={{ flexDirection: 'row', width: '90%', alignItems: 'center', padding: 10, justifyContent: 'space-between' }}>
                <View style={{ width: vw * 15 }}>
                    <Lightbox navigator={props.navigator} style={{ justifyContent: "center" }}
                        renderContent={() => (
                            <Image
                                source={{
                                    uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                                }}
                                style={{ alignSelf: "center", minWidth: 300, minHeight: 300 }}
                            />
                        )}>
                        <Image
                            style={styles.img}
                            source={{
                                uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                            }}
                        />
                    </Lightbox>
                </View>
                <TouchableOpacity onPress={() => redirectToUserProfile(item.uuid)}>
                    <View style={{ marginLeft: 20, width: vw * 65 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: RFPercentage(2) }}>{checkUsername(item.username)}</Text>
                        <Text style={{ fontSize: RFPercentage(2.7) }}>{item.first_name} {item.last_name && item.last_name}</Text>
                    </View>
                </TouchableOpacity>
                {item.is_following ?
                    <View style={{ marginRight: 15, width: vw * 15 }}>
                        <TouchableOpacity onPress={() => handleUnfollow(item.uuid)}>
                            <FIcon5 name="user-minus" size={20} color="#6c0ac7" />
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{ marginRight: 15, width: vw * 15 }}>
                        <TouchableOpacity onPress={() => saveFollowData(item.uuid)}>
                            <FIcon5 name="user-plus" size={20} color="#6c0ac7" />
                        </TouchableOpacity>
                    </View>
                }
            </View>
        </View>
    }

    return (<>
        <View>
            <Text style={{ textAlign: 'center', fontSize: RFPercentage(3), marginTop: 10, fontWeight: 'bold' }}>
                {userLists && userLists.length} {followType && followType != '' && followType}
            </Text>
            {userLists && userLists.length ?
                <FlatList
                    style={{ width: width, marginBottom: 120 }}
                    keyExtractor={(item, index) => index}
                    data={userLists}
                    renderItem={(item, index) => renderItem(item, index)}
                    // onEndReached={() => loadMoreData()}
                    // onEndReachedThreshold={0.9}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    ListFooterComponent={
                        isrefresh &&
                        <ActivityIndicator color="#6C0AC7" />
                    }
                />
                :
                <Text style={{ textAlign: 'center' }}>No user found</Text>
            }
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    img: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2
    }
})
