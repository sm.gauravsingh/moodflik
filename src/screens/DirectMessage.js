import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Modal, Dimensions, Pressable, Image, NativeEventEmitter } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import QB from "quickblox-react-native-sdk"
import config from '../QBConfig'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/Loader'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AwesomeAlert from 'react-native-awesome-alerts';
import { HomeServices } from '../Services/Home.services';
import { NotificationHandler } from '../Services/NotificationHandler';
import { useFocusEffect } from '@react-navigation/native';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

const defaultImg = require('../assets/default_profile.png')
const emitter = new NativeEventEmitter(QB.chat);
const homeServices = new HomeServices();
const notificationHandler = new NotificationHandler();

export default function DirectMessage(props) {
    const [senderId, setSenderId] = useState('');
    const [userId, setUserId] = useState('');
    const [dialogList, setDialogeList] = useState([]);
    const [isLoader, setIsLoader] = useState(false);
    const [phoneDialogue, setPhoneDialogue] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [unauthorised, setUnauthorised] = useState(false);
    const [searchValue, setSearchValue] = useState('');

    const filter = {
        field: QB.chat.DIALOGS_FILTER.FIELD.NAME,
        operator: QB.chat.DIALOGS_FILTER.OPERATOR.CTN,
        value: searchValue
    };

    // sorted ascending by "last_message_date_sent"
    const sort = {
        field: QB.chat.DIALOGS_SORT.FIELD.LAST_MESSAGE_DATE_SENT,
        ascending: true
    };

    useEffect(() => {
        // notificationHandler.pushNotification("testing")
        getSenderId()
    }, []);

    // handleDownload = async () => {
    //     RNFetchBlob.config({
    //         fileCache: true,
    //         appendExt: 'png',
    //     })
    //         .fetch('GET', 'https://s3.us-east-2.amazonaws.com/moodflik-portal/img%2Frn_image_picker_lib_temp_d3e52cc6-54e1-48d8-bd99-8385a6b38642.jpg')
    //         .then(res => {
    //             CameraRoll.saveToCameraRoll(res.data, 'photo')
    //                 .then(res => console.log(res))
    //                 .catch(err => console.log(err))
    //             alert("saved")
    //         })
    //         .catch(error => console.log(error));
    // };

    useEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage.refresh) {
            getDialogs()
        }
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.type == 'send_post') {
                sendPostToUsers(props.route.params.refreshPage.userlist, props.route.params.refreshPage.selectedpost)
            }
        }
    }, [props]);

    var sendPostToUsers = async (userlist, postId) => {
        setIsLoader(true);
        userlist.map((x, i) => {
            setTimeout(() => {
                createDialogue(x, postId, true);
                if (i == userlist.length - 1) {
                    setIsLoader(false);
                }
            }, 100);
        })
    }

    var getSenderId = async () => {
        try {
            const value = await AsyncStorage.getItem('_moodflikAppData')
            if (value && value !== null) {
                var udata = JSON.parse(value);
                if (udata._qbUid == "") {
                    setUserId(udata.userId);
                    checkLogin(udata.userId)
                } else {
                    setUserId(udata.userId);
                    setSenderId(udata._qbUid);
                    getDialogs()
                }
            }
        } catch (e) {
            // error reading value
        }
    }

    var checkLogin = async (uid) => {
        await homeServices.getUserDetails(uid).then(
            (data) => {
                if (data.username) {
                    QB.auth
                        .login({
                            login: data.username,
                            password: 'qb_moodflik_test'
                        })
                        .then(function (info) {
                            setSenderId(info.user.id)
                            getDialogs()
                            setUnauthorised(false);
                        })
                        .catch(function (e) {
                            console.log("login.error", e);
                            if (data.phone_number == null || data.phone_number == '') {
                                setPhoneDialogue(true);
                                setUnauthorised(true);
                            }
                            if (data.phone_number && data.phone_number != '') {
                                reconnectQB(data);
                            }
                        });
                }
            },
            (error) => {
                console.log("error.response.status", error);
            });
    }

    const setQBUID = async (uid, userbiodata) => {
        try {
            var userData1 = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData1);
            if (userData && userData != "" && userData.userId) {
                var loginData = {
                    ...userData,
                    _qbUid: uid
                }
                try {
                    await AsyncStorage.setItem('_moodflikAppData', JSON.stringify(loginData));
                    var payload = {
                        occupant_id: uid
                    }
                    await homeServices.updateUserDetails(payload, userbiodata.uuid).then(
                        (data) => {
                            if (data.status) {
                                console.log("Successfully Updated Occupant id", data)
                            } else {
                                console.log("Updated Occupant id failed", data)
                            }
                        },
                        (error) => {
                            console.log("error.response.status", error);
                        }
                    );
                } catch (err) {
                    console.log("Error Update id", err);
                }
            }
        } catch (err) {
            console.log(err);
        }
    }

    const reconnectQB = (udata) => {
        QB.users
            .create({
                email: udata.email,
                fullName: udata.first_name + ' ' + udata.last_name,
                login: udata.username,
                password: 'qb_moodflik_test',
                phone: udata.phone_number,
                tags: ['moodflik', 'quickblox']
            })
            .then(function (user) {
                console.log("User created:", user)
                setQBUID(user.id, udata)
                checkLogin(userId);
            })
            .catch(function (e) {
                console.log("Error 3:", e)
            });
    }

    const updateData = async () => {
        var payload = {
            phone_number: phoneNumber,
        };
        setIsLoader(true)
        await homeServices.updateUserDetails(cleanPayload, userId).then(
            (udata) => {
                setIsLoader(false)
                if (udata) {
                    reconnectQB(udata)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const getDialogs = () => {
        setIsLoader(true)
        QB.chat
            .isConnected()
            .then(function (connected) {
                if (connected) {
                    QB.chat
                        .getDialogs({
                            filter: filter,
                            sort: sort,
                            limit: 100,
                            // skip: 0,
                        })
                        .then(function (result) {
                            setIsLoader(false)
                            formatDialog(result.dialogs);
                            console.log("dialoggg", result.dialogs)
                            setDialogeList(result.dialogs && result.dialogs)
                        })
                        .catch(function (e) {
                            setIsLoader(false)
                            console.log("Dialogue Error 1:", e)
                        });
                }
            })
            .catch(function (e) {
                setIsLoader(false)
                console.log("Dialogue Error 2:", e)
            });
    }

    const createDialogue = (occupantId, postId, bulkSend = false) => {
        QB.chat
            .createDialog({
                type: QB.chat.DIALOG_TYPE.CHAT,
                occupantsIds: [occupantId]
            })
            .then(function (dialog) {
                console.log("dialog 1:", dialog)
                if (bulkSend) {
                    sendMessage(dialog, postId, postId != "" ? 'post' : 'chat')
                } else {
                    props.navigation.navigate('Chat', {
                        refreshPage: {
                            refresh: Math.random(),
                            dialogId: dialog.id,
                            sender_id: senderId,
                            user_id: userId
                        },
                    })
                }
            })
            .catch(function (e) {
                console.log("Dialogue Error 3:", e)
            });
    }

    const sendMessage = (dialog, postId, type = 'chat') => {
        var tempBody = "";
        if (type == 'post') {
            tempBody = "[" + postId + "][post]";
        }
        const message = {
            dialogId: dialog.id,
            body: tempBody,
            saveToHistory: true
        };
        QB.chat
            .sendMessage(message)
            .then(function () {
                if (props.route.params.refreshPage.userlist.length == 1) {
                    joinDialogue(dialog)
                } else {
                    getDialogs()
                }
            })
            .catch(function (e) {
                alert(e)
            })
    }

    const joinDialogue = (dialogData) => {
        QB.chat
            .joinDialog({ dialogId: dialogData.id })
            .then(function () {
                props.navigation.navigate('Chat', {
                    refreshPage: {
                        refresh: Math.random(),
                        dialogId: dialogData.id,
                        dialogName: dialogData.name,
                        sender_id: senderId,
                        user_id: userId
                    },
                })
            })
            .catch(function (e) {
                console.log("Error 4:", e)
            })
    }

    const handleUserList = () => {
        if (unauthorised) {
            setPhoneDialogue(true);
        } else {
            props.navigation.navigate('UsersList', {
                refreshPage: {
                    refresh: Math.random(),
                },
            })
        }
    }

    const formatTime = (d) => {
        var date = new Date(d);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    const formatDialog = async (dialogues) => {
        dialogues.length && dialogues.map(async (x) => {
            x.userData = await getUserData(x.occupantsIds)
        })
        setDialogeList([...dialogues])
    }

    const getUserData = async (ids) => {
        var occId = ids.filter(item => item != senderId)
        await homeServices.getUserData(occId).then(
            (data) => {
                if (data) {
                    return data[0]
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const checkPostType = (body) => {
        var matches = body.match(/\[([^\][]*)]/g);
        if (matches && matches.length > 0) {
            var regex = /\[([^\][]*)]/g;
            var results = [], m;
            while (m = regex.exec(body)) {
                results.push(m[1]);
            }
            if (results && results.length > 0 && results[1]) {
                if (results[1] == 'post') {
                    return 'Post';
                } else if (results[1] == 'image') {
                    return 'Image';
                } else {
                    return 'text';
                }
            } else {
                return 'text';
            }
        } else {
            return 'text';
        }
    }

    const getLastMessage = (msg) => {
        if (checkPostType(msg) == 'text') {
            return msg;
        } else {
            return checkPostType(msg)
        }
    }

    return (<>
        {isLoader &&
            <Loader />
        }
        <ScrollView>
            <View>
                {dialogList.length > 0 ? dialogList.map((y, i) => {
                    return <TouchableOpacity onPress={() => joinDialogue(y)}>
                        <View style={styles.chat_box}>
                            <View style={styles.image_box}>
                                <Image style={styles.profile_img} source={defaultImg} />
                            </View>
                            <View style={styles.detail_box}>
                                <Text style={styles.name}>{y.name}</Text>
                                <Text style={styles.msg}>{getLastMessage(y.lastMessage)}</Text>
                            </View>
                            <View style={styles.info_box}>
                                <Text style={styles.time}>{formatTime(y.lastMessageDateSent)}</Text>
                                {y.unreadMessagesCount &&
                                    <View style={styles.unreadBox}>
                                        <Text style={styles.unread}>{y.unreadMessagesCount}</Text>
                                    </View>
                                }
                            </View>
                        </View>
                    </TouchableOpacity>
                })
                    :
                    <View style={{ flexDirection: 'column', justifyContent: 'center', height: 80 * vh }}>
                        <Text style={{ textAlign: 'center', fontSize: RFPercentage(4), fontStyle: 'italic' }}>No Chat Found</Text>
                    </View>
                }
            </View>
        </ScrollView>
        <View style={styles.floating_menu}>
            <TouchableOpacity onPress={() => handleUserList()}>
                <View style={styles.menu_box}>
                    <MIcon name={"chat-plus"} color="#fff" style={{ textAlign: 'center' }} size={RFPercentage(4)} />
                </View>
            </TouchableOpacity>
        </View>
        <AwesomeAlert
            show={phoneDialogue}
            showProgress={false}
            title={`Update your phone number to use chat`}
            // message={`Select Option:`}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={true}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="Go Back"
            confirmText="Go To Profile Setting"
            cancelButtonColor='#28a745'
            confirmButtonColor="#6C0AC7"
            onCancelPressed={() => {
                props.navigation.goBack()
                setPhoneDialogue(false)
            }}
            onConfirmPressed={() => {
                props.navigation.navigate('ProfileSetting')
                setPhoneDialogue(false)
            }}
        />
    </>
    )
}

const styles = StyleSheet.create({
    chat_box: {
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: '#000',
        borderBottomWidth: 0.5

    },
    name: {
        fontSize: RFPercentage(3)
    },
    msg: {
        fontSize: RFPercentage(2),
        color: '#444'
    },
    image_box: {
        width: 65,
    },
    profile_img: {
        width: 50,
        height: 50,
        borderWidth: 0.5,
        borderRadius: 70 / 2,
        borderColor: '#000'
    },
    detail_box: {
        width: vw * 60,
    },
    info_box: {
    },
    time: {
        fontSize: RFPercentage(2),
        color: '#444'
    },
    unreadBox: {
        backgroundColor: '#58BF30',
        width: 20,
        height: 20,
        borderRadius: 20 / 2,
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 10
    },
    unread: {
        fontSize: RFPercentage(2),
        color: '#fff',
        textAlign: 'center'
    },
    floating_menu: {
        backgroundColor: '#6C0AC7',
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        position: 'absolute',
        bottom: 20,
        right: 20,
        zIndex: 999
    },
    menu_box: {
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
    }
})
