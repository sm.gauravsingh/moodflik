import React, { useRef, useCallback, Component, useState, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView, DrawerLayoutAndroid, KeyboardAvoidingView, Image, Dimensions, TouchableOpacity, Alert, Platform } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
// import ImagePicker  from "react-native-image-picker";
import Loader from '../components/Loader'
import CustomButton from '../components/CustomButton'
import CustomTextArea from '../components/CustomTextArea'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import BottomMenu from '../components/BottomMenu'
import NavigationView from "../components/NavigationView";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import FIcon from 'react-native-vector-icons/FontAwesome';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Header from '../components/Header';
import { RNS3 } from 'react-native-s3-upload';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { HomeServices } from '../Services/Home.services';
import DocumentPicker from 'react-native-document-picker';
import Video from 'react-native-video';
import AwesomeAlert from 'react-native-awesome-alerts';
import Media from '../components/Media';
import {
    GiphyDialog,
    GiphyDialogConfig,
    GiphyDialogEvent,
    GiphyDialogMediaSelectEventHandler,
    GiphyMedia,
} from '@giphy/react-native-sdk'
import '../giphy.setup'
import { DEFAULT_DIALOG_SETTINGS, GiphyDialogSettings } from '../Settings'

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

const bio_data = {
    phone_number: "",
    country: "",
    website: "",
    city: "",
    me: "",
    like: "",
    dislike: "",
    user: "",
    photo_url: ""
}

export default function EditPost(props) {
    const drawer = useRef(null);

    const [isLoader, setIsLoader] = useState(false);
    const [likeSection, setLikeSection] = useState(true);
    const [lovingText, setLovingText] = useState("");
    const [lovingTextWhy, setLovingTextWhy] = useState("");
    const [dislikeText, setDislikeText] = useState("");
    const [dislikeTextWhy, setDislikeTextWhy] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [chooseImageOption, setChooseImageOption] = useState(false);
    const [bioId, setBioId] = useState('');
    const [bioData, setBioData] = useState(bio_data);
    const [userId, setUserId] = useState('');
    const [mediaUrl, setMediaUrl] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [gifUri, setGifUri] = useState("");
    const [videoUri, setVideoUri] = useState("");
    const [fileUri, setFileuri] = useState("");
    const [mediaType, setMediaType] = useState("");
    const [charContentCount, setCharContentCount] = useState(0);
    const [charWhyContentCount, setCharWhyContentCount] = useState(0);
    const [medias, setMedias] = useState<GiphyMedia[]>([]);
    const [giphyDialogSettings, setGiphyDialogSettings] = useState<GiphyDialogConfig>(DEFAULT_DIALOG_SETTINGS)

    const mediasRef = useRef(medias)
    mediasRef.current = medias

    const addMedia = useCallback((media: GiphyMedia) => {
        setMedias([media, ...mediasRef.current])
        setGifUri(media.url)
        setMediaUrl(media.url)
        setMediaType('gif')
        console.log('giiiffff', media.url)
    }, [])


    useEffect(() => {
        getUser();
        setIsLoader(true)
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.item) {
                const postData = props.route.params.refreshPage.item;
                if (postData.post_type == 'like') {
                    setLovingText(postData.content)
                    setLovingTextWhy(postData.why_content)
                    setLikeSection(true)
                } else {
                    setDislikeText(postData.content)
                    setDislikeTextWhy(postData.why_content)
                    setLikeSection(false)
                }
                setCharContentCount(postData.content.length)
                setCharWhyContentCount(postData.why_content.length)
                setMediaUrl(postData.media_url);
                setMediaType(postData.content_type);
            }
        }
    }, []);


    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                getBioData(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        const handler: GiphyDialogMediaSelectEventHandler = (e) => {
            addMedia(e.media)
            GiphyDialog.hide()
        }
        const listener = GiphyDialog.addListener(
            GiphyDialogEvent.MediaSelected,
            handler
        )
        return () => {
            listener.remove()
        }
    }, [addMedia])

    var getBioData = async (uid) => {
        await homeServices.getUserDetails(uid).then(
            (data) => {
                console.log("nionionio", data)
                setIsLoader(false)
                if (data && data.uuid) {
                    setBioId(data.uuid);
                    setBioData(data)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }


    const handleMediaClick = async (fileType) => {
        if (fileType == "image") {
            setChooseImageOption(true);
            return true;
        }
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            uploadImageToS3(res, fileType);
            console.log("uploadedUrl------", res)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const handleCamera = () => {
        setChooseImageOption(false)
        var options = {
            mediaType: 'photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        }
        launchCamera(options, function (response) {
            var file = {
                uri: response.uri,
                name: response.fileName,
                type: "image/png"
            }
            uploadImageToS3(file, 'image')
        });
    }
    const handleLibrary = () => {
        setChooseImageOption(false)
        var options = {
            mediaType: 'photo'
        }
        launchImageLibrary(options, function (response) {
            var file = {
                uri: response.uri,
                name: response.fileName,
                type: "image/png"
            }
            uploadImageToS3(file, 'image')
        })
    }

    const uploadImageToS3 = (file, fileType) => {
        setChooseImageOption(false)
        setIsLoader(true)
        const options = {
            keyPrefix: "img/",
            bucket: "moodflik-portal",
            region: "us-east-2",
            accessKey: "AKIAWFNJSIDVAQA2KNEU",
            secretKey: "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",
            successActionStatus: 201
        }
        RNS3.put(file, options).then(response => {
            if (response.status !== 201) {
                setIsLoader(false)
                throw new Error("Failed to upload image to S3");
            }
            setIsLoader(false)
            console.log("upload response", response.body)
            setMediaUrl(response.body.postResponse.location)
            if (fileType == "image") {
                setImageUrl(response.body.postResponse.location)
                setMediaType('image')
            } else if (fileType == "gif") {
                // setGifUri(response.body.postResponse.location)
                setMediaType('gif')
            } else if (fileType == "video") {
                setVideoUri(response.body.postResponse.location)
                setMediaType('video')
            } else if (fileType == "file") {
                setFileuri(response.body.postResponse.location)
                setMediaType('file')
            }
            // getUserData();
        }).catch(function (error) {
            console.log("aeee", error)
        });
    }

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem('_moodflikAppData')
            if (value && value !== null) {
                return JSON.parse(value)
            }
        } catch (e) {
            console.log(e)
        }
    }

    const addPost = async (type) => {
        setIsLoader(true)
        var userData = await getData();
        if (type == "Like") {
            var payload = {
                "content": lovingText,
                "content_type": mediaType && mediaType != '' ? mediaType : 'text',
                "post_type": "like",
                "why_content": lovingTextWhy,
                "media_url": mediaUrl
            }
            await homeServices.updatePost(payload, props.route.params.refreshPage.item.uuid).then(
                (data) => {
                    setIsLoader(false)
                    console.log("Post added-", data);
                    if (data && data.uuid) {
                        setImageUrl("")
                        setFileuri("")
                        setLovingText("")
                        setDislikeText("")
                        setLovingTextWhy("")
                        setDislikeTextWhy("")
                        props.navigation.navigate('Home', {
                            refreshPage: {
                                refresh: Math.random(),
                                type: 'post_updated'
                            },
                        })
                    }
                },
                (error) => {
                    console.log("error.response.status", error);
                }
            );
        } else {
            var payload = {
                "content": dislikeText,
                "content_type": mediaType && mediaType != '' ? mediaType : 'text',
                "post_type": "dislike",
                "why_content": dislikeTextWhy,
                "media_url": mediaUrl
            }
            await homeServices.updatePost(payload, props.route.params.refreshPage.item.uuid).then(
                (data) => {
                    setIsLoader(false)
                    console.log("Post added-", data);
                    if (data && data.uuid) {
                        setImageUrl("")
                        setFileuri("")
                        setLovingText("")
                        setDislikeText("")
                        setLovingTextWhy("")
                        setDislikeTextWhy("")
                        props.navigation.navigate('Home', {
                            refreshPage: {
                                refresh: Math.random(),
                                type: 'post_updated'
                            },
                        })
                    }
                },
                (error) => {
                    console.log("error.response.status", error);
                }
            );

        }
    }

    const youtube_parser = (url) => {
        var regExp = /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
        var match = url.match(regExp);
        return (match && match[1].length==11)? true : false;
    }

    const handleLovingText = (text : string) => {
        setLovingText(text);
        if(youtube_parser(text)){
            setMediaUrl(text)
            setMediaType('thumbnail')
        }
    }

    const handleDislikeText = (text : string) => {
        setDislikeText(text);
        if(youtube_parser(text)){
            setMediaUrl(text)
            setMediaType('thumbnail')
        }
    }

    const checkUsername = (val:string) => {
        if(val.includes("@")){
            return val;
        }else{
            return '@'+val;
        }
    }

    return (<>
        <ScrollView style={styles.main}>
            {isLoader ?
                <Loader />
                : <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <View style={{ width: vw * 90, marginBottom: 100 }}>
                        <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 0, alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <View>
                                        <Image style={styles.img} source={bioData.photo_url ? { uri: bioData.photo_url } : require('../assets/example.png')} />
                                    </View>
                                    <View style={{ marginLeft: 20 }}>
                                        <Text>{bioData.first_name} {bioData.last_name && bioData.last_name}</Text>
                                        <Text>{bioData.username}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            {likeSection ?
                                <CustomButton textStyle={{ width: 70 * vw }} text={`What’s got you smiling today?`} enabled={true} btnWidth={90} invertColour={!likeSection ? '#6C0AC7' : '#108A07'} textColor="#fff" isIcon={true} icon={<Image
                                    style={{ height: 25, width: 25, marginLeft: 20 }}
                                    source={require('../assets/like.png')}
                                />} textAlign={"left"} />
                                :
                                <CustomButton textStyle={{ width: 70 * vw }} text={`What’s been getting on your nerves today?`} invertColour={likeSection ? '#6C0AC7' : '#BF1414'} enabled={true} btnWidth={90} isIcon={true} icon={<Image
                                    style={{ height: 25, width: 25, marginLeft: 20 }}
                                    source={require('../assets/dislike.png')}
                                />} textAlign={"left"} />
                            }
                        </View>
                        {likeSection ?
                            <View>
                                <Text style={{ marginTop: 15, fontWeight: 'bold', textAlign: 'center' }}>I'm Currently Loving:</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={lovingText} action={(text) => handleLovingText(text)} max={130} charCount={charContentCount} />
                                    {mediaUrl != "" &&
                                        <View style={{ padding: 5 }}>
                                            <Media url={mediaUrl} type={mediaType} />
                                        </View>
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 10 }}>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => GiphyDialog.show()}>
                                        <Icon name="gif" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("image")}>
                                        <Icon name="camera" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("video")}>
                                        <Icon name="video" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("file")}>
                                        <Icon name="attachment" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.italic}>Why? (Optional)</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={lovingTextWhy} action={(text) => setLovingTextWhy(text)} max={150} charCount={charWhyContentCount} />
                                </View>
                                <CustomButton text="Post" enabled={true} btnAction={() => addPost("Like")} btnWidth={40} />
                            </View>
                            :
                            <View>
                                <Text style={{ marginTop: 15, fontWeight: 'bold', textAlign: 'center' }}>I Currently Dislike:</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={dislikeText} action={(text) => handleDislikeText(text)} max={130} charCount={charContentCount} />
                                    {mediaUrl != "" &&
                                        <View style={{ padding: 5 }}>
                                            <Media url={mediaUrl} type={mediaType} />
                                        </View>
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 10 }}>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => GiphyDialog.show()}>
                                        <Icon name="gif" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("image")}>
                                        <Icon name="camera" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("video")}>
                                        <Icon name="video" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: RFPercentage(4), marginTop: -5 }}>/</Text>
                                    <TouchableOpacity style={styles.mediaIcon} onPress={() => handleMediaClick("file")}>
                                        <Icon name="attachment" size={RFPercentage(4)} color="#000" />
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.italic}>Why? (Optional)</Text>
                                <View style={styles.contentBox}>
                                    <CustomTextArea value={dislikeTextWhy} action={(text) => setDislikeTextWhy(text)} max={150} charCount={charWhyContentCount} />
                                </View>
                                <CustomButton text="Post" enabled={true} btnAction={() => addPost("Dislike")} btnWidth={40} />
                            </View>
                        }
                    </View>
                </View>
            }
        </ScrollView>
        <AwesomeAlert
            show={chooseImageOption}
            showProgress={false}
            title={`Choose Image`}
            message={`Select Option:`}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={true}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="Camera"
            confirmText="Gallery"
            cancelButtonColor='#28a745'
            confirmButtonColor="#6C0AC7"
            onCancelPressed={() => {
                handleCamera()
                setChooseImageOption(false)
            }}
            onConfirmPressed={() => {
                handleLibrary()
                setChooseImageOption(false)
            }}
            onDismiss={() => {
                setChooseImageOption(false)
            }}
        />
        <View style={{ position: 'absolute', top: Platform.OS === 'ios' ? height - 180 : height - 160, width: width }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={styles.footer}>
                    <BottomMenu
                        navigation={props.navigation}
                    />
                </View>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        height: height,
        width: width,
    },
    italic: {
        fontWeight: 'bold',
        fontSize: RFPercentage(2.6),
        fontStyle: 'italic',
        textAlign: 'center'
    },
    mediaIcon: {
        width: 60,
        alignItems: 'center'
    },
    img: {
        width: 70,
        height: 70,
        borderRadius: 70 / 2,
        borderColor: '#000',
        borderWidth: 0.5
    },
    contentBox: {
        borderColor: '#aaa',
        borderWidth: 0.5,
        borderRadius: 7,
        marginBottom: 5
    }
})
