import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, FlatList, ActivityIndicator, Text, View, Dimensions, Image, PermissionsAndroid, ToastAndroid, AlertIOS, Platform } from 'react-native'
import CustomButton from '../components/CustomButton'
import Like from '../components/Like'
import Dislike from '../components/Dislike'
import Icon from 'react-native-vector-icons/AntDesign';
import FIcon5 from 'react-native-vector-icons/FontAwesome5';
import FIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import BottomMenu from '../components/BottomMenu'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { HomeServices } from '../Services/Home.services';
import Loader from '../components/Loader'

import { globalColors, globalSpaces, globalStyles, globalHeadingFont, globalFontFamily } from '../globals/theme'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { RFPercentage } from 'react-native-responsive-fontsize';
import Post from '../components/Post';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();


export default function Dashboard_old(props) {
    const [likesCount, setLikesCount] = useState(0);
    const [likeSection, setLikeSection] = useState(true);
    const [dislikesCount, setDislikesCount] = useState(0);
    const [likepostPagination, setLikePostPagination] = useState({});
    const [dislikepostPagination, setDisLikePostPagination] = useState({});
    const [likesPostList, setLikesPostList] = useState([]);
    const [disLikesPostList, setDisLikesPostList] = useState([]);
    const [isLoader, setIsLoader] = useState(false);
    const [likeRefresh, setLikeListRefresh] = useState(false);
    const [dislikeRefresh, setDisLikeListRefresh] = useState(false);
    const [userId, setUserId] = useState('');

    useEffect(() => {
        getDashboardData();
        getDashboardDataDislikes();
        getUser();
        setIsLoader(true)
        checkPermission()
    }, []);

    const checkPermission = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,)
            }
        })
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
            }
        })
    }

    var getDashboardData = async () => {
        await homeServices.getDashboardData('like').then(
            (data) => {
                if (data && data.results) {
                    setIsLoader(false)
                    setLikesPostList(data.results);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setLikePostPagination(pagination)
                    setLikesCount(data.count)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    var getDashboardDataDislikes = async () => {
        await homeServices.getDashboardData('dislike').then(
            (data) => {
                if (data && data.results) {
                    // setIsLoader(false)
                    setDisLikesPostList(data.results);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setDisLikePostPagination(pagination)
                    setDislikesCount(data.count);
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    var handleLikeDisLike = async (post, type) => {
        handleSeen(post, type)
        var postData = {
            post: post.uuid,
            reaction: type
        }
        await homeServices.likeDisLikePost(postData).then(
            (data) => {
                if (data) {
                    console.log("ppp", data)
                    if (likeSection) {
                        if (type == "like") {
                            likesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.like_count = !x.is_like ? x.like_count + 1 : x.like_count
                                    x.dislike_count = x.is_dislike ? x.dislike_count - 1 : x.dislike_count
                                    x.is_like = true
                                    x.is_dislike = false
                                }
                            })
                        } else {
                            likesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.dislike_count = !x.is_dislike ? x.dislike_count + 1 : x.dislike_count
                                    x.like_count = x.is_like ? x.like_count - 1 : x.like_count
                                    x.is_dislike = true
                                    x.is_like = false
                                }
                            })
                        }
                        setLikesPostList([...likesPostList])
                    } else {
                        if (type == "like") {
                            disLikesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.like_count = !x.is_like ? x.like_count + 1 : x.like_count
                                    x.dislike_count = x.is_dislike ? x.dislike_count - 1 : x.dislike_count
                                    x.is_like = true
                                    x.is_dislike = false
                                }
                            })
                        } else {
                            disLikesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.dislike_count = !x.is_dislike ? x.dislike_count + 1 : x.dislike_count
                                    x.like_count = x.is_like ? x.like_count - 1 : x.like_count
                                    x.is_dislike = true
                                    x.is_like = false
                                }
                            })
                        }
                        setDisLikesPostList([...disLikesPostList])
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    var handleFavourite = async (post, type) => {
        handleSeen(post, type)
        var postData = {
            post: post.uuid,
        }
        if (!post.is_favourite) {
            await homeServices.addFavourite(postData).then(
                (data) => {
                    console.log("favvv", data)
                    if (data) {
                        if (likeSection) {
                            likesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.favourite_count = !x.is_favourite ? x.favourite_count + 1 : x.favourite_count
                                    x.is_favourite = true
                                }
                            })
                            setLikesPostList([...likesPostList])
                        } else {
                            disLikesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.favourite_count = !x.is_favourite ? x.favourite_count + 1 : x.favourite_count
                                    x.is_favourite = true
                                }
                            })
                            setDisLikesPostList([...disLikesPostList])
                        }
                    }
                },
                (error) => {
                    console.log("error.response.status", error);
                }
            );
        } else {
            await homeServices.removeFavourite(postData).then(
                (data) => {
                    console.log("favvv", data)
                    if (data) {
                        if (likeSection) {
                            likesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.favourite_count = x.is_favourite ? x.favourite_count - 1 : x.favourite_count
                                    x.is_favourite = false
                                }
                            })
                            setLikesPostList([...likesPostList])
                        } else {
                            disLikesPostList.map((x) => {
                                if (x.uuid == post.uuid) {
                                    x.favourite_count = x.is_favourite ? x.favourite_count - 1 : x.favourite_count
                                    x.is_favourite = false
                                }
                            })
                            setDisLikesPostList([...disLikesPostList])
                        }
                    }
                },
                (error) => {
                    console.log("error.response.status", error);
                }
            );
        }
    }

    var handleComment = async (post, type) => {
        handleSeen(post, type)
        props.navigation.navigate('Comments', {
            refreshPage: {
                refresh: Math.random(),
                post: post,
                type: type
            },
        })
    }
    var handleShare = async (post, type) => {
        handleSeen(post, type)
        if (type == "like") {
            var postData = {
                users_id: userId,
                like_post_id: post.post_id
            }
        } else {
            var postData = {
                users_id: userId,
                dislike_post_id: post.post_id
            }
        }
        await homeServices.handleShare(postData).then(
            (data) => {
                if (data) {
                    console.log(data)
                    // getDashboardData();
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }
    var handleSeen = async (post, type) => {
        var postData = {
            post: post.uuid,
        }
        await homeServices.addSeen(postData).then(
            (data) => {
                console.log("seeen", data)
                if (data.response) {
                    
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    const dateFormat = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        return result;
    }
    const check5Minute = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        if(result.includes('seconds ago')){
            return false;
        }else if(result.includes('minutes ago')){
                let firstWord = result.split(" ")[0]
                var check = parseInt(firstWord.trim());
                if(check > 4){
                    return true;
                }else{
                    return false;
                }
        }else if(!result.includes('seconds ago')){
            return true;
        }else{
            return false;
        }
    }
    const timeDifference = (current, previous) => {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        var elapsed = current - previous;
        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        }
        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }
        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        }
        else if (elapsed < msPerMonth) {
            if (Math.round(elapsed / msPerDay) > 1) {
                return Math.round(elapsed / msPerDay) + ' days ago';
            } else {
                return Math.round(elapsed / msPerDay) + ' day ago';
            }
        }
        else if (elapsed < msPerYear) {
            if (Math.round(elapsed / msPerMonth) > 1) {
                return Math.round(elapsed / msPerMonth) + ' months ago';
            } else {
                return Math.round(elapsed / msPerMonth) + ' month ago';
            }
        }
        else {
            if (Math.round(elapsed / msPerYear) > 1) {
                return Math.round(elapsed / msPerYear) + ' years ago';
            } else {
                return Math.round(elapsed / msPerYear) + ' year ago';
            }
        }
    }

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    var saveFollowData = async (uid) => {
        var payload = {
            following_user: uid
        }
        await homeServices.saveFollowData(payload).then(
            (data) => {
                if (data) {
                    var msg = "Now following"
                    if (Platform.OS === 'android') {
                        ToastAndroid.show(msg, ToastAndroid.SHORT)
                    } else {
                        AlertIOS.alert(msg);
                    }
                }
            },
            (error) => {
                console.log("error.response.status", error);
            }
        );
    }

    var getLoadMoreData = async (url) => {
        await homeServices.getLoadMoreData(url).then(
            (data) => {
                if (data.results) {
                    setIsLoader(false)
                    if (likeSection) {
                        setLikesPostList([...likesPostList, ...data.results]);
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setLikePostPagination(pagination)
                        setLikeListRefresh(false)

                    } else {
                        setDisLikesPostList([...disLikesPostList, ...data.results]);
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setDisLikePostPagination(pagination)
                        setDisLikeListRefresh(false)

                    }
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const loadMoreData = () => {
        if (likeSection) {
            if (likepostPagination && likepostPagination.next && likepostPagination.next != "") {
                getLoadMoreData(likepostPagination.next);
                setLikeListRefresh(true)
            }
        } else {
            if (dislikepostPagination && dislikepostPagination.next && dislikepostPagination.next != "") {
                getLoadMoreData(dislikepostPagination.next);
                setDisLikeListRefresh(true)
            }
        }
        // alert('load ....')
    }

    const renderItem = ({ item, index }) => {
        return (<View style={{ paddingHorizontal: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, marginBottom: 10, alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', width: '90%', alignItems: 'center' }}>
                    <View style={styles.profile_img}>
                        <Image
                            style={styles.img}
                            source={{
                                uri: item.photo_url ? item.photo_url : 'https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1601946265473x999519114820069900%2Fprofile%2520pic%2520avatar.png?w=128&h=128&auto=compress&dpr=1&fit=max',
                            }}
                        />
                    </View>
                    <View style={{marginLeft:15}}>
                        <Text>{item.user}</Text>
                        <Text>{item.first_name} {item.last_name && item.last_name}</Text>
                    </View>
                    {likeSection ?
                        <View style={{ marginTop: 30,marginLeft:15 }}>
                            <Text style={styles.label}>{check5Minute(item.created_at) ? 'was' : 'is currently'} loving:</Text>
                        </View>
                        :
                        <View style={{ marginTop: 30,marginLeft:20 }}>
                            <Text style={styles.label}>{check5Minute(item.created_at) ? '' : 'currently'} dislikes:</Text>
                        </View>
                    }
                </View>
                {userId != item.uuid &&
                    <View style={{ marginRight: 15 }}>
                        <TouchableOpacity onPress={() => saveFollowData(item.uuid)}>
                            <FIcon5 name="user-plus" size={20} color="#6c0ac7" />
                        </TouchableOpacity>
                    </View>
                }
            </View>
            {likeSection ?
                <View style={{ marginHorizontal: 10 }}>
                    <Like mediaUrl={item.media_url} mediaType={item.content_type} c_width={vw * 90} text={item.content} why={item.why_content} />
                </View>
                :
                <View style={{ marginHorizontal: 10 }}>
                    <Dislike mediaUrl={item.media_url} mediaType={item.content_type} c_width={vw * 90} text={item.content} why={item.why_content} />
                </View>
            }
            <View style={{ marginHorizontal: 5, marginBottom: 10 }}>
                <View style={{ alignSelf: 'flex-end', marginRight: 15, marginVertical: 10 }}>
                    <Text>{dateFormat(item.created_at)}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                    <View style={{ flexDirection: 'row' }}>
                        <FIcon onPress={() => handleFavourite(item, likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_favourite ? 'heart' : 'heart-o'} size={25} color="#6c0ac7" />
                        <Text>({item.favourite_count})</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon onPress={() => handleLikeDisLike(item, "like")} style={{ marginRight: 5, marginTop: -3 }} name={item.is_like ? 'like1' : 'like2'} size={25} color="#6c0ac7" />
                        <Text>({item.like_count})</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <Icon onPress={() => handleLikeDisLike(item, 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_dislike ? 'dislike1' : 'dislike2'} size={25} color="#6c0ac7" />
                        <Text>({item.dislike_count})</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <FIcon onPress={() => handleComment(item, likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: -2 }} name={item.is_comment ? 'comment' : 'comment-o'} size={25} color="#6c0ac7" />
                        <Text>({item.comments_count})</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <FIcon onPress={() => handleShare(item, likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: 0 }} name={true ? 'share-square' : 'share-square-o'} size={25} color="#6c0ac7" />
                        <Text>({item.like_count})</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <IonIcon onPress={() => handleSeen(item, likeSection ? "like" : 'dislike')} style={{ marginRight: 5, marginTop: -1 }} name={item.is_seen ? 'eye' : 'eye-outline'} size={27} color="#6c0ac7" />
                        <Text>({item.seen_count})</Text>
                    </View>
                </View>
            </View>
        </View>
        );
    };

    const renderPost = ({ item, index }) => {
        return <Post userId={userId} navigation={props.navigation} likeSection={likeSection} post={item}/>
    }

    return (<>
        <ScrollView scrollEnabled={false} style={{ height: height }}>
            {isLoader ?
                <Loader />
                :
                <View style={styles.main}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <CustomButton text={`Public Likes (${likesCount})`} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={48} invertColour={!likeSection ? '#6C0AC7' : '#108A07'} textColor="#fff" isIcon={true} icon={<Icon name="heart" size={20} color="#fff" />} textAlign={'left'} />
                        <CustomButton text={`Public Dislikes (${dislikesCount})`} invertColour={likeSection ? '#6C0AC7' : '#BF1414'} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={48} isIcon={true} icon={<Icon name="dislike1" size={20} color="#fff" style={{ left: 5, top: 2 }} />} textAlign={"left"} />
                    </View>
                    {likeSection ?
                        <FlatList
                            style={{ width: width, marginBottom: 170 }}
                            keyExtractor={(item, index) => index}
                            data={likesPostList}
                            key={'likesection'}
                            // initialScrollIndex={likepostIndex}
                            // renderItem={(item, index) => renderItem(item, index)}
                            renderItem={(item, index) => renderPost(item,index)}
                            onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            // ListFooterComponent={this.renderFooter.bind(this)}
                            ListFooterComponent={
                                likeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                        :
                        <FlatList
                            style={{ width: width, marginBottom: 170 }}
                            keyExtractor={(item, index) => index}
                            data={disLikesPostList}
                            key={'dislikesection'}
                            // initialScrollIndex={dislikepostIndex}
                            // renderItem={(item, index) => renderItem(item, index)}
                            renderItem={(item, index) => renderPost(item,index)}
                            onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            // onViewableItemsChanged={handleChangePost}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            ListFooterComponent={
                                dislikeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                    }
                </View>
            }
        </ScrollView>
        <BottomMenu
            navigation={props.navigation}
            activeScreen="Home"
        />
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        paddingHorizontal: 5,
        height: height
    },
    img: {
        width: 60,
        height: 60,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 60 / 2
    },
    label: {
        fontWeight: 'bold',
        color: '#000',
        fontStyle: 'italic',
        fontSize: RFPercentage(2.7),
        textAlign: 'center',
        marginBottom: 10
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
})
