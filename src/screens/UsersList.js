import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, TextInput, Platform, Dimensions, Image } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import QB from "quickblox-react-native-sdk"
import Icon from 'react-native-vector-icons/Ionicons';
import CustomInput from '../components/CustomInput'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/Loader'
import { ScrollView } from 'react-native-gesture-handler';
import { HomeServices } from '../Services/Home.services';
const { width, height } = Dimensions.get('window');
const defaultImg = require('../assets/default_profile.png')

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const w = Dimensions.get('window');
const homeServices = new HomeServices();

export default function UsersList(props) {
    const [isLoader, setIsLoader] = useState(false);
    const [isSeacrh, setIsSearch] = useState(false);
    const [userList, setUserList] = useState([]);
    const [userCount, setUserCount] = useState(0);
    const [userId, setUserId] = useState('');
    const [senderId, setSenderId] = useState('');
    const [text, setText] = useState('');

    useEffect(() => {
        getSenderId()
        setIsLoader(true)
        getUsersList('')
    }, []);

    // useEffect(() => {
    //     console.log('ytttyytr-',text)
    // }, [text]);

    var getSenderId = async () => {
        try {
            const value = await AsyncStorage.getItem('_moodflikAppData')
            if (value && value !== null) {
                var udata = JSON.parse(value);
                setUserId(udata.userId);
                setSenderId(udata._qbUid);
            }
        } catch (e) {
            // error reading value
        }
    }

    var getUsersList = async (q) => {
        await homeServices.getUserList(q).then(
            (data) => {
                console.log("Query-", q)
                // console.log("Youuu-", data)
                setIsLoader(false)
                if (data) {
                    setUserList(data.results)
                    setUserCount(data.count)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const joinDialogue = (occupantId) => {
        console.log('aoccupantId', occupantId)
        QB.chat
            .createDialog({
                type: QB.chat.DIALOG_TYPE.CHAT,
                occupantsIds: [occupantId]
            })
            .then(function (dialog) {
                console.log("dialog 1:", dialog)
                props.navigation.navigate('Chat', {
                    refreshPage: {
                        refresh: Math.random(),
                        dialogId: dialog.id,
                        dialogName: dialog.name,
                        sender_id: senderId,
                        user_id: userId
                    },
                })
            })
            .catch(function (e) {
                console.log("Dialogue Error 3:", e)
            });
    }

    var handleOnchange = (q) => {
        setText(q)
        getUsersList(q);
    }

    return (<>
        <View style={styles.header}>
            <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10, marginTop: 10, width: vw * 10 }} onPress={props.navigation.goBack}>
                <Icon name={"arrow-back"} color="#fff" size={RFPercentage(5)} />
            </TouchableOpacity>
            <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10, width: vw * 70 }}>
                <Text style={styles.title}>Select Contact</Text>
                <Text style={styles.total}>{userCount} contacts</Text>
            </View>
            <View style={{ flexDirection: 'column', justifyContent: 'center', width: vw * 10 }}>
                <TouchableOpacity onPress={() => setIsSearch(!isSeacrh)}>
                    <Icon name={"search-outline"} color="#fff" style={{ textAlign: 'center' }} size={RFPercentage(4)} />
                </TouchableOpacity>
            </View>
        </View>
        {isLoader ?
            <Loader />
            :
            <>
                {isSeacrh &&
                    <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                        <View style={{ width: vw * 90 }}>
                            <TextInput placeholder="Search ..." value={text} onChangeText={(text) => handleOnchange(text)} />
                        </View>
                    </View>
                }
                <ScrollView style={styles.chat_area}>
                    {userList.length > 0 && userList.map((x, i) => {
                        return <TouchableOpacity onPress={() => joinDialogue(x.occupant_id)}>
                            <View style={styles.chat_box}>
                                <View style={styles.image_box}>
                                    <Image style={styles.profile_img} source={x.photo_url ? x.photo_url + `?buster=${Math.random()}` : defaultImg} />
                                </View>
                                <View style={styles.detail_box}>
                                    <Text style={styles.name}>{x.first_name} {x.last_name}</Text>
                                    <Text style={styles.username}>{x.username}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    })

                    }
                </ScrollView>
            </>
        }
    </>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        backgroundColor: '#6C0AC7',
        height: Platform.OS === 'ios' ? 100 : 80,
        flexDirection: 'row',
        paddingTop: Platform.OS === 'ios' ? 30 : 0,
        justifyContent: 'flex-start'
    },
    title: {
        fontSize: RFPercentage(3),
        color: '#fff',
        textAlign: 'left'
    },
    total: {
        fontSize: RFPercentage(2),
        color: '#fff',
        textAlign: 'left'
    },
    chat_area: {
        // backgroundColor: '#fff',
        marginBottom: 80
    },
    chat_box: {
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: '#000',
        borderBottomWidth: 0.5

    },
    name: {
        fontSize: RFPercentage(3)
    },
    username: {
        fontSize: RFPercentage(2)
    },
    msg: {
        fontSize: RFPercentage(2),
        color: '#444'
    },
    image_box: {
        width: 65,
    },
    profile_img: {
        width: 50,
        height: 50,
        borderWidth: 0.5,
        borderRadius: 70 / 2,
        borderColor: '#000',
        resizeMode: 'cover'
    },
    detail_box: {
        width: vw * 60,
    },
    info_box: {
    },
    time: {
        fontSize: RFPercentage(2),
        color: '#444'
    },
    unreadBox: {
        backgroundColor: '#58BF30',
        width: 20,
        height: 20,
        borderRadius: 20 / 2,
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 10
    },
    unread: {
        fontSize: RFPercentage(2),
        color: '#fff',
        textAlign: 'center'
    }
})
