import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, FlatList, ActivityIndicator, Text, View, Dimensions, Image, PermissionsAndroid, ToastAndroid, AlertIOS, Platform } from 'react-native'
import CustomButton from '../components/CustomButton'
import Icon from 'react-native-vector-icons/AntDesign';
import BottomMenu from '../components/BottomMenu'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { HomeServices } from '../Services/Home.services';
import Loader from '../components/Loader'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { RFPercentage } from 'react-native-responsive-fontsize';
import Post from '../components/Post';
import CustomToast from '../components/CustomToast';
import { useFocusEffect } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function Dashboard(props) {
    const [likesCount, setLikesCount] = useState(0);
    const [likeSection, setLikeSection] = useState(true);
    const [dislikesCount, setDislikesCount] = useState(0);
    const [likepostPagination, setLikePostPagination] = useState({});
    const [dislikepostPagination, setDisLikePostPagination] = useState({});
    const [likesPostList, setLikesPostList] = useState([]);
    const [disLikesPostList, setDisLikesPostList] = useState([]);
    const [isLoader, setIsLoader] = useState(false);
    const [likeRefresh, setLikeListRefresh] = useState(false);
    const [dislikeRefresh, setDisLikeListRefresh] = useState(false);
    const [isPostAdded, setShowPostAdded] = useState(false);
    const [userId, setUserId] = useState('');

    useEffect(() => {
        getDashboardData();
        getDashboardDataDislikes();
        getUser();
        setIsLoader(true);
        checkPermission();
    }, []);

    useFocusEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.type == 'post_added' || props.route.params.refreshPage.type == 'post_updated') {
                setShowPostAdded(true);
                setTimeout(() => {
                    setShowPostAdded(false);
                    props.route.params.refreshPage.type = "";
                }, 7000);
            }
        }
    });

    const refreshScreen = () => {
        setIsLoader(true)
        setLikesPostList([]);
        setDisLikesPostList([]);
        getDashboardData();
        getDashboardDataDislikes();
        setShowPostAdded(false);
        props.route.params.refreshPage.type = "";
    }

    const checkPermission = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,)
            }
        })
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
            }
        })
    }

    var getDashboardData = async () => {
        await homeServices.getDashboardData('like').then(
            (data) => {
                if (data && data.results) {
                    setIsLoader(false)
                    console.log("ppppp", data)
                    setLikesPostList(data.results);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setLikePostPagination(pagination)
                    setLikesCount(data.count)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    var getDashboardDataDislikes = async () => {
        await homeServices.getDashboardData('dislike').then(
            (data) => {
                if (data && data.results) {
                    // setIsLoader(false)
                    setDisLikesPostList(data.results);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setDisLikePostPagination(pagination)
                    setDislikesCount(data.count);
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }


    var getLoadMoreData = async (url) => {
        await homeServices.getLoadMoreData(url).then(
            (data) => {
                if (data.results) {
                    setIsLoader(false)
                    if (likeSection) {
                        setLikesPostList([...likesPostList, ...data.results]);
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setLikePostPagination(pagination)
                        setLikeListRefresh(false)

                    } else {
                        setDisLikesPostList([...disLikesPostList, ...data.results]);
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setDisLikePostPagination(pagination)
                        setDisLikeListRefresh(false)

                    }
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const loadMoreData = () => {
        if (likeSection) {
            if (likepostPagination && likepostPagination.next && likepostPagination.next != "") {
                getLoadMoreData(likepostPagination.next);
                setLikeListRefresh(true)
            }
        } else {
            if (dislikepostPagination && dislikepostPagination.next && dislikepostPagination.next != "") {
                getLoadMoreData(dislikepostPagination.next);
                setDisLikeListRefresh(true)
            }
        }
    }

    const renderPost = ({ item, index }) => {
        return <Post navigator={props.navigator} userId={userId} navigation={props.navigation} likeSection={likeSection} post={item} />
    }

    return (<>
        <ScrollView scrollEnabled={false} style={{ height: height }}>
            {isLoader ?
                <Loader />
                :
                <View style={styles.main}>
                    {isPostAdded &&
                        <CustomToast title={props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage.type == 'post_added' ? 'New Post Added' : 'Post Updated'} text="Click here to view" action={() => refreshScreen()} onCancel={() => setShowPostAdded(false)} />
                    }
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <CustomButton text={`Public Likes (${likesCount})`} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={48} invertColour={!likeSection ? '#6C0AC7' : '#108A07'} textColor="#fff" isIcon={true} icon={<Icon name="heart" size={20} color="#fff" />} textAlign={'left'} />
                        <CustomButton text={`Public Dislikes (${dislikesCount})`} invertColour={likeSection ? '#6C0AC7' : '#BF1414'} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={48} isIcon={true} icon={<Icon name="dislike1" size={20} color="#fff" style={{ left: 5, top: 2 }} />} textAlign={"left"} />
                    </View>
                    {likeSection ?
                        <FlatList
                            style={{ width: width, marginBottom: 170 }}
                            keyExtractor={(item, index) => index}
                            data={likesPostList}
                            key={'likesection'}
                            // initialScrollIndex={likepostIndex}
                            renderItem={(item, index) => renderPost(item, index)}
                            onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            // ListFooterComponent={this.renderFooter.bind(this)}
                            ListFooterComponent={
                                likeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                        :
                        <FlatList
                            style={{ width: width, marginBottom: 170 }}
                            keyExtractor={(item, index) => index}
                            data={disLikesPostList}
                            key={'dislikesection'}
                            // initialScrollIndex={dislikepostIndex}
                            renderItem={(item, index) => renderPost(item, index)}
                            onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            // onViewableItemsChanged={handleChangePost}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            ListFooterComponent={
                                dislikeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                    }
                </View>
            }
        </ScrollView>
        <BottomMenu
            navigation={props.navigation}
            activeScreen="Home"
        />
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        paddingHorizontal: 5,
        height: height
    },
    img: {
        width: 60,
        height: 60,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 60 / 2
    },
    label: {
        fontWeight: 'bold',
        color: '#000',
        fontStyle: 'italic',
        fontSize: RFPercentage(2.7),
        textAlign: 'center',
        marginBottom: 10
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
})
