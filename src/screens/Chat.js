import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, FlatList, TouchableOpacity, NativeEventEmitter, Text, PermissionsAndroid, View, TextInput, Platform, Dimensions, Image } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import QB from "quickblox-react-native-sdk"
import Icon from 'react-native-vector-icons/Ionicons';
import CustomInput from '../components/CustomInput'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/Loader'
import { ScrollView } from 'react-native-gesture-handler';
import { HomeServices } from '../Services/Home.services';
import { ActivityIndicator } from 'react-native';
import Post from '../components/Post';
import DocumentPicker from 'react-native-document-picker';
import ChatPost from './ChatPost';
import Media from '../components/Media';
import { NotificationHandler } from '../Services/NotificationHandler';
import QBMedia from './QBMedia';
import OneSignal from 'react-native-onesignal';

const { width, height } = Dimensions.get('window');
const homeServices = new HomeServices();
const notificationHandler = new NotificationHandler();
// const emitter = new NativeEventEmitter(QB.chat);
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
export default function Chat(props) {
    const [messages, setMessages] = useState([]);
    const [msg, setMsg] = useState('');
    const [dialogue_id, setDialogueId] = useState('');
    const [senderId, setSenderId] = useState('');
    const [isLoader, setIsLoader] = useState(false);
    const [chatUserName, setChatUserName] = useState('');
    const [recipientId, setRecipientId] = useState('');

    useEffect(() => {
        setIsLoader(true)
    }, []);


    useEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage.dialogId) {
            console.log("[[[[[[--", props.route.params.refreshPage)
            setDialogueId(props.route.params.refreshPage.dialogId);
            setSenderId(props.route.params.refreshPage.sender_id)
            getMessages(props.route.params.refreshPage.dialogId)
            setChatUserName(props.route.params.refreshPage.dialogName)
        }
    }, [props]);

    const markMessageRead = (item) => {
        if (item.properties.read != 1 || item.properties.read != '1') {
            const markMessageReadParams = {
                message: {
                    id: item.id,
                    dialogId: dialogue_id,
                    senderId: 12345,
                },
            };
            QB.chat
                .markMessageRead(markMessageReadParams)
                .then(function () {
                    console.log("marked as read successfully ")
                })
                .catch(function (e) {
                    console.log("handle error ", e)
                });
        }
    }

    const checkPermission = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,)
            }
        })
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
            if (!response) {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA)
            }
        })
    }

    const getMessages = (id) => {
        QB.chat
            .getDialogMessages({
                dialogId: id,
                sort: {
                    ascending: false,
                    field: QB.chat.MESSAGES_SORT.FIELD.DATE_SENT
                },
                markAsRead: false
            })
            .then(function (result) {
                console.log("result error:-", result.messages[0].recipientId)
                setMessages(result.messages)
                setIsLoader(false)
                if (result.messages && result.messages[0] && result.messages[0].recipientId) {
                    setRecipientId(result.messages[0].recipientId)
                }
            })
            .catch(function (e) {
                setIsLoader(false)
                console.log("message error:-", e)
            });
    }


    const sendMessage = () => {
        sendNotification()
        const message = {
            dialogId: dialogue_id,
            body: msg,
            saveToHistory: true
        };
        QB.chat
            .sendMessage(message)
            .then(function () {
                setMsg('');
                getMessages(dialogue_id)
            })
            .catch(function (e) {
                alert(e)
            })
    }

    const sendNotification = () => {
        const event = {
            notificationType: QB.events.NOTIFICATION_TYPE.PUSH,
            payload: {
                ios_voip: 1, // to send VoIP push notification (https://docs.quickblox.com/reference/push-notifications#push-notification-formats)
                message: "You get new message",
                // key: value
            },
            recipientsIds: [recipientId], // users' IDs to deliver notification
            senderId: senderId, // ID of the user who created the event
            type: QB.events.NOTIFICATION_EVENT_TYPE.ONE_SHOT,
        };

        QB.events
            .create(event)
            .then(function (data) {
                console.log("notification event(s) created successfully-", data)
                /* notification event(s) created successfully */
            })
            .catch(function (e) {
                console.log("handle error-", e)
                /* handle error */
            });
    }

    const sendMessageContent = (mesg) => {
        const message = {
            dialogId: dialogue_id,
            body: mesg,
            saveToHistory: true
        };
        QB.chat
            .sendMessage(message)
            .then(function () {
                setMsg('');
                getMessages(dialogue_id)
            })
            .catch(function (e) {
                alert(e)
            })
    }

    var formatTime = (timestamp) => {
        var date = new Date(timestamp);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    const handleMessage = () => {
        sendMessage()
    }

    const onBack = () => {
        props.navigation.navigate('DirectMessage', {
            refreshPage: {
                refresh: Math.random(),
            },
        })
        // props.navigation.goBack()
        // const leaveDialogParam = { dialogId: dialogue_id };
        // QB.chat
        //     .leaveDialog(leaveDialogParam)
        //     .then(function () {
        //         props.navigation.goBack()
        //     })
        //     .catch(function (e) {
        //         console.log('Cant leave dialogue-', e)
        //     })
    }

    const loadMoreData = () => {

    }

    const checkPostType = (body) => {
        var matches = body.match(/\[([^\][]*)]/g);
        if (matches && matches.length > 0) {
            var regex = /\[([^\][]*)]/g;
            var results = [], m;
            while (m = regex.exec(body)) {
                results.push(m[1]);
            }
            if (results && results.length > 0 && results[1]) {
                if (results[1] == 'post') {
                    return 'post';
                } else if (results[1] == 'image') {
                    return 'image';
                } else if (results[1] == 'video') {
                    return 'video';
                } else {
                    return 'text';
                }
            } else {
                return 'text';
            }
        } else {
            return 'text';
        }
    }

    const getPostId = (body) => {
        var regex = /\[([^\][]*)]/g;
        var results = [], m;
        while (m = regex.exec(body)) {
            results.push(m[1]);
        }
        return results[0];
    }

    const goToSinglePost = (id) => {
        props.navigation.navigate('SinglePost', {
            refreshPage: {
                refresh: Math.random(),
                postId: id,
                type: 'single_post'
            },
        })
    }

    const uploadQBFile = async (url, type) => {
        const contentUploadParams = {
            url: url,
            public: true
        };

        QB.content
            .upload(contentUploadParams)
            .then(function (file) {
                if (type.includes("image")) {
                    var messg = "[" + file.id + "][image]";
                    sendMessageContent(messg)
                }
            })
            .catch(function (e) {
                console.log("upload error:", e)
            })
    }

    const handleMediaShare = async () => {
        checkPermission();
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            uploadQBFile(res.uri, res.type);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const youtube_parser = (url) => {
        var regExp = /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
        var match = url.match(regExp);
        return (match && match[1].length == 11) ? true : false;
    }

    const handleMesg = (msg) => {
        setMsg(msg);
        // if(youtube_parser(text)){
        //     setMediaUrl(text)
        //     setMediaType('thumbnail')
        // }else{
        //     setMediaUrl('')
        //     setMediaType('')
        // }
    }

    const renderItem = ({ item, index }) => {
        if (item.senderId == senderId) {
            return <View style={styles.right_box}>
                <View style={styles.right_chat}>
                    {checkPostType(item.body) == 'text' &&
                        <Text style={styles.chatMesg}>{item.body}</Text>
                    }
                    {checkPostType(item.body) == 'post' &&
                        <TouchableOpacity onPress={() => goToSinglePost(getPostId(item.body))}>
                            <View>
                                <ChatPost navigation={props.navigation} body={getPostId(item.body)} />
                            </View>
                        </TouchableOpacity>
                    }
                    {checkPostType(item.body) == 'image' &&
                        <View style={{ width: vw * 65 }}>
                            <QBMedia id={getPostId(item.body)} type={checkPostType(item.body)} />
                        </View>
                    }
                    {youtube_parser(item.body) &&
                        <View style={{ width: vw * 65 }}>
                            <Media url={item.body} navigator={props.navigator} type={'thumbnail'} />
                        </View>
                    }
                    <Text style={styles.time}>{formatTime(item.dateSent)}</Text>
                </View>
            </View>
        } else {
            return <View style={styles.left_box}>
                {markMessageRead(item)}
                <View style={styles.left_chat}>
                    {checkPostType(item.body) == 'text' &&
                        <Text style={styles.chatMesg}>{item.body}</Text>
                    }
                    {checkPostType(item.body) == 'post' &&
                        <TouchableOpacity onPress={() => goToSinglePost(getPostId(item.body))}>
                            <View>
                                <ChatPost navigation={props.navigation} body={getPostId(item.body)} />
                            </View>
                        </TouchableOpacity>
                    }
                    {checkPostType(item.body) == 'image' &&
                        <View style={{ width: vw * 65 }}>
                            <QBMedia id={getPostId(item.body)} type={checkPostType(item.body)} />
                        </View>
                    }
                    {youtube_parser(item.body) &&
                        <View style={{ width: vw * 65 }}>
                            <Media url={item.body} navigator={props.navigator} type={'thumbnail'} />
                        </View>
                    }
                    <Text style={styles.time}>{formatTime(item.dateSent)}</Text>
                </View>
            </View>
        }
    }

    return (<>
        <View style={styles.header}>
            <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10, marginTop: 10, width: vw * 10 }} onPress={() => onBack()}>
                <Icon name={"arrow-back"} color="#fff" size={RFPercentage(5)} />
            </TouchableOpacity>
            <View style={{ flexDirection: 'column', justifyContent: 'center', width: vw * 80 }}>
                <Text style={styles.title}>{chatUserName}</Text>
            </View>
        </View>
        {isLoader ?
            <Loader />
            :
            <FlatList
                style={{ width: width, marginBottom: 78 }}
                keyExtractor={(item, index) => index}
                data={messages}
                key={'chat'}
                inverted={-1}
                renderItem={(item, index) => renderItem(item, index)}
                onEndReached={() => loadMoreData()}
                onEndReachedThreshold={0.9}
            // ItemSeparatorComponent={() => <View style={styles.separator} />}
            />
        }
        <View style={styles.typing_area}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'column', justifyContent: 'center', height: 77 }}>
                    <TouchableOpacity onPress={() => handleMediaShare()}>
                        <Icon name={"add"} color="#fff" size={RFPercentage(7)} />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'center', height: 77 }}>
                    <View style={styles.text_input}>
                        <CustomInput border_width={0} margin_bottom={0} returnKeyType="next" value={msg} action={(text) => handleMesg(text)} placeHolder={"Write message..."} />
                    </View>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'center', height: 77 }}>
                    <TouchableOpacity onPress={handleMessage}>
                        <Icon name={"md-send-sharp"} color="#fff" style={{ marginLeft: 20 }} size={RFPercentage(5)} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        backgroundColor: '#6C0AC7',
        height: Platform.OS === 'ios' ? 100 : 80,
        flexDirection: 'row',
        paddingTop: Platform.OS === 'ios' ? 30 : 0,
        justifyContent: 'flex-start'
    },
    title: {
        fontSize: RFPercentage(3),
        color: '#fff',
        textAlign: 'center'
    },
    chat_area: {
        // backgroundColor: '#fff',
        marginBottom: 80
    },
    typing_area: {
        backgroundColor: '#6C0AC7',
        // height: 100,
        width: width,
        borderTopColor: '#000',
        borderTopWidth: 0.5,
        position: 'absolute',
        bottom: 0
    },
    text_input: {
        width: vw * 70,
        backgroundColor: '#fff',
        borderRadius: 100,
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    chatMesg: {
        color: '#fff',
        fontSize: RFPercentage(3)
    },
    time: {
        color: '#fff',
        fontSize: RFPercentage(2),
        textAlign: 'right',
    },
    left_box: {
        paddingTop: 5,
        paddingLeft: 5,
    },
    right_box: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: 5,
        paddingRight: 5,
    },
    left_chat: {
        backgroundColor: '#444',
        padding: 10,
        width: vw * 70,
        borderRadius: 7
    },
    right_chat: {
        backgroundColor: '#6C0AC7',
        padding: 10,
        maxWidth: vw * 70,
        borderRadius: 7
    }
})
