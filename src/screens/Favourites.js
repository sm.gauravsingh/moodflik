import React, { Component, useRef, useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, DrawerLayoutAndroid, Image, ScrollView, Dimensions } from 'react-native'
import CustomButton from '../components/CustomButton'
import Icon from 'react-native-vector-icons/AntDesign';
import BottomMenu from '../components/BottomMenu';
import { HomeServices } from '../Services/Home.services';
import Loader from '../components/Loader';
import { RFPercentage } from 'react-native-responsive-fontsize';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Post from '../components/Post';

const { width, height } = Dimensions.get('window');
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const homeServices = new HomeServices();

export default function Favourites(props) {
    const drawer = useRef(null);
    const [likesCount, setLikesCount] = useState(0);
    const [likeSection, setLikeSection] = useState(true);
    const [dislikesCount, setDislikesCount] = useState(0);
    const [isLoader, setIsLoader] = useState(false);
    const [likesPostList, setLikesPostList] = useState([]);
    const [disLikesPostList, setDisLikesPostList] = useState([]);
    const [userId, setUserId] = useState('');
    const [likeRefresh, setLikeListRefresh] = useState(false);
    const [dislikeRefresh, setDisLikeListRefresh] = useState(false);
    const [likepostPagination, setLikePostPagination] = useState({});
    const [dislikepostPagination, setDisLikePostPagination] = useState({});

    useEffect(() => {
        if (props && props.route && props.route.params && props.route.params.refreshPage && props.route.params.refreshPage) {
            if (props.route.params.refreshPage.check == 'fav') {
                if (props.route.params.refreshPage.type == 'like') {
                    setLikeSection(true)
                } else {
                    setLikeSection(false)
                }
                getFavouritesData(props.route.params.refreshPage.user_id);
                getUser();
            } else {
                getFavouritesData('');
                getUser();
            }
        }
    }, [props]);

    var getFavouritesData = async (id="") => {
        setIsLoader(true)
        await homeServices.getFavouritesData('like',id).then(
            (data) => {
                console.log("ttt", data)
                if (data.results) {
                    setIsLoader(false)
                    setLikesPostList(data.results);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setLikePostPagination(pagination)
                    setLikesCount(data.count)
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
        await homeServices.getFavouritesData('dislike',id).then(
            (data) => {
                if (data.results) {
                    setIsLoader(false)
                    setDisLikesPostList(data.results);
                    var pagination = {
                        count: data.count,
                        next: data.next && data.next,
                        previous: data.previous && data.previous
                    }
                    setDisLikePostPagination(pagination)
                    setDislikesCount(data.count);
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const getMediaUrl = (data) => {
        if (data.photo && data.photo != "") {
            return data.photo;
        } else if (data.video && data.video != "") {
            return data.video;
        } else if (data.gif && data.gif != "") {
            return data.gif;
        } else if (data.file && data.file != "") {
            return data.file;
        } else {
            return "";
        }
    }

    const getMediaType = (data) => {
        if (data.photo && data.photo != "") {
            return "image";
        } else if (data.video && data.video != "") {
            return "video";
        } else if (data.gif && data.gif != "") {
            return "gif";
        } else if (data.file && data.file != "") {
            return "file";
        } else {
            return "";
        }
    }

    const dateFormat = (date) => {
        var result = timeDifference(new Date(), new Date(date));
        return result;
    }
    const timeDifference = (current, previous) => {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = current - previous;

        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        }

        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }

        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        }

        else if (elapsed < msPerMonth) {
            if (Math.round(elapsed / msPerDay) > 1) {
                return Math.round(elapsed / msPerDay) + ' days ago';
            } else {
                return Math.round(elapsed / msPerDay) + ' day ago';
            }
        }

        else if (elapsed < msPerYear) {
            if (Math.round(elapsed / msPerMonth) > 1) {
                return Math.round(elapsed / msPerMonth) + ' months ago';
            } else {
                return Math.round(elapsed / msPerMonth) + ' month ago';
            }
        }
        else {
            if (Math.round(elapsed / msPerYear) > 1) {
                return Math.round(elapsed / msPerYear) + ' years ago';
            } else {
                return Math.round(elapsed / msPerYear) + ' year ago';
            }
        }
    }

    const getUser = async () => {
        try {
            var userData = await AsyncStorage.getItem('_moodflikAppData');
            userData = JSON.parse(userData);
            if (userData && userData != "" && userData.userId) {
                setUserId(userData.userId)
                return userData;
            }
        } catch (err) {
            console.log(err);
        }
    }

    var getLoadMoreData = async (url) => {
        await homeServices.getLoadMoreData(url).then(
            (data) => {
                if (data.results) {
                    setIsLoader(false)
                    if (likeSection) {
                        setLikesPostList([...likesPostList, ...data.favorites]);
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setLikePostPagination(pagination)
                        setLikeListRefresh(false)

                    } else {
                        setDisLikesPostList([...disLikesPostList, ...data.favorites]);
                        var pagination = {
                            count: data.count,
                            next: data.next && data.next,
                            previous: data.previous && data.previous
                        }
                        setDisLikePostPagination(pagination)
                        setDisLikeListRefresh(false)

                    }
                }
            },
            (error) => {
                setIsLoader(false)
                console.log("error.response.status", error);
            }
        );
    }

    const loadMoreData = () => {
        if (likeSection) {
            if (likepostPagination && likepostPagination.next && likepostPagination.next != "") {
                getLoadMoreData(likepostPagination.next);
                setLikeListRefresh(true)
            }
        } else {
            if (dislikepostPagination && dislikepostPagination.next && dislikepostPagination.next != "") {
                getLoadMoreData(dislikepostPagination.next);
                setDisLikeListRefresh(true)
            }
        }
    }

    const renderPost = ({ item, index }) => {
        return <Post navigator={props.navigator} userId={userId} navigation={props.navigation} likeSection={likeSection} post={item} />
    }

    return (<>
        <ScrollView scrollEnabled={true} style={{ height: height }}>
            {isLoader ?
                <Loader />
                :
                <View style={styles.main}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <CustomButton text={`Public Likes (${likesCount})`} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={48} invertColour={!likeSection ? '#6C0AC7' : '#108A07'} textColor="#fff" isIcon={true} icon={<Icon name="heart" size={20} color="#fff" />} textAlign={'left'} />
                        <CustomButton text={`Public Dislikes (${dislikesCount})`} invertColour={likeSection ? '#6C0AC7' : '#BF1414'} btnAction={() => setLikeSection(!likeSection)} enabled={true} btnWidth={48} isIcon={true} icon={<Icon name="dislike1" size={20} color="#fff" style={{ left: 5, top: 2 }} />} textAlign={"left"} />
                    </View>
                    {likeSection ?
                        <FlatList
                            style={{ width: width, marginBottom: 170 }}
                            keyExtractor={(item, index) => index}
                            data={likesPostList}
                            key={'likesection'}
                            // initialScrollIndex={likepostIndex}
                            renderItem={(item, index) => renderPost(item, index)}
                            onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            // ListFooterComponent={this.renderFooter.bind(this)}
                            ListFooterComponent={
                                likeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                        :
                        <FlatList
                            style={{ width: width, marginBottom: 170 }}
                            keyExtractor={(item, index) => index}
                            data={disLikesPostList}
                            key={'dislikesection'}
                            // initialScrollIndex={dislikepostIndex}
                            renderItem={(item, index) => renderPost(item, index)}
                            onEndReached={() => loadMoreData()}
                            onEndReachedThreshold={5}
                            // onViewableItemsChanged={handleChangePost}
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            ListFooterComponent={
                                dislikeRefresh &&
                                <ActivityIndicator color="#6C0AC7" />
                            }
                        />
                    }
                </View>
            }
        </ScrollView>
        <BottomMenu
            navigation={props.navigation}
            activeScreen="Favourites"
        />
    </>
    )
}

const styles = StyleSheet.create({
    main: {
        paddingHorizontal: 5
    },
    img: {
        width: 60,
        height: 60
    },
    label: {
        fontWeight: 'bold',
        color: '#000',
        fontStyle: 'italic',
        fontSize: RFPercentage(2.7),
        textAlign: 'center',
        marginBottom: 10
    }
})
